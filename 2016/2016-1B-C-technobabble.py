#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve 2016 Round 1B C.

https://code.google.com/codejam/contest/11254486/dashboard#s=p2

Problem

Every year, your professor posts a blank sign-up sheet for a prestigious
scientific research conference on her door. If a student wants to give a
lecture at the conference, they choose a two-word topic that is not already on
the sheet and write it on the sheet. Once the deadline has passed, the
professor has one of her grad students put the topics in a random order, to
avoid biasing for or against students who signed up earlier. Then she presents
the topics to you for review.

Since the snacks at the conference are excellent, some students try to fake
their way into the conference. They choose the first word of some topic already
on the sheet and the second word of some topic already on the sheet, and
combine them (putting the first word first, and the second word second) to
create a new "topic" (as long as it isn't already on the sheet). Since your
professor is open-minded, sometimes this strategy actually works!

The fakers are completely unoriginal and can't come up with any new first or
second words on their own; they must use existing ones from the sheet.
Moreover, they won't try to use an existing first word as their own second word
(unless the word also already exists on the sheet as a second word), or vice
versa.

You have a list of all N of the submitted topics, in some arbitrary order; you
don't know the order in which they were actually written on the sheet. What's
the largest number of them that could have been faked?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each consists of one line with an integer N, followed by N lines, each
of which represents a different topic and has two strings of uppercase English
letters: the two words of the topic, in order.

Output

For each test case, output one line containing Case #x: y, where x is the test
case number (starting from 1) and y is an integer: the largest number of topics
that could have possibly been faked.

Limits

1 ≤ T ≤ 100.
1 ≤ length of each word ≤ 20.
No topic is repeated within a case.

Small dataset

1 ≤ N ≤ 16.

Large dataset

1 ≤ N ≤ 1000.

Sample

Input

3
3
HYDROCARBON COMBUSTION
QUAIL BEHAVIOR
QUAIL COMBUSTION
3
CODE JAM
SPACE JAM
PEARL JAM
2
INTERGALACTIC PLANETARY
PLANETARY INTERGALACTIC

Output

Case #1: 1
Case #2: 0
Case #3: 0
"""

from hopcroftkarp import HopcroftKarp

T = int(input())  # number of test cases
for t in range(T):
    N = int(input())  # number of topics

    U = set()
    V = set()
    G = dict()
    for n in range(N):
        u, v = input().split()  # first and second topic words
        v += '_'  # in case same word in both sides
        # each bipartite set
        U.add(u)
        V.add(v)
        # the graph
        if u not in G:
            G[u] = set()
        G[u].add(v)

    mm = HopcroftKarp(G).maximum_matching()
    r = int(len(mm) / 2)  # the returned graph has both directions
    # N - r => number of not matched edges
    # u - r => free u vertices
    # v - r => free v vertices
    # N - r - (u - r) - (v - r)
    # N - r - u + r - v + r
    # N + r - u - v
    res = N + r - len(U) - len(V)

    # print('\n'.join([tp[0] + ' ' + tp[1] for tp in topics]))
    print('Case #{:d}: {:d}'.format(t + 1, res))
