package main

import (
	"fmt"
)

func solve(S string) int {
	// Init at zero flips
	var flips int
	var prev = rune(S[0])
	for _, v := range S {
		// Check change
		if prev != v {
			flips++
		}
		// Next
		prev = v
	}
	if rune(S[len(S)-1]) == '-' {
		flips++
	}
	return flips
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var S string // pancakes state +--+--++-
		fmt.Scanln(&S)
		// Solve
		r := solve(S)
		fmt.Printf("Case #%d: %d\n", t+1, r)
	}
}
