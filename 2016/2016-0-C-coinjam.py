#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Qualification Round C 2016.

https://code.google.com/codejam/contest/6254486/dashboard#s=p2

Problem

A jamcoin is a string of N ≥ 2 digits with the following properties:

* Every digit is either 0 or 1.
* The first digit is 1 and the last digit is 1.
* If you interpret the string in any base between 2 and 10, inclusive, the
  resulting number is not prime.

Not every string of 0s and 1s is a jamcoin. For example, 101 is not a jamcoin;
its interpretation in base 2 is 5, which is prime. But the string 1001 is a
jamcoin: in bases 2 through 10, its interpretation is 9, 28, 65, 126, 217, 344,
513, 730, and 1001, respectively, and none of those is prime.

We hear that there may be communities that use jamcoins as a form of currency.
When sending someone a jamcoin, it is polite to prove that the jamcoin is
legitimate by including a nontrivial divisor of that jamcoin's interpretation
in each base from 2 to 10. (A nontrivial divisor for a positive integer K is
some positive integer other than 1 or K that evenly divides K.) For
convenience, these divisors must be expressed in base 10.

For example, for the jamcoin 1001 mentioned above, a possible set of nontrivial
divisors for the base 2 through 10 interpretations of the jamcoin would be: 3,
7, 5, 6, 31, 8, 27, 5, and 77, respectively.

Can you produce J different jamcoins of length N, along with proof that they
are legitimate?

Input

The first line of the input gives the number of test cases, T. T test cases
follow; each consists of one line with two integers N and J.

Output

For each test case, output J+1 lines. The first line must consist of only
Case #x:, where x is the test case number (starting from 1). Each of the last J
lines must consist of a jamcoin of length N followed by nine integers. The i-th
of those nine integers (counting starting from 1) must be a nontrivial divisor
of the jamcoin when the jamcoin is interpreted in base i+1.

All of these jamcoins must be different. You cannot submit the same jamcoin in
two different lines, even if you use a different set of divisors each time.

Limits

T = 1. (There will be only one test case.)
It is guaranteed that at least J distinct jamcoins of length N exist.

Small dataset

N = 16.
J = 50.

Large dataset

N = 32.
J = 500.

Note that, unusually for a Code Jam problem, you already know the exact
contents of each input file. For example, the Small dataset's input file will
always be exactly these two lines:

1
16 50

So, you can consider doing some computation before actually downloading an
input file and starting the clock.

Sample

Input

1
6 3

Output

Case #1:
100011 5 13 147 31 43 1121 73 77 629
111111 21 26 105 1302 217 1032 513 13286 10101
111001 3 88 5 1938 7 208 3 20 11
"""

import math
import sys


def find_coins(N, X):
    """
    Find some valid coins.

    N digits, 10 1s, N-10 0s
    every 11 in base b is divisible by b+1
    11 in base 2 => 3 is divisible by b+1 => 3
    11 in base 3 => 4 is divisible by b+1 => 4
    11 in base 4 => 5 is divisible by b+1 => 5
    """
    for i in range(N - 10):
        for j in range(N - 10 - i):
            for k in range(N - 10 - i - j):
                l = N - 10 - i - j - k
                assert l >= 0
                template = "11{}11{}11{}11{}11"  # 10 11s and N-10 0s
                output = template.format("0" * i, "0" * j, "0" * k, "0" * l)
                factors = "3 4 5 6 7 8 9 10 11"
                print output, factors
                X -= 1
                if X == 0:
                    return
    # If we get here, we didn't mine enough jamcoins!
    assert False

fh = open(sys.argv[1], 'r')
T = int(fh.readline())  # number of test cases
for t in range(T):
    N, J = [int(a) for a in fh.readline().split()]  # length / num jamcoins
    print('Case #{:d}:'.format(t + 1))
    find_coins(N, J)
    exit(0)

    found = 0  # found correct coinjams
    i = 0  # number to fill inside the pattern (converted to binary)
    pattern = '1{:s}1'  # main number pàttern
    maxpattern = '1' * (N - 2)  # max number to be inside the pattern
    maxnumber = int(maxpattern, base=2)  # max number possible
    while found < J and i <= maxnumber:
        # convert number i to the pattern
        binnumber = bin(i)
        text = binnumber[2:]
        text = ((N - 2) - len(text)) * '0' + text
        number = pattern.format(text)

        # find if number has any prime in the different bases
        divisors = list()
        numbers = list()
        for b in range(2, 10 + 1):
            done = False
            num = int(number, base=b)
            numbers.append(num)
            # check primes [2, 3]
            for x in [2, 3]:
                if num % x == 0:
                    divisors.append(str(x))
                    done = True
                    break
            if not done:
                # check all other numbers containing primes
                # the form is 6n+1 or 6n-1 [5, 7, 11, 13, 17, 19...]
                # until the sqrt() of the number
                for a in range(1, int((math.sqrt(num) + 1.0) / 6.0) + 1):
                    one = int(str(6 * a - 1), base=12)
                    two = int(str(6 * a + 1), base=12)
                    one = 6 * a - 1
                    two = 6 * a + 1
                    if num % one == 0:
                        divisors.append(str(one))
                        done = True
                        break
                    elif num % two == 0:
                        divisors.append(str(two))
                        done = True
                        break

            if not done:
                break  # avoid searching more if on base is prime
        if len(divisors) == 9 or number == '100011':
            found += 1
            out = ' '.join(divisors)
            print('{:s} {:s}'.format(number, out))
        # next loop
        i += 1
