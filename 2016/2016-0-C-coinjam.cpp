// Solve Qualification Round C 2016.
// https://code.google.com/codejam/contest/6254486/dashboard#s=p2

#include <bitset>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

unsigned long int get_num_base(const std::string &text, int base = 10)
{
  unsigned long int result = 0;
  for (int i = 0; i < text.length(); i++)
  {
    result = result * base + stoi(text.substr(i, 1));
  }
  return result;
}

int main()
{
  int T, N, J;
  cin >> T;  // num of test cases
  for (int t = 0; t < T; t++)
  {
    cout << "Case #" << t + 1 << ":" << endl;
    cin >> N >> J;  // length / num jamcoins to find

    unsigned int found = 0;  // found coinjams
    unsigned long int i = 0;   // number to fill inside the pattern (converted to binary)
    string maxtext(N - 2, '1');
    unsigned long int maxnumber = get_num_base(maxtext);

    // variables for looking for divisors
    while ((found < J) && (i <= maxnumber))
    {
      // Create the pattern
      string inside = bitset<30>(i).to_string();
      string pattern = "1" + inside + "1";

      // Get numbers in all bases
      vector<unsigned long int> divisors(9, 0);
      bool good = true;
      for (int b = 2; b < 11; b++)
      {
        unsigned long int num = get_num_base(pattern, b);
        // Check for divisors 2, 3
        bool done = false;
        for (int x = 2; x <= 3; x++)
        {
          if (num % x == 0)
          {
            divisors[b - 2] = x;
            done = true;
            break;
          }
        }
        // check other divisors
        if (!done)
        {
          for (int a = 1; a < static_cast<int>((sqrt(num) + 1.0)/6.0) + 1; a++)
          {
            unsigned long int one = 6 * a - 1;
            unsigned long int two = 6 * a + 1;
            if (num % one == 0)
            {
              divisors[b - 2] = one;
              done = true;
              break;
            }
            else if (num % two == 0)
            {
              divisors[b - 2] = two;
              done = true;
              break;
            }
          }
        }  // end if not done in one base
        if (!done)
        {
          good = false;
          break;
        }
      }  // end for base

      // result
      if (good)
      {
        found += 1;
        cout << pattern << " ";
        for (int i = 0; i < 9; i++)
        {
          cout << divisors[i] << " ";
        }
        cout << endl;
      }
      // next loop
      i++;
    }  // end while
  }  // end for T
  return 0;
}
