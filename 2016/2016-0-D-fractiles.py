#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve 2016 Qualification D.

https://code.google.com/codejam/contest/6254486/dashboard#s=p3

Problem

Long ago, the Fractal civilization created artwork consisting of linear rows of
tiles. They had two types of tile that they could use: gold (G) and lead (L).

Each piece of Fractal artwork is based on two parameters: an original sequence
of K tiles, and a complexity C. For a given original sequence, the artwork with
complexity 1 is just that original sequence, and the artwork with complexity
X+1 consists of the artwork with complexity X, transformed as follows:

replace each L tile in the complexity X artwork with another copy of the
original sequence replace each G tile in the complexity X artwork with K G
tiles

For example, for an original sequence of LGL, the pieces of artwork with
complexity 1 through 3 are:

C = 1: LGL (which is just the original sequence)
C = 2: LGLGGGLGL
C = 3: LGLGGGLGLGGGGGGGGGLGLGGGLGL

Here's an illustration of how the artwork with complexity 2 is generated from
the artwork with complexity 1:

[image]

You have just discovered a piece of Fractal artwork, but the tiles are too
dirty for you to tell what they are made of. Because you are an expert
archaeologist familiar with the local Fractal culture, you know the values of K
and C for the artwork, but you do not know the original sequence. Since gold is
exciting, you would like to know whether there is at least one G tile in the
artwork. Your budget allows you to hire S graduate students, each of whom can
clean one tile of your choice (out of the KC tiles in the artwork) to see
whether the tile is G or L.

Is it possible for you to choose a set of no more than S specific tiles to
clean, such that no matter what the original pattern was, you will be able to
know for sure whether at least one G tile is present in the artwork? If so,
which tiles should you clean?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each consists of one line with three integers: K, C, and S.

Output

For each test case, output one line containing Case #x: y, where x is the test
case number (starting from 1) and y is either IMPOSSIBLE if no set of tiles
will answer your question, or a list of between 1 and S positive integers,
which are the positions of the tiles that will answer your question. The tile
positions are numbered from 1 for the leftmost tile to KC for the rightmost
tile. Your chosen positions may be in any order, but they must all be
different.

If there are multiple valid sets of tiles, you may output any of them. Remember
that once you submit a Small and it is accepted, you will not be able to
download and submit another Small input. See the FAQ for a more thorough
explanation. This reminder won't appear in problems in later rounds.

Limits

1 ≤ T ≤ 100.
1 ≤ K ≤ 100.
1 ≤ C ≤ 100.
KC ≤ 1018.

Small dataset

S = K.
Large dataset

1 ≤ S ≤ K.

Sample

Input

5
2 3 2
1 1 1
2 1 1
2 1 2
3 2 3

Output

Case #1: 2
Case #2: 1
Case #3: IMPOSSIBLE
Case #4: 1 2
Case #5: 2 6
"""

import sys


def get_number(coords, length):
    """
    Get final position following different coordinates.

    The level of complexity C is taken from the length of coordinates.
    The length parameter is the number of K tiles in the original sequence.

    Returns the position counting from one.
    """
    val = 1
    C = len(coords)
    for i in range(C):
        # K^(C-1-i) how many levels affect this choice
        # coords[i] - 1 => the path chosen
        val += (length ** ((C - 1) - i)) * (coords[i] - 1)
    return val


fh = open(sys.argv[1], 'r')
T = int(fh.readline())  # number of test cases
for t in range(T):
    K, C, S = [int(a) for a in fh.readline().split()]  # length / comp / grad

    # Impossible or possible
    # At each level of complexity C we can know C tiles with only one look
    # Taking path [1, 2, 3] on a four tile K = 4 what we know is that:
    # $$$? we discern the first three tiles
    if S * C < K:
        # Impossible
        print('Case #{:d}: IMPOSSIBLE'.format(t + 1))
    else:
        # Possible
        allindexs = set(range(1, K + 1))
        numtouse = C
        coordinates = list()
        currentindexs = set(range(1, K + 1))
        for s in range(S):
            indextouse = set()
            if len(currentindexs) >= numtouse:
                # get the ones you want
                for a in range(numtouse):
                    indextouse.add(currentindexs.pop())
                indextouse = list(indextouse)
            else:
                # fill with already used indexes
                indextouse = [1] * (numtouse - len(currentindexs))
                indextouse += list(currentindexs)

            # get location in the final tiles (which to clean)
            num = get_number(indextouse, K)
            coordinates.append(str(num))
            currentindexs -= set(indextouse)
            # Finish condition
            if len(currentindexs) == 0:
                res = ' '.join(coordinates)
                print('Case #{:d}: {:s}'.format(t + 1, res))
                break
