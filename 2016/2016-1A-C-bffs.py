#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve 2016 Round 1A B.

https://code.google.com/codejam/contest/4304486/dashboard#s=p2

Problem

You are a teacher at the brand new Little Coders kindergarten. You have N kids
in your class, and each one has a different student ID number from 1 through N.
Every kid in your class has a single best friend forever (BFF), and you know
who that BFF is for each kid. BFFs are not necessarily reciprocal -- that is,
B being A's BFF does not imply that A is B's BFF.

Your lesson plan for tomorrow includes an activity in which the participants
must sit in a circle. You want to make the activity as successful as possible
by building the largest possible circle of kids such that each kid in the
circle is sitting directly next to their BFF, either to the left or to the
right. Any kids not in the circle will watch the activity without
participating.

What is the greatest number of kids that can be in the circle?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each test case consists of two lines. The first line of a test case
contains a single integer N, the total number of kids in the class. The second
line of a test case contains N integers F1, F2, ..., FN, where Fi is the
student ID number of the BFF of the kid with student ID i.

Output

For each test case, output one line containing "Case #x: y", where x is the
test case number (starting from 1) and y is the maximum number of kids in the
group that can be arranged in a circle such that each kid in the circle is
sitting next to his or her BFF.

Limits

1 ≤ T ≤ 100.
1 ≤ Fi ≤ N, for all i.
Fi ≠ i, for all i. (No kid is their own BFF.)

Small dataset

3 ≤ N ≤ 10.

Large dataset

3 ≤ N ≤ 1000.

Sample

Input

4
4
2 3 4 1
4
3 3 4 1
4
3 3 4 3
10
7 8 10 10 9 2 9 6 3 3

Output

Case #1: 4
Case #2: 3
Case #3: 3
Case #4: 6
"""

import sys

fh = open(sys.argv[1], 'r')
T = int(fh.readline())  # number of test cases
for t in range(T):
    N = int(fh.readline())  # number of kids
    BFFs = [int(v) - 1 for v in fh.readline().split()]  # BFFs of each kid

    ans = 0

    best = [0 for i in range(N)]
    # starting at each node
    for n in range(N):
        cur = n  # current kid
        vis = [0 for i in range(N)]
        vis[cur] = 1
        while vis[BFFs[cur]] == 0:  # not closing a loop
            vis[BFFs[cur]] = 1
            cur = BFFs[cur]
        # Big single loop
        if BFFs[cur] == n:
            ans = max(ans, sum(vis))
        # Contains a 2 people loop at the end
        if BFFs[BFFs[cur]] == cur:
            # Save longest path that leads to this loop
            best[cur] = max(best[cur], sum(vis))

    tot = 0
    for n in range(N):
        # Is a 2 people loop
        if BFFs[BFFs[n]] == n:
            # all groups ending with 2 loop can be put together
            tot += best[n] + best[BFFs[n]] - 2
    # we counted each twice
    tot /= 2

    print('Case #{:d}: {}'.format(t + 1, max(ans, tot)))
