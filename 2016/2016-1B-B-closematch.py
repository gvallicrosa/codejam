#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve 2016 Round 1B B.

https://code.google.com/codejam/contest/11254486/dashboard#s=p1

Problem

You are attending the most important game in sports history. The Oceania Coders
are playing the Eurasia Jammers in the Centrifugal Bumble-Puppy world finals.
Unfortunately, you were sleep deprived from all the anticipation, so you fell
asleep during the game!

The scoreboard is currently displaying both scores, perhaps with one or more
leading zeroes (because the scoreboard displays a fixed number of digits).
While you were asleep, some of the lights on the scoreboard were damaged by
strong ball hits, so one or more of the digits in one or both scores are not
being displayed.

You think close games are more exciting, and you would like to imagine that the
scores are as close as possible. Can you fill in all of the missing digits in a
way that minimizes the absolute difference between the scores? If there is more
than one way to attain the minimum absolute difference, choose the way that
minimizes the Coders' score. If there is more than one way to attain the
minimum absolute difference while also minimizing the Coders' score, choose the
way that minimizes the Jammers' score.

Input

The first line of the input gives the number of test cases, T. T cases follow.
Each case consists of one line with two non-empty strings C and J of the same
length, composed only of decimal digits and question marks, representing the
score as you see it for the Coders and the Jammers, respectively. There will be
at least one question mark in each test case.

Output

For each test case, output one line containing Case #x: c j, where x is the
test case number (starting from 1), c is C with the question marks replaced by
digits, and j is J with the question marks replaced by digits, such that the
absolute difference between the integers represented by c and j is minimized.
If there are multiple solutions with the same absolute difference, use the one
in which c is minimized; if there are multiple solutions with the same absolute
difference and the same value of c, use the one in which j is minimized.

Limits

1 ≤ T ≤ 200.
C and J have the same length.

Small dataset

1 ≤ the length of C and J ≤ 3.

Large dataset

1 ≤ the length of C and J ≤ 18.

Sample

Input

4
1? 2?
?2? ??3
? ?
?5 ?0

Output

Case #1: 19 20
Case #2: 023 023
Case #3: 0 0
Case #4: 05 00
"""

from operator import itemgetter
import sys


def prefix(C, J, k):
    """Substitute the leading values."""
    c = list()
    j = list()
    for i in range(k):
        if C[i] == '?' and J[i] == '?':
            c.append('0')
            j.append('0')
        elif C[i] == '?' and J[i] != '?':
            c.append(J[i])
            j.append(J[i])
        elif C[i] != '?' and J[i] == '?':
            c.append(C[i])
            j.append(C[i])
        else:
            c.append(C[i])
            j.append(J[i])
    c = ''.join(c)
    j = ''.join(j)
    return c, j


def tomin(s):
    """To minimum value."""
    return s.replace('?', '0')


def tomax(s):
    """To maximum value."""
    return s.replace('?', '9')


def p_one(c):
    """Add one."""
    return str(int(c) + 1)


def m_one(c):
    """Add minus one."""
    return str(int(c) - 1)

fh = open(sys.argv[1], 'r')
T = int(fh.readline())  # number of test cases
for t in range(T):
    C, J = fh.readline().split()  # scores of coders and jammers

    sol = list()

    # Blind substitution
    c, j = prefix(C, J, len(C))
    sol.append((c, j))

    # All other possibilities
    for i in range(len(C)):
        c, j = prefix(C, J, i)
        if C[i] == '?' and J[i] == '?':
            sol.append((c + '1' + tomin(C[i + 1:]),
                        j + '0' + tomax(J[i + 1:])))
            sol.append((c + '0' + tomax(C[i + 1:]),
                        j + '1' + tomin(J[i + 1:])))
        elif C[i] == '?' and J[i] != '?':
            if J[i] != '9':
                sol.append((c + p_one(J[i]) + tomin(C[i + 1:]),
                            j + tomax(J[i:])))
            if J[i] != '0':
                sol.append((c + m_one(J[i]) + tomax(C[i + 1:]),
                            j + tomin(J[i:])))
        elif C[i] != '?' and J[i] == '?':
            if C[i] != '9':
                sol.append((c + tomax(C[i:]),
                            j + p_one(C[i]) + tomin(J[i + 1:])))
            if C[i] != '0':
                sol.append((c + tomin(C[i:]),
                            j + m_one(C[i]) + tomax(J[i + 1:])))
        else:
            sol.append((c + tomin(C[i:]), j + tomax(J[i:])))
            sol.append((c + tomax(C[i:]), j + tomin(J[i:])))

    num = [(abs(int(a) - int(b)), a, b) for a, b in sol]
    num.sort(key=itemgetter(0, 1, 2))  # sort by diff, coders and jammers
    res = num[0][1] + ' ' + num[0][2]

    print("Case #{:d}: {}".format(t + 1, res))
