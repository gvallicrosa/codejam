package main

import (
	"fmt"
	"strconv"
)

func solve(N int) int {
	// Numbers seen
	var nums [10]int
	done := false
	num := N
	for !done {
		// Add numbers
		for _, s := range strconv.Itoa(num) {
			n, _ := strconv.Atoi(string(s))
			nums[n]++
		}
		// Check all numbers
		done = true
		for _, n := range nums {
			if n == 0 {
				done = false
			}
		}
		// Increase
		if !done {
			num += N
		}
	}
	return num
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var N int // initial number: N, 2N, 3N, ...
		fmt.Scanln(&N)
		// Solve
		if N == 0 {
			fmt.Printf("Case #%d: INSOMNIA\n", t+1)
		} else {
			r := solve(N)
			fmt.Printf("Case #%d: %d\n", t+1, r)
		}

	}
}
