#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve 2016 Round 1B A.

https://code.google.com/codejam/contest/11254486/dashboard#s=p0

Problem

You just made a new friend at an international puzzle conference, and you asked
for a way to keep in touch. You found the following note slipped under your
hotel room door the next day:

"Salutations, new friend! I have replaced every digit of my phone number with
its spelled-out uppercase English representation ("ZERO", "ONE", "TWO",
"THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE" for the digits 0
through 9, in that order), and then reordered all of those letters in some way
to produce a string S. It's up to you to use S to figure out how many digits
are in my phone number and what those digits are, but I will tell you that my
phone number consists of those digits in nondecreasing order. Give me a call...
if you can!"

You would to like to call your friend to tell him that this is an obnoxious way
to give someone a phone number, but you need the phone number to do that! What
is it?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each consists of one line with a string S of uppercase English letters.

Output

For each test case, output one line containing Case #x: y, where x is the test
case number (starting from 1) and y is a string of digits: the phone number.

Limits

1 ≤ T ≤ 100.
A unique answer is guaranteed to exist.

Small dataset

3 ≤ length of S ≤ 20.

Large dataset

3 ≤ length of S ≤ 2000.

Sample

Input

4
OZONETOWER
WEIGHFOXTOURIST
OURNEONFOE
ETHER

Output

Case #1: 012
Case #2: 2468
Case #3: 114
Case #4: 3
"""

import sys

nums = ["ZERO",  # only Z
        "ONE",
        "TWO",  # only W
        "THREE",
        "FOUR",  # only U
        "FIVE",
        "SIX",  # only X
        "SEVEN",
        "EIGHT",  # only G
        "NINE"]

nums = ["ONE",  # only O
        "THREE",  # only THR
        "FIVE",  # only F
        "SEVEN",  # only S
        "NINE"]

for i in range(len(nums)):
    orig = nums[i]
    coun = [0] * len(orig)
    for j in range(len(nums)):
        if i != j:
            comp = nums[j]
            for k, c in enumerate(orig):
                for d in comp:
                    if c == d:
                        coun[k] += 1
    # print orig, coun

nums = ["ZERO",  # only Z
        "TWO",  # only W
        "FOUR",  # only U
        "SIX",  # only X
        "EIGHT",  # only G
        "ONE",
        "THREE",
        "FIVE",
        "SEVEN",
        "NINE"]

equiv = {"ZERO": 0,  # only Z
         "TWO": 2,  # only W
         "FOUR": 4,  # only U
         "SIX": 6,  # only X
         "EIGHT": 8,  # only G
         "ONE": 1,
         "THREE": 3,
         "FIVE": 5,
         "SEVEN": 7,
         "NINE": 9}

fh = open(sys.argv[1], 'r')
T = int(fh.readline())  # number of test cases
for t in range(T):
    S = fh.readline().split()[0]  # mixed string of spelled numbers

    curr = S
    idx = 0
    res = list()
    # print curr
    while len(curr) > 0:
        count = 0
        # find all chars of the number
        for c in nums[idx]:
            if curr.find(c) >= 0:
                count += 1
        # if all chars found
        if count == len(nums[idx]):
            # delete chars
            for c in nums[idx]:
                i = curr.find(c)
                curr = curr[:i] + curr[i + 1:]
            # print ">>", nums[idx]
            # print curr
            res.append(equiv[nums[idx]])
        else:
            # look at next number
            idx += 1
        # print len(curr)

    res.sort()
    res = ''.join([str(v) for v in res])
    print("Case #{:d}: {}".format(t + 1, res))
