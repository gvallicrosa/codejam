"""
--- Day 2: Corruption Checksum ---

The spreadsheet consists of rows of apparently-random numbers. To make sure the recovery process
is on the right track, they need you to calculate the spreadsheet's checksum. For each row,
determine the difference between the largest value and the smallest value; the checksum is the
sum of all of these differences.

For example, given the following spreadsheet:

5 1 9 5
7 5 3
2 4 6 8

* The first row's largest and smallest values are 9 and 1, and their difference is 8.
* The second row's largest and smallest values are 7 and 3, and their difference is 4.
* The third row's difference is 6.

In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

What is the checksum for the spreadsheet in your puzzle input?

-------------------------------------------

It sounds like the goal is to find the only two numbers in each row where one evenly divides the
other - that is, where the result of the division operation is a whole number. They would like you
to find those numbers on each line, divide them, and add up each line's result.

For example, given the following spreadsheet:

5 9 2 8
9 4 7 3
3 8 6 5

* In the first row, the only two numbers that evenly divide are 8 and 2; the result of this division is 4.
* In the second row, the two numbers are 9 and 3; the result is 3.
* In the third row, the result is 2.

In this example, the sum of the results would be 4 + 3 + 2 = 9.

What is the sum of each row's result in your puzzle input?
"""

import sys
import numpy as np


def solve(lines):
    """Solve the spreadsheet."""
    result = 0
    for line in lines:
        vals = np.array(line.split(), dtype=np.int64)
        result += vals.max() - vals.min()
    return result

example = [
    "5 1 9 5\n",
    "7 5 3\n",
    "2 4 6 8\n"
]

assert solve(example) == 18


def solve2(lines):
    """Solve the spreadsheet."""
    result = 0
    for line in lines:
        vals = [int(v) for v in line.split()]
        vals.sort(reverse=True)
        # Find the evenly divisible ones
        found = False
        i = 0
        while not found and i < len(vals) - 1:
            j = i + 1
            while not found and j < len(vals):
                if vals[i] % vals[j] == 0:
                    found = True
                    result += vals[i] / vals[j]
                # next
                j += 1
            # next
            i += 1
    return result

example2 = [
    "5 9 2 8\n",
    "9 4 7 3\n",
    "3 8 6 5\n"
]

assert solve2(example2) == 9

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print solve2(data)
