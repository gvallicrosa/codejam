"""
--- Day 23: Coprocessor Conflagration ---

You decide to head directly to the CPU and fix the printer from there. As you get close, you find
an experimental coprocessor doing so much work that the local programs are afraid it will halt and
catch fire. This would cause serious issues for the rest of the computer, so you head in and see
what you can do.

The code it's running seems to be a variant of the kind you saw recently on that tablet. The
general functionality seems very similar, but some of the instructions are different:

* set X Y sets register X to the value of Y.
* sub X Y decreases register X by the value of Y.
* mul X Y sets register X to the result of multiplying the value contained in register X by the
value of Y.
* jnz X Y jumps with an offset of the value of Y, but only if the value of X is not zero. (An
offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so
on.)

Only the instructions listed above are used. The eight registers here, named a through h, all start
at 0.

The coprocessor is currently set to some kind of debug mode, which allows for testing, but prevents
it from doing any meaningful work.

If you run the program (your puzzle input), how many times is the mul instruction invoked?

--------------------------------------------

Now, it's time to fix the problem.

The debug mode switch is wired directly to register a. You flip the switch, which makes register a
now start at 1 when the program is executed.

Immediately, the coprocessor begins to overheat. Whoever wrote this program obviously didn't choose
a very efficient implementation. You'll need to optimize the program if it has any hope of
completing before Santa needs that printer working.

The coprocessor's ultimate goal is to determine the final value left in register h once the program
completes. Technically, if it had that... it wouldn't even need to run the program.

After setting register a to 1, if the program were to run to completion, what value would be left
in register h?
"""

from collections import defaultdict
import sys
import numpy as np


class Registers(object):
    def __init__(self, lines, inita=0):
        # copy and clean
        self.lines = lines
        for line in self.lines:
            if line[-1] == '\n':
                line = line[:-1]
        # init
        self.reg = defaultdict(int)
        self.reg['a'] = inita  # second part
        self.pos = 0
        self.jump = False
        self.muls = 0

    def tonum(self, s):
        if s.isalpha():
            return self.reg[s]
        return int(s)

    def increment(self):
        if not(0 <= self.pos < len(self.lines)):
            return False
        if self.jump:
            self.jump = False
            return True
        # otherwise
        self.pos += 1
        if 0 <= self.pos < len(self.lines):
            return True
        return False

    def process(self, debug=False):
        # Get line
        line = self.lines[self.pos]
        temp = line.split()
        if debug:
            print(self.pos, line)
        # Process command
        if 'set' in line:
            self.reg[temp[1]] = self.tonum(temp[2])
        elif 'sub' in line:
            self.reg[temp[1]] -= self.tonum(temp[2])
        elif 'mul' in line:
            self.muls += 1
            self.reg[temp[1]] *= self.tonum(temp[2])
        elif 'jnz' in line:
            if self.tonum(temp[1]) != 0:
                self.jump = True
                self.pos += self.tonum(temp[2])
        # Increment position
        return self.increment()


def solve(lines):
    reg = Registers(lines)
    while reg.process(debug=False):
        # print reg.reg
        # raw_input()
        pass
    return reg.muls


def solve2():
    b = 84 * 100 + 100000
    c = b + 17000
    # all primes needed for testing
    maxprime = int(np.sqrt(b + 1))
    primes = [2, ]
    for n in range(3, maxprime + 1, 2):
        prime = True
        for p in primes:
            if n % p == 0:
                prime = False
                break
        if prime:
            primes.append(n)
    print(primes)
    # test the case
    h = 0
    for n in range(b, c + 1, 17):
        prime = True
        for p in primes:
            if n % p == 0:
                prime = False
        if not prime:
            h += 1
    return h

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print(solve(data))
    print(solve2())
