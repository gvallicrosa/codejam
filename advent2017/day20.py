"""
--- Day 20: Particle Swarm ---

Suddenly, the GPU contacts you, asking for help. Someone has asked it to simulate too many
particles, and it won't be able to finish them all in time to render the next frame at this rate.

It transmits to you a buffer (your puzzle input) listing each particle in order (starting with
particle 0, then particle 1, particle 2, and so on). For each particle, it provides the X, Y, and Z
coordinates for the particle's position (p), velocity (v), and acceleration (a), each in the format
<X,Y,Z>.

Each tick, all particles are updated simultaneously. A particle's properties are updated in the
following order:

* Increase the X velocity by the X acceleration.
* Increase the Y velocity by the Y acceleration.
* Increase the Z velocity by the Z acceleration.
* Increase the X position by the X velocity.
* Increase the Y position by the Y velocity.
* Increase the Z position by the Z velocity.

Because of seemingly tenuous rationale involving z-buffering, the GPU would like to know which
particle will stay closest to position <0,0,0> in the long term. Measure this using the Manhattan
distance, which in this situation is simply the sum of the absolute values of a particle's X, Y,
and Z position.

For example, suppose you are only given two particles, both of which stay entirely on the X-axis
(for simplicity). Drawing the current states of particles 0 and 1 (in that order) with an adjacent
a number line and diagram of current X positions (marked in parenthesis), the following would take place:

p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>                         (0)(1)

p=< 4,0,0>, v=< 1,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
p=< 2,0,0>, v=<-2,0,0>, a=<-2,0,0>                      (1)   (0)

p=< 4,0,0>, v=< 0,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
p=<-2,0,0>, v=<-4,0,0>, a=<-2,0,0>          (1)               (0)

p=< 3,0,0>, v=<-1,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
p=<-8,0,0>, v=<-6,0,0>, a=<-2,0,0>                         (0)

At this point, particle 1 will never be closer to <0,0,0> than particle 0, and so, in the long run,
particle 0 will stay closest.

Which particle will stay closest to position <0,0,0> in the long term?
"""

import sys
import numpy as np


def vector_from_values(subline):
    temp = subline.split('<')[1].split('>')[0]
    return np.array([float(v) for v in temp.split(',')])


class Particle(object):
    def __init__(self, line):
        # clean
        if line[-1] == '\n':
            line = line[:-1]
        # separate
        pos, vel, acc = line.split(', ')
        # gather
        self.pos = vector_from_values(pos)
        self.vel = vector_from_values(vel)
        self.acc = vector_from_values(acc)
        # collisions
        self.destroyed = False

    def update(self):
        if not self.destroyed:
            self.vel += self.acc
            self.pos += self.vel


def solve(lines):
    # read data
    particles = list()
    for line in lines:
        particles.append(Particle(line))
    # compute a lot of updates
    for _ in range(1000):
        for p in particles:
            p.update()
    # check closest
    dis = 1e100
    idx = -1
    for i, p in enumerate(particles):
        d = np.sum(np.abs(p.pos))
        if d < dis:
            dis = d
            idx = i
    return idx


example = [
    "p=<3,0,0>, v=< 2,0,0>, a=<-1,0,0>\n",
    "p=<4,0,0>, v=< 0,0,0>, a=<-2,0,0>\n"
]

assert solve(example) == 0


def solve2(lines):
    # read data
    particles = list()
    for line in lines:
        particles.append(Particle(line))
    # compute a lot of updates
    nums = range(len(particles))
    for _ in range(1000):
        # move
        for n in nums:
            particles[n].update()
        # collision
        coll = list()
        for i, n in enumerate(nums):
            for j in range(i + 1, len(nums)):
                o = nums[j]
                if np.array_equal(particles[n].pos, particles[o].pos):
                    coll.append(n)
                    coll.append(o)
        # delete
        coll = set(coll)
        for v in coll:
            nums.remove(v)
    return len(nums)

example2 = [
    "p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>\n",
    "p=<-4,0,0>, v=<2,0,0>, a=<0,0,0>\n",
    "p=<-2,0,0>, v=<1,0,0>, a=<0,0,0>\n",
    "p=< 3,0,0>, v=<-1,0,0>, a=<0,0,0>\n"
]

assert solve2(example2) == 1

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print solve2(data)
