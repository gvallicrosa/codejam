"""
--- Day 21: Fractal Art ---

You find a program trying to generate some art. It uses a strange process that involves repeatedly
enhancing the detail of an image through a set of rules.

The image consists of a two-dimensional square grid of pixels that are either on (#) or off (.).
The program always begins with this pattern:

.#.
..#
###

Because the pattern is both 3 pixels wide and 3 pixels tall, it is said to have a size of 3.

Then, the program repeats the following process:

* If the size is evenly divisible by 2, break the pixels up into 2x2 squares, and convert each 2x2
square into a 3x3 square by following the corresponding enhancement rule.
* Otherwise, the size is evenly divisible by 3; break the pixels up into 3x3 squares, and convert
each 3x3 square into a 4x4 square by following the corresponding enhancement rule.

Because each square of pixels is replaced by a larger one, the image gains pixels and so its size
increases.

The artist's book of enhancement rules is nearby (your puzzle input); however, it seems to be
missing rules. The artist explains that sometimes, one must rotate or flip the input pattern to
find a match. (Never rotate or flip the output pattern, though.) Each pattern is written concisely:
rows are listed as single units, ordered top-down, and separated by slashes. For example, the
following rules correspond to the adjacent patterns:

../.#  =  ..
          .#

                .#.
.#./..#/###  =  ..#
                ###

                        #..#
#..#/..../#..#/.##.  =  ....
                        #..#
                        .##.

When searching for a rule to use, rotate and flip the pattern as necessary. For example, all of the
following patterns match the same rule:

.#.   .#.   #..   ###
..#   #..   #.#   ..#
###   ###   ##.   .#.

Suppose the book contained the following two rules:

../.# => ##./#../...
.#./..#/### => #..#/..../..../#..#

As before, the program begins with this pattern:

.#.
..#
###

The size of the grid (3) is not divisible by 2, but it is divisible by 3. It divides evenly into a
single square; the square matches the second rule, which produces:

#..#
....
....
#..#

The size of this enhanced grid (4) is evenly divisible by 2, so that rule is used. It divides
evenly into four squares:

#.|.#
..|..
--+--
..|..
#.|.#

Each of these squares matches the same rule (../.# => ##./#../...), three of which require some
flipping and rotation to line up with the rule. The output for the rule is the same in all four
cases:

##.|##.
#..|#..
...|...
---+---
##.|##.
#..|#..
...|...

Finally, the squares are joined into a new grid:

##.##.
#..#..
......
##.##.
#..#..
......

Thus, after 2 iterations, the grid contains 12 pixels that are on.

How many pixels stay on after 5 iterations?
"""

import math
import sys
import numpy as np
import better_exceptions


def togrid(line):
    rows = line.split('/')
    grid = np.zeros((len(rows), len(rows)), dtype=str)
    for r, row in enumerate(rows):
        grid[r, :] = list(row)
    return grid


class Pattern(object):
    def __init__(self, line):
        # clean
        if line[-1] == '\n':
            line = line[:-1]
        # get data
        inp, out = line.split(' => ')
        self.inp = togrid(inp)
        self.out = togrid(out)
        self.size = self.inp.shape[0]

    def check(self, other):
        if other.shape[0] != self.size:
            return False
        # flipped
        flip = np.flipud(self.inp)
        # check rotation
        for i in range(4):
            rot = np.rot90(self.inp, k=i)
            # print rot
            if np.array_equal(rot, other):
                return True
            rot = np.rot90(flip, k=i)
            # print rot
            if np.array_equal(rot, other):
                return True
        return False


def solve(lines, iterations=5):
    # Gather
    patterns = list()
    for line in lines:
        patterns.append(Pattern(line))
    # Init
    grid = np.array([['.', '#', '.'],
                     ['.', '.', '#'],
                     ['#', '#', '#']
                     ])
    # iterate
    for _ in range(iterations):
        print _
        size = grid.shape[0]
        if size % 2 == 0:
            sub = 2
        else:
            sub = 3
        subsquares = list()
        for r in range(size / sub):
            for c in range(size / sub):
                # print r, c
                # print sub * r, sub * r + sub, sub * c, sub * c + sub
                subsquares.append(
                    grid[sub * r:sub * r + sub, sub * c: sub * c + sub])
        # substitutions
        # print "grid"
        # print grid
        print "subgrids", len(subsquares)
        # for g in subsquares:
        #     print g
        newsubs = list()
        for sub in subsquares:
            for pat in patterns:
                if pat.check(sub):
                    newsubs.append(pat.out)
        assert len(subsquares) == len(newsubs)
        # join to single grid
        sub = newsubs[0].shape[0]
        size = int(math.sqrt(len(newsubs))) * sub
        grid = np.zeros((size, size), dtype=str)
        i = 0
        for r in range(size / sub):
            for c in range(size / sub):
                grid[sub * r:sub * r + sub, sub *
                     c: sub * c + sub] = newsubs[i]
                i += 1
        # print "new"
        # print grid
    # count on
    count = 0
    for r in range(grid.shape[0]):
        for c in range(grid.shape[0]):
            if grid[r, c] == '#':
                count += 1
    return count


example = [
    "../.# => ##./#../...\n",
    ".#./..#/### => #..#/..../..../#..#\n"
]

assert solve(example, iterations=2) == 12

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print solve(data, iterations=18)
