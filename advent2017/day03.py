"""
--- Day 3: Spiral Memory ---

You come across an experimental new kind of memory stored on an infinite two-dimensional grid.

Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then
counting up while spiraling outward. For example, the first few squares are allocated like this:

17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23---> ...

While this is very space-efficient (no squares are skipped), requested data must be carried back to
square 1 (the location of the only access port for this memory system) by programs that can only
move up, down, left, or right. They always take the shortest path: the Manhattan Distance between
the location of the data and square 1.

For example:

* Data from square 1 is carried 0 steps, since it's at the access port.
* Data from square 12 is carried 3 steps, such as: down, left, left.
* Data from square 23 is carried only 2 steps: up twice.
* Data from square 1024 must be carried 31 steps.

How many steps are required to carry the data from the square identified in your puzzle input all
the way to the access port?

Your puzzle input is 325489.

-------------------------------------------

As a stress test on the system, the programs here clear the grid and then store the value 1 in
square 1. Then, in the same allocation order as shown above, they store the sum of the values in
all adjacent squares, including diagonals.

So, the first few squares' values are chosen as follows:

* Square 1 starts with the value 1.
* Square 2 has only one adjacent filled square (with value 1), so it also stores 1.
* Square 3 has both of the above squares as neighbors and stores the sum of their values, 2.
* Square 4 has all three of the aforementioned squares as neighbors and stores the sum of their
values, 4.
* Square 5 only has the first and fourth squares as neighbors, so it gets the value 5.

Once a square is written, its value does not change. Therefore, the first few squares would receive
the following values:

147  142  133  122   59
304    5    4    2   57
330   10    1    1   54
351   11   23   25   26
362  747  806--->   ...

What is the first value written that is larger than your puzzle input?

Your puzzle input is still 325489.
"""

import numpy as np


def compute(board, pos):
    """Sum all neighbors."""
    result = 0
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            if i == 0 and j == 0:
                continue
            if board[pos[0] + i, pos[1] + j] != 0:
                result += board[pos[0] + i, pos[1] + j]
    return result


def solve2(num):
    """Solve second spiral part."""
    moves = [
        np.array([-1, 0]),  # up
        np.array([0, -1]),  # left
        np.array([1, 0]),  # down
        np.array([0, 1])  # right
    ]
    # init
    board = np.zeros((21, 21), dtype=np.int64)
    board[10, 10] = 1
    # second number
    edge = 3
    pos = np.array([10, 11])
    moveidx = 0
    filled = 0
    tofill = edge - 1
    val = compute(board, pos)
    while val < num:
        # save value
        # print "save", val, pos
        board[pos[0], pos[1]] = val
        filled += 1
        # print board
        # limits for next increment
        if filled == tofill:
            moveidx = (moveidx + 1) % 4
            if moveidx == 0:
                # new square
                edge += 2
            tofill = edge - 1
            if moveidx == 0:
                tofill -= 1
            if moveidx == 3:
                # last one is in new square
                tofill += 1
            filled = 0
        # increase position
        pos += moves[moveidx]
        # compute new num
        val = compute(board, pos)
        # raw_input("press")
    return val


def solve(num):
    """
    Solve the spiral memory.

    1, 9, 25, 49, ...

    61 60  59  58  57
               31  56
           13  30  55
        3  12  29  54
     1  2  11  28  53
        9  10  27  52
           25  26  51
               49  50
                   81 82
    """
    # Find the square edge of the previous square
    edge = 1
    while num - (edge * edge) > 1:
        edge += 2
    # print "num", num, "edge", edge
    if num == edge * edge:
        # print "corner"
        return edge - 1
    edge -= 2
    # Find the position in main square
    # on the perimeter but not last
    # which edge of the four
    firstnum = edge * edge + 1
    firstpos = [(edge + 1) / 2, (1 - edge) / 2]  # right, up
    # print "first", firstnum, firstpos
    # find the number
    val = firstnum
    pos = np.array(firstpos)
    tofill = edge
    # check
    if val == num:
        return np.sum(np.abs(pos))
    # the movement
    moveidx = 0  # up
    movement = np.array([0, 1])
    while True:
        for _ in range(tofill):
            # increase number and position
            val += 1
            pos += movement
            # print val, pos
            if val == num:
                return np.sum(np.abs(pos))
        if moveidx == 0:
            moveidx = 1  # left
            movement = np.array([-1, 0])
        elif moveidx == 1:
            moveidx = 2  # down
            movement = np.array([0, -1])
        elif moveidx == 2:
            moveidx = 3  # right
            movement = np.array([1, 0])
        else:
            print "impossible to reach here"
            break
        tofill = edge + 1

    # print num, edge, edge * edge, offset

assert solve(1) == 0
assert solve(12) == 3
assert solve(23) == 2
assert solve(1024) == 31

print solve(325489)
print solve2(325489)
