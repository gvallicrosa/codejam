r"""
--- Day 11: Hex Ed ---

Crossing the bridge, you've barely reached the other side of the stream when a program comes up to
you, clearly in distress. "It's my child process," she says, "he's gotten lost in an infinite grid!"

Fortunately for her, you have plenty of experience with infinite grids.

Unfortunately for you, it's a hex grid.

The hexagons ("hexes") in this grid are aligned such that adjacent hexes can be found to the north,
northeast, southeast, south, southwest, and northwest:

  \ n  /
nw +--+ ne
  /    \
-+      +-
  \    /
sw +--+ se
  / s  \

You have the path the child process took. Starting where he started, you need to determine the
fewest number of steps required to reach him. (A "step" means to move from the hex you are in to any
adjacent hex.)

For example:

* ne,ne,ne is 3 steps away.
* ne,ne,sw,sw is 0 steps away (back where you started).
* ne,ne,s,s is 2 steps away (se,se).
* se,sw,se,sw,sw is 3 steps away (s,s,sw).

---------------------------------------

How many steps away is the furthest he ever got from his starting position?
"""

import sys


def distance(pose):
    """Distance from the origin."""
    dis = 0
    for v in pose:
        dis += abs(v)
    return dis / 2


def solve(line):
    """
    Solve path.

    Inspiration from: https://www.redblobgames.com/grids/hexagons/
    """
    # substract line ending
    if line[-1] == '\n':
        line = line[:-1]
    # database of possible movements
    db = {
        'n': [0, 1, -1],
        'ne': [1, 0, -1],
        'se': [1, -1, 0],
        's': [0, -1, 1],
        'sw': [-1, 0, 1],
        'nw': [-1, 1, 0]
    }
    # move according to problem
    furthest = 0
    coords = line.split(',')
    pose = [0, 0, 0]
    for coord in coords:
        move = db[coord]
        for i in range(3):
            pose[i] += move[i]
        # recompute furthest
        d = distance(pose)
        if d > furthest:
            furthest = d
    # compute distance
    return distance(pose), furthest


assert solve('ne,ne,ne')[0] == 3
assert solve('ne,ne,sw,sw')[0] == 0
assert solve('ne,ne,s,s')[0] == 2
assert solve('se,sw,se,sw,sw')[0] == 3

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()[0]
    print solve(data)
