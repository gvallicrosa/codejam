"""
--- Day 4: High-Entropy Passphrases ---

A new system policy has been put in place that requires all accounts to use a passphrase instead of simply a password. A passphrase consists of a series of words (lowercase letters) separated by spaces.

To ensure security, a valid passphrase must contain no duplicate words.

For example:

aa bb cc dd ee is valid.
aa bb cc dd aa is not valid - the word aa appears more than once.
aa bb cc dd aaa is valid - aa and aaa count as different words.

The system's full passphrase list is available as your puzzle input.
How many passphrases are valid?

-------------------------------------------

For added security, yet another system policy has been put in place. Now, a valid passphrase
must contain no two words that are anagrams of each other - that is, a passphrase is invalid
if any word's letters can be rearranged to form any other word in the passphrase.

For example:

abcde fghij is a valid passphrase.
abcde xyz ecdab is not valid - the letters from the third word can be rearranged to form the
first word.
a ab abc abd abf abj is a valid passphrase, because all letters need to be used when forming
another word.
iiii oiii ooii oooi oooo is valid.
oiii ioii iioi iiio is not valid - any of these words can be rearranged to form any other word.

Under this new system policy, how many passphrases are valid?
"""

import sys


def validate(passphrase):
    """Validate if no word is repeated."""
    words = passphrase.split()
    unique = set(words)
    return len(words) == len(unique)


def count_valid(phrases):
    """Count valid phassparses."""
    count = 0
    for line in phrases:
        if validate(line):
            count += 1
    return count

assert validate("aa bb cc dd ee") is True
assert validate("aa bb cc dd aa") is False
assert validate("aa bb cc dd aaa") is True


def validate_sorted(passphrase):
    """Validate if no word is repeated."""
    words = passphrase.split()
    # print words
    for i in range(len(words)):
        words[i] = ''.join(sorted(words[i]))
        # words[i].sort()
    # print words
    unique = set(words)
    return len(words) == len(unique)


def count_valid_sorted(phrases):
    """Count valid phassparses."""
    count = 0
    for line in phrases:
        if validate_sorted(line):
            count += 1
    return count

assert validate_sorted("abcde fghij") is True
assert validate_sorted("abcde xyz ecdab") is False
assert validate_sorted("a ab abc abd abf abj") is True
assert validate_sorted("iiii oiii ooii oooi oooo") is True
assert validate_sorted("oiii ioii iioi iiio") is False


if __name__ == '__main__':
    lines = open(sys.argv[1]).readlines()
    print count_valid_sorted(lines)
