"""
--- Day 18: Duet ---

You discover a tablet containing some strange assembly code labeled simply "Duet". Rather than
bother the sound card with it, you decide to run the code yourself. Unfortunately, you don't see
any documentation, so you're left to figure out what the instructions mean on your own.

It seems like the assembly is meant to operate on a set of registers that are each named with a
single letter and that can each hold a single integer. You suppose each register should start with
a value of 0.

There aren't that many instructions, so it shouldn't be hard to figure out what they do. Here's
what you determine:

* snd X plays a sound with a frequency equal to the value of X.
* set X Y sets register X to the value of Y.
* add X Y increases register X by the value of Y.
* mul X Y sets register X to the result of multiplying the value contained in register X by the
value of Y.
* mod X Y sets register X to the remainder of dividing the value contained in register X by the
value of Y (that is, it sets X to the result of X modulo Y).
* rcv X recovers the frequency of the last sound played, but only when the value of X is not zero.
(If it is zero, the command does nothing.)
* jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero.
(An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and
so on.)

Many of the instructions can take either a register (a single letter) or a number. The value of a
register is the integer it contains; the value of a number is that number.

After each jump instruction, the program continues with the instruction to which the jump jumped.
After any other instruction, the program continues with the next instruction. Continuing (or
jumping) off either end of the program terminates it.

For example:

set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2

* The first four instructions set a to 1, add 2 to it, square it, and then set it to itself modulo
5,  resulting in a value of 4.
* Then, a sound with frequency 4 (the value of a) is played.
* After that, a is set to 0, causing the subsequent rcv and jgz instructions to both be skipped
(rcv because a is 0, and jgz because a is not greater than 0).
* Finally, a is set to 1, causing the next jgz instruction to activate, jumping back two
instructions to another jump, which jumps again to the rcv, which ultimately triggers the recover
operation.

At the time the recover operation is executed, the frequency of the last sound played is 4.

What is the value of the recovered frequency (the value of the most recently played sound) the
first time a rcv instruction is executed with a non-zero value?

---------------------------------------------

As you congratulate yourself for a job well done, you notice that the documentation has been on the
back of the tablet this entire time. While you actually got most of the instructions correct, there
are a few key differences. This assembly code isn't about sound at all - it's meant to be run twice
at the same time.

Each running copy of the program has its own set of registers and follows the code independently -
in fact, the programs don't even necessarily run at the same speed. To coordinate, they use the send
(snd) and receive (rcv) instructions:

* snd X sends the value of X to the other program. These values wait in a queue until that program
is ready to receive them. Each program has its own message queue, so a program can never receive a
message it sent.
* rcv X receives the next value and stores it in register X. If no values are in the queue, the
program waits for a value to be sent to it. Programs do not continue to the next instruction until
they have received a value. Values are received in the order they are sent.

Each program also has its own program ID (one 0 and the other 1); the register p should begin with
this value.

For example:

snd 1
snd 2
snd p
rcv a
rcv b
rcv c
rcv d

Both programs begin by sending three values to the other. Program 0 sends 1, 2, 0; program 1 sends
1, 2, 1. Then, each program receives a value (both 1) and stores it in a, receives another value
(both 2) and stores it in b, and then each receives the program ID of the other program (program 0
receives 1; program 1 receives 0) and stores it in c. Each program now sees a different value in its
own copy of register c.

Finally, both programs try to rcv a fourth time, but no data is waiting for either of them, and they
reach a deadlock. When this happens, both programs terminate.

It should be noted that it would be equally valid for the programs to run at different speeds; for
example, program 0 might have sent all three values and then stopped at the first rcv before
program 1 executed even its first instruction.

Once both of your programs have terminated (regardless of what caused them to do so), how many times
did program 1 send a value?
"""

import sys
from collections import defaultdict


def solve(lines):
    regs = defaultdict(int)
    lastsound = -1
    pos = 0
    while 0 <= pos < len(lines):
        jump = False
        line = lines[pos]
        if line[-1] == '\n':
            line = line[:-1]
        # print line
        vals = line.split()
        if 'snd' in line:
            if not vals[1].isalpha():
                lastsound = int(vals[1])
            else:
                lastsound = regs[vals[1]]
            # print lastsound
        elif 'set' in line:
            if not vals[2].isalpha():
                regs[vals[1]] = int(vals[2])
            else:
                regs[vals[1]] = regs[vals[2]]
            # print regs[vals[1]]
        elif 'add' in line:
            if not vals[2].isalpha():
                regs[vals[1]] += int(vals[2])
            else:
                regs[vals[1]] += regs[vals[2]]
            # print regs[vals[1]]
        elif 'mul' in line:
            if not vals[2].isalpha():
                regs[vals[1]] *= int(vals[2])
            else:
                regs[vals[1]] *= regs[vals[2]]
            # print regs[vals[1]]
        elif 'mod' in line:
            if not vals[2].isalpha():
                regs[vals[1]] %= int(vals[2])
            else:
                regs[vals[1]] %= regs[vals[2]]
            # print regs[vals[1]]
        elif 'rcv' in line:
            if regs[vals[1]]:
                # print regs[vals[1]]
                return lastsound
            # print 'ignored'
        elif 'jgz' in line:
            if regs[vals[1]]:
                jump = True
                if not vals[2].isalpha():
                    pos += int(vals[2])
                else:
                    pos += regs[vals[2]]
                # print pos
            else:
                # print 'ignored'
                pass

                # next
        if not jump:
            pos += 1
        # raw_input()
    print "out"


example = [
    "set a 1\n",
    "add a 2\n",
    "mul a a\n",
    "mod a 5\n",
    "snd a\n",
    "set a 0\n",
    "rcv a\n",
    "jgz a -1\n",
    "set a 1\n",
    "jgz a -2\n"
]

assert solve(example) == 4


class Register(object):
    def __init__(self, initval):
        self.initval = initval
        self.reg = defaultdict(int)
        self.reg['p'] = initval
        self.pos = 0  # current line
        self.recv = False  # flag receiving
        self.recvto = None  # where to receive
        self.jump = False  # flag jumping
        self.queue = list()  # queue of sent data
        self.send_count = 0
        print self.reg

    def advance(self):
        if self.recv:
            return  # wait until something to read
        if self.jump:
            self.jump = False  # already moved in the jump command
            return
        # otherwise
        self.pos += 1  # increase normal line

    def to_num(self, s):
        if not s.isalpha():
            return int(s)
        return self.reg[s]

    def process(self, line, other):
        # keep waiting for a receive
        if self.recv:
            if other.queue:
                val = other.queue.pop(0)
                self.reg[self.recvto] = val
                self.recv = False
            return  # do not process anything else
        # clean line
        if line[-1] == '\n':
            line = line[:-1]
        # parse line
        vals = line.split()
        if 'snd' in line:
            # send
            # print "send"
            self.send_count += 1
            self.queue.append(self.to_num(vals[1]))
        elif 'set' in line:
            # set
            # print "set"
            self.reg[vals[1]] = self.to_num(vals[2])
        elif 'add' in line:
            # add
            # print "add"
            self.reg[vals[1]] += self.to_num(vals[2])
        elif 'mul' in line:
            # multiply
            # print "multiply"
            self.reg[vals[1]] *= self.to_num(vals[2])
        elif 'mod' in line:
            # modulus
            # print "modulus"
            self.reg[vals[1]] %= self.to_num(vals[2])
        elif 'rcv' in line:
            # receive
            self.recv = True  # keep waiting for a receive
            self.recvto = vals[1]
        elif 'jgz' in line:
            # jump
            if self.to_num(vals[1]) > 0:
                self.jump = True  # do not advance +1
                self.pos += self.to_num(vals[2])  # advance specified by jump


def solve2(lines):
    reg0 = Register(0)
    reg1 = Register(1)
    while (0 <= reg0.pos < len(lines)) or (0 <= reg1.pos < len(lines)):
        # reg0
        if 0 <= reg0.pos < len(lines):
            reg0.process(lines[reg0.pos], reg1)
            reg0.advance()
        # reg1
        if 0 <= reg1.pos < len(lines):
            reg1.process(lines[reg1.pos], reg0)
            reg1.advance()
        # check stalled
        if reg0.recv and reg1.recv:
            if not reg0.queue and not reg1.queue:
                print "stalled"
                break
    return reg1.send_count

example2 = [
    "snd 1\n",
    "snd 2\n",
    "snd p\n",
    "rcv a\n",
    "rcv b\n",
    "rcv c\n",
    "rcv d\n",
]

assert solve2(example2) == 3

if __name__ == '__main__':
    text = open(sys.argv[1]).readlines()
    print solve2(text)
