"""
--- Day 19: A Series of Tubes ---

Somehow, a network packet got lost and ended up here. It's trying to follow a routing diagram (your
puzzle input), but it's confused about where to go.

Its starting point is just off the top of the diagram. Lines (drawn with |, -, and +) show the path
it needs to take, starting by going down onto the only line connected to the top of the diagram. It
needs to follow this path until it reaches the end (located somewhere within the diagram) and stop
there.

Sometimes, the lines cross over each other; in these cases, it needs to continue going the same
direction, and only turn left or right when there's no other option. In addition, someone has left
letters on the line; these also don't change its direction, but it can use them to keep track of
where it's been. For example:

     |
     |  +--+
     A  |  C
 F---|----E|--+
     |  |  |  D
     +B-+  +--+

Given this diagram, the packet needs to take the following path:

* Starting at the only line touching the top of the diagram, it must go down, pass through A, and
continue onward to the first +.
* Travel right, up, and right, passing through B in the process.
* Continue down (collecting C), right, and up (collecting D).
* Finally, go all the way left through E and stopping at F.

Following the path to the end, the letters it sees on its path are ABCDEF.

The little packet looks up at you, hoping you can help it find the way. What letters will it see (in
the order it would see them) if it follows the path? (The routing diagram is very wide; make sure you
view it without line wrapping.)

----------------------------------------------------------------

"""

import sys
import numpy as np


class Maze(object):
    def __init__(self, lines):
        # cleanup
        for i in range(len(lines)):
            if lines[i][-1] == '\n':
                lines[i] = lines[i][:-1]
        # create matrix
        self.grid = np.zeros((len(lines), len(lines[0])), dtype=str)
        for r in range(self.grid.shape[0]):
            self.grid[r, :] = list(lines[r])
        # find start
        self.find_start()
        self.steps = 1

    def find_start(self):
        r = 0
        c = self.grid[r, :].tolist().index('|')
        print "start", r, c, self.grid[r, c]
        self.curr = np.array([r, c])  # current position
        self.move = np.array([1, 0])  # move down
        self.symb = '|'  # current symbol followed

    def check_next(self):
        # increase position
        self.curr = self.curr + self.move  # TODO: check bounds?
        v = self.grid[self.curr[0], self.curr[1]]
        print v, self.curr
        if v == '+':
            # change direction
            if self.check_neighbours():
                self.steps += 1
                return True
            return False
        elif v.isalpha():
            # collect
            self.collect.append(v)
        elif v == ' ':
            return False
        self.steps += 1
        return True

    def check_neighbours(self):
        found = False
        if self.move[1] != 0:
            # going horizontal
            # print 'horizontal'
            if self.curr[0] > 0:
                # check up
                v = self.grid[self.curr[0] - 1, self.curr[1]]
                # print '>', v
                if v.isalpha() or v == '|':
                    found = True
                    self.move = np.array([-1, 0])
            if self.curr[0] < self.grid.shape[0] - 1:
                # check down
                v = self.grid[self.curr[0] + 1, self.curr[1]]
                # print '>', v
                if v.isalpha() or v == '|':
                    found = True
                    self.move = np.array([+1, 0])
        else:
            # moving vertical
            # print 'vertical'
            if self.curr[1] > 0:
                # check left
                v = self.grid[self.curr[0], self.curr[1] - 1]
                # print '>', v
                if v.isalpha() or v == '-':
                    found = True
                    self.move = np.array([0, -1])
            if self.curr[1] < self.grid.shape[1] - 1:
                # check right
                v = self.grid[self.curr[0], self.curr[1] + 1]
                # print '>', v
                if v.isalpha() or v == '-':  # or v == '+'
                    found = True
                    self.move = np.array([0, +1])
        return found

    def compute(self):
        self.collect = list()
        while self.check_next():
            pass
        print self.steps
        return ''.join(self.collect), self.steps


def solve(lines):
    maze = Maze(lines)
    return maze.compute()


example = [
    "     |          \n",
    "     |  +--+    \n",
    "     A  |  C    \n",
    " F---|----E|--+ \n",
    "     |  |  |  D \n",
    "     +B-+  +--+ \n"
]

assert solve(example) == ('ABCDEF', 38)


if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print solve(data)
