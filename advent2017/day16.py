"""
--- Day 16: Permutation Promenade ---

You come upon a very unusual sight; a group of programs here appear to be dancing.

There are sixteen programs in total, named a through p. They start by standing in a line: a stands
in position 0, b stands in position 1, and so on until p, which stands in position 15.

The programs' dance consists of a sequence of dance moves:

* Spin, written sX, makes X programs move from the end to the front, but maintain their order
otherwise. (For example, s3 on abcde produces cdeab).
* Exchange, written xA/B, makes the programs at positions A and B swap places.
* Partner, written pA/B, makes the programs named A and B swap places.

For example, with only five programs standing in a line (abcde), they could do the following dance:

* s1, a spin of size 1: eabcd.
* x3/4, swapping the last two programs: eabdc.
* pe/b, swapping programs e and b: baedc.

After finishing their dance, the programs end up in order baedc.

You watch the dance for a while and record their dance moves (your puzzle input). In what order are
the programs standing after their dance?

-------------------------------------------------------------

Now that you're starting to get a feel for the dance moves, you turn your attention to the dance as
a whole.

Keeping the positions they ended up in from their previous dance, the programs perform it again and
again: including the first dance, a total of one billion (1000000000) times.

In the example above, their second dance would begin with the order baedc, and use the same dance
moves:

s1, a spin of size 1: cbaed.
x3/4, swapping the last two programs: cbade.
pe/b, swapping programs e and b: ceadb.

In what order are the programs standing after their billion dances?
"""

import sys


def spin(data, size):
    one = data[:-size]
    two = data[-size:]
    data[:size] = two
    data[size:] = one


def exchange(data, x, y):
    a = data[x]
    b = data[y]
    data[x] = b
    data[y] = a


def partner(data, a, b):
    x = data.index(a)
    y = data.index(b)
    exchange(data, x, y)


def solve(line, num=16, dances=0):
    # safe check
    if line[-1] == '\n':
        line = line[:-1]
    # init letters
    letters = [chr(ord('a') + i) for i in range(num)]
    print letters
    # check moves
    moves = line.split(',')
    if dances != 0:
        for _ in range(dances):
            dance(letters, moves)
    else:
        # one billion
        # look for a loop
        seen = list()
        a = 0
        found = False
        while a < 1000 and not found:
            b = 0
            while b < 1000 and not found:
                c = 0
                while c < 1000 and not found:
                    dance(letters, moves)
                    # check
                    temp = ''.join(letters)
                    if temp in seen:
                        found = True
                        print "found", a, b, c
                    else:
                        seen.append(temp)
                    # next loop
                    c += 1
                b += 1
            a += 1
        # compute how many left
        word = ''.join(letters)
        loop_start = seen.index(word)
        loop_length = len(seen) - loop_start
        loop_extra = (1000 * 1000 * 1000 - loop_start) % loop_length
        return seen[loop_start + loop_extra - 1]
    return ''.join(letters)


def dance(letters, moves):
    for move in moves:
        if move[0] == 's':
            spin(letters, int(move[1:]))
        elif move[0] == 'x':
            a, b = move[1:].split('/')
            exchange(letters, int(a), int(b))
        elif move[0] == 'p':
            a, b = move[1:].split('/')
            partner(letters, a, b)
        # print move
        # print letters
    # return letters


assert solve('s1,x3/4,pe/b\n', 5, 1) == 'baedc'
assert solve('s1,x3/4,pe/b\n', 5, 2) == 'ceadb'

if __name__ == '__main__':
    text = open(sys.argv[1]).readlines()[0]
    print solve(text)
