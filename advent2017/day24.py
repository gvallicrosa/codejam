"""
--- Day 24: Electromagnetic Moat ---

The CPU itself is a large, black building surrounded by a bottomless pit. Enormous metal tubes
extend outward from the side of the building at regular intervals and descend down into the void.
There's no way to cross, but you need to get inside.

No way, of course, other than building a bridge out of the magnetic components strewn about nearby.

Each component has two ports, one on each end. The ports come in all different types, and only
matching types can be connected. You take an inventory of the components by their port types (your
puzzle input). Each port is identified by the number of pins it uses; more pins mean a stronger
connection for your bridge. A 3/7 component, for example, has a type-3 port on one side, and a
type-7 port on the other.

Your side of the pit is metallic; a perfect surface to connect a magnetic, zero-pin port. Because
of this, the first port you use must be of type 0. It doesn't matter what type of port you end with;
your goal is just to make the bridge as strong as possible.

The strength of a bridge is the sum of the port types in each component. For example, if your bridge
is made of components 0/3, 3/7, and 7/4, your bridge has a strength of 0+3 + 3+7 + 7+4 = 24.

For example, suppose you had the following components:

0/2
2/2
2/3
3/4
3/5
0/1
10/1
9/10

With them, you could make the following valid bridges:

* 0/1
* 0/1--10/1
* 0/1--10/1--9/10
* 0/2
* 0/2--2/3
* 0/2--2/3--3/4
* 0/2--2/3--3/5
* 0/2--2/2
* 0/2--2/2--2/3
* 0/2--2/2--2/3--3/4
* 0/2--2/2--2/3--3/5

(Note how, as shown by 10/1, order of ports within a component doesn't matter. However, you may
only use each port on a component once.)

Of these bridges, the strongest one is 0/1--10/1--9/10; it has a strength of 0+1 + 1+10 + 10+9 = 31.

What is the strength of the strongest bridge you can make with the components you have available?

--------------------------------------------------

The bridge you've built isn't long enough; you can't jump the rest of the way.

In the example above, there are two longest bridges:

0/2--2/2--2/3--3/4
0/2--2/2--2/3--3/5

Of them, the one which uses the 3/5 component is stronger; its strength is
0+2 + 2+2 + 2+3 + 3+5 = 19.

What is the strength of the longest bridge you can make? If you can make multiple bridges of the
longest length, pick the strongest one.
"""

import copy
import sys


class Component(object):
    def __init__(self, line):
        # clean
        if line[-1] == '\n':
            line = line[:-1]
        # copy
        temp = line.split('/')
        self.one = int(temp[0])
        self.two = int(temp[1])
        self.one_used = False
        self.two_used = False

    def __str__(self):
        return '{}/{}'.format(self.one, self.two)

    def compatible(self, conn):
        if conn == self.one or conn == self.two:
            return True
        return False

    def attach(self, conn):
        if conn == self.one and not self.one_used:
            self.one_used = True
            return True
        if conn == self.two and not self.two_used:
            self.two_used = True
            return True
        raise Exception("unnatachable")

    def reset(self):
        self.one_used = False
        self.two_used = False

    def get_value(self):
        return self.one + self.two

    def get_empty(self):
        if self.one_used and not self.two_used:
            self.two_used = True
            return self.two
        elif self.two_used and not self.one_used:
            self.one_used = True
            return self.one
        else:
            raise Exception("invalid")


def create_bridge(parts, current):
    # current plug
    plug = 0
    if current:
        plug = current[-1].get_empty()
    # valid pieces
    valid = 0
    best = 0
    bridge = None
    for i, piece in enumerate(parts):
        if piece.compatible(plug):
            valid += 1
            # fresh copies
            c = copy.deepcopy(current)
            p = copy.deepcopy(parts)
            # delete used part, attach and put it in the bridge
            r = p.pop(i)
            r.attach(plug)
            c.append(r)
            # solve for the new bridge
            res, b = create_bridge(p, c)
            if res > best:
                best = res
                bridge = b
    # no valid pieces => finished bridge
    if valid == 0:
        # sum all the bridge
        s = 0
        for p in current:
            s += p.get_value()
        # print(s)
        return s, current
    return best, bridge


def solve(lines):
    # create
    comps = list()
    for line in lines:
        comps.append(Component(line))
    # solve
    s, b = create_bridge(comps, list())
    print('--'.join([str(p) for p in b]))
    return s

example = [
    "0/2\n",
    "2/2\n",
    "2/3\n",
    "3/4\n",
    "3/5\n",
    "0/1\n",
    "10/1\n",
    "9/10\n"
]

assert solve(example) == 31


def create_longest_bridge(parts, current):
    # current plug
    plug = 0
    if current:
        plug = current[-1].get_empty()
    # valid pieces
    valid = 0
    best = 0
    longest = 0
    bridge = None
    for i, piece in enumerate(parts):
        if piece.compatible(plug):
            valid += 1
            # fresh copies
            c = copy.deepcopy(current)
            p = copy.deepcopy(parts)
            # delete used part, attach and put it in the bridge
            r = p.pop(i)
            r.attach(plug)
            c.append(r)
            # solve for the new bridge
            res, b = create_longest_bridge(p, c)
            if len(b) > longest:
                best = res
                bridge = b
                longest = len(b)
            elif len(b) == longest:
                if res > best:
                    best = res
                    bridge = b
                    longest = len(b)
    # no valid pieces => finished bridge
    if valid == 0:
        # sum all the bridge
        s = 0
        for p in current:
            s += p.get_value()
        # print(s)
        return s, current
    return best, bridge


def solve2(lines):
    # create
    comps = list()
    for line in lines:
        comps.append(Component(line))
    # solve
    s, b = create_longest_bridge(comps, list())
    print('--'.join([str(p) for p in b]))
    return s

assert solve2(example) == 19

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print(solve2(data))
