"""
--- Day 22: Sporifica Virus ---

Diagnostics indicate that the local grid computing cluster has been contaminated with the
Sporifica Virus. The grid computing cluster is a seemingly-infinite two-dimensional grid of compute
nodes. Each node is either clean or infected by the virus.

To prevent overloading the nodes (which would render them useless to the virus) or detection by
system administrators, exactly one virus carrier moves through the network, infecting or cleaning
nodes as it moves. The virus carrier is always located on a single node in the network (the current
node) and keeps track of the direction it is facing.

To avoid detection, the virus carrier works in bursts; in each burst, it wakes up, does some work,
and goes back to sleep. The following steps are all executed in order one time each burst:

* If the current node is infected, it turns to its right. Otherwise, it turns to its left. (Turning
is done in-place; the current node does not change.)
* If the current node is clean, it becomes infected. Otherwise, it becomes cleaned. (This is done
after the node is considered for the purposes of changing direction.)
* The virus carrier moves forward one node in the direction it is facing.

Diagnostics have also provided a map of the node infection status (your puzzle input). Clean nodes
#. This map only shows the center of the grid; there
are shown as .; infected nodes are shown as
are many more nodes beyond those shown, but none of them are currently infected.

The virus carrier begins in the middle of the map facing up.

For example, suppose you are given a map like this:

..#
#..
...

Then, the middle of the infinite grid looks like this, with the virus carrier's position marked
with [ ]:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . # . . .
. . . #[.]. . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

The virus carrier is on a clean node, so it turns left, infects the node, and moves left:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . # . . .
. . .[#]# . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

The virus carrier is on an infected node, so it turns right, cleans the node, and moves up:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . .[.]. # . . .
. . . . # . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

Four times in a row, the virus carrier finds a clean, infects it, turns left, and moves forward,
ending in the same place and still facing up:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . #[#]. # . . .
. . # # # . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

Now on the same node as before, it sees an infection, which causes it to turn right, clean the
node, and move forward:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . # .[.]# . . .
. . # # # . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

After the above actions, a total of 7 bursts of activity had taken place. Of them, 5 bursts of
activity caused an infection.

After a total of 70, the grid looks like this, with the virus carrier facing up:

. . . . . # # . .
. . . . # . . # .
. . . # . . . . #
. . # . #[.]. . #
. . # . # . . # .
. . . . . # # . .
. . . . . . . . .
. . . . . . . . .

By this time, 41 bursts of activity caused an infection (though most of those nodes have since
been cleaned).

After a total of 10000 bursts of activity, 5587 bursts will have caused an infection.

Given your actual map, after 10000 bursts of activity, how many bursts cause a node to become
infected? (Do not count nodes that begin infected.)

----------------------------------------------------

As you go to remove the virus from the infected nodes, it evolves to resist your attempt.

Now, before it infects a clean node, it will weaken it to disable your defenses. If it encounters
an infected node, it will instead flag the node to be cleaned in the future. So:

* Clean nodes become weakened.
* Weakened nodes become infected.
* Infected nodes become flagged.
* Flagged nodes become clean.

Every node is always in exactly one of the above states.

The virus carrier still functions in a similar way, but now uses the following logic during its
bursts of action:

* Decide which way to turn based on the current node:
  * If it is clean, it turns left.
  * If it is weakened, it does not turn, and will continue moving in the same direction.
  * If it is infected, it turns right.
  * If it is flagged, it reverses direction, and will go back the way it came.
* Modify the state of the current node, as described above.
* The virus carrier moves forward one node in the direction it is facing.

Start with the same map (still using . for clean and # for infected) and still with the virus
carrier starting in the middle and facing up.

Using the same initial state as the previous example, and drawing weakened as W and flagged as F,
the middle of the infinite grid looks like this, with the virus carrier's position again marked
with [ ]:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . # . . .
. . . #[.]. . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

This is the same as before, since no initial nodes are weakened or flagged. The virus carrier is
on a clean node, so it still turns left, instead weakens the node, and moves left:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . # . . .
. . .[#]W . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

The virus carrier is on an infected node, so it still turns right, instead flags the node, and
moves up:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . .[.]. # . . .
. . . F W . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

This process repeats three more times, ending on the previously-flagged node and facing right:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . W W . # . . .
. . W[F]W . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

Finding a flagged node, it reverses direction and cleans the node:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . W W . # . . .
. .[W]. W . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

The weakened node becomes infected, and it continues in the same direction:

. . . . . . . . .
. . . . . . . . .
. . . . . . . . .
. . W W . # . . .
.[.]# . W . . . .
. . . . . . . . .
. . . . . . . . .
. . . . . . . . .

Of the first 100 bursts, 26 will result in infection. Unfortunately, another feature of this
evolved virus is speed; of the first 10000000 bursts, 2511944 will result in infection.

Given your actual map, after 10000000 bursts of activity, how many bursts cause a node to become
infected? (Do not count nodes that begin infected.)
"""

import better_exceptions
import sys
import numpy as np


class Turn(object):
    def __init__(self):
        self.down = np.array([+1, 0])
        self.up = np.array([-1, 0])
        self.right = np.array([0, +1])
        self.left = np.array([0, -1])

    def turn(self, other, turn_right):

        if np.array_equal(self.down, other):
            if turn_right:
                return self.left
            else:
                return self.right
        elif np.array_equal(self.left, other):
            if turn_right:
                return self.up
            else:
                return self.down
        elif np.array_equal(self.up, other):
            if turn_right:
                return self.right
            else:
                return self.left
        elif np.array_equal(self.right, other):
            if turn_right:
                return self.down
            else:
                return self.up

    def turn2(self, other, keep, reverse, turn_right):
        # new turning
        if keep:
            return other
        elif reverse:
            return -other
        # old turning
        if np.array_equal(self.down, other):
            if turn_right:
                return self.left
            else:
                return self.right
        elif np.array_equal(self.left, other):
            if turn_right:
                return self.up
            else:
                return self.down
        elif np.array_equal(self.up, other):
            if turn_right:
                return self.right
            else:
                return self.left
        elif np.array_equal(self.right, other):
            if turn_right:
                return self.down
            else:
                return self.up


class Grid(object):
    def __init__(self, lines):
        # create
        grid = list()
        for line in lines:
            if line[-1] == '\n':
                line = line[:-1]
            grid.append(list(line))
        self.grid = np.array(grid, dtype=str)
        # init
        self.move = np.array([-1, 0])
        self.pos = np.array([self.grid.shape[0] / 2, self.grid.shape[1] / 2])
        self.turn = Turn()
        self.infect = 0

    def increase_grid(self):
        size = self.grid.shape + np.array([2, 2])
        temp = np.zeros(size, dtype=str)
        temp[:, :] = '.'
        temp[1:-1, 1:-1] = self.grid
        self.grid = temp
        self.pos += np.array([1, 1])

    def advance(self):
        # turn
        v = self.grid[self.pos[0], self.pos[1]]
        self.move = self.turn.turn(self.move, v == '#')
        # infect/clean
        if v == '#':
            v = '.'
        else:
            v = '#'
            self.infect += 1
        self.grid[self.pos[0], self.pos[1]] = v
        # advance
        future = self.pos + self.move
        if (0 <= future[0] < self.grid.shape[0]) and (0 <= future[1] < self.grid.shape[1]):
            pass
        else:
            self.increase_grid()
        self.pos += self.move

    def show(self):
        rows = '\n'.join([' '.join(self.grid[r, :].tolist())
                          for r in range(self.grid.shape[0])])
        print rows
        print self.pos
        print ''

    def advance2(self):
        event = {
            '.': 'W',
            'W': '#',
            '#': 'F',
            'F': '.'
        }
        # turn
        v = self.grid[self.pos[0], self.pos[1]]
        self.move = self.turn.turn2(self.move, v == 'W', v == 'F', v == '#')
        # infect/clean
        v = event[v]
        if v == '#':
            self.infect += 1
        self.grid[self.pos[0], self.pos[1]] = v
        # advance
        future = self.pos + self.move
        if (0 <= future[0] < self.grid.shape[0]) and (0 <= future[1] < self.grid.shape[1]):
            pass
        else:
            self.increase_grid()
        self.pos += self.move


def solve(lines, bursts=10000):
    # create grid
    grid = Grid(lines)
    # grid.show()
    # bursts
    for _ in range(bursts):
        grid.advance()
    # grid.show()
    print grid.infect
    return grid.infect


example = [
    "..#\n",
    "#..\n",
    "...\n"
]

assert solve(example, bursts=7) == 5
assert solve(example, bursts=70) == 41
assert solve(example) == 5587


def solve2(lines, bursts=10 * 1000 * 1000):
    # create grid
    grid = Grid(lines)
    # grid.show()
    # bursts
    i = 0
    while i < bursts:
        grid.advance2()
        i += 1
    # grid.show()
    print grid.infect
    return grid.infect

assert solve2(example, bursts=7) == 1
assert solve2(example, bursts=100) == 26
assert solve2(example) == 2511944

if __name__ == '__main__':
    data = open(sys.argv[1]).readlines()
    print solve2(data)
