use std::env;
use std::fs;

#[derive(PartialEq, Clone, Copy)]
pub enum State {
    Empty,
    Occupied,
    Floor,
    Invalid,
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut chairs = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            let row: Vec<_> = line
                .chars()
                .map(|c| match c {
                    '.' => State::Floor,
                    '#' => State::Occupied,
                    'L' => State::Empty,
                    _ => State::Invalid,
                })
                .collect();
            chairs.push(row);
        }
    }
    solve_simple(&chairs);
    solve_extra(&chairs);
}

fn solve_simple(map: &Vec<Vec<State>>) {
    // copy map
    let mut chairs = Vec::new();
    for row in map {
        let mut data = Vec::new();
        for v in row {
            data.push(*v);
        }
        chairs.push(data);
    }
    // simulate
    let mut occ_prev = 0; // always zero in the first one
    let mut occ = 1; // always zero in the first one
    while occ_prev != occ {
        // update and reset
        occ_prev = occ;
        occ = 0;

        // explore
        let mut new_chairs = Vec::new();
        let rows = chairs.len();
        let cols = chairs[0].len();
        for r in 0..rows {
            let mut new_row = Vec::new();
            for c in 0..cols {
                // check neighbors
                let mut neigh_occ = 0;
                // top-left
                if (r > 0) && (c > 0) && chairs[r - 1][c - 1] == State::Occupied {
                    neigh_occ += 1;
                }
                // top
                if (r > 0) && chairs[r - 1][c] == State::Occupied {
                    neigh_occ += 1;
                }
                // top-right
                if (r > 0) && (c < cols - 1) && chairs[r - 1][c + 1] == State::Occupied {
                    neigh_occ += 1;
                }
                // left
                if (c > 0) && chairs[r][c - 1] == State::Occupied {
                    neigh_occ += 1;
                }
                // right
                if (c < cols - 1) && chairs[r][c + 1] == State::Occupied {
                    neigh_occ += 1;
                }
                // bottom-left
                if (r < rows - 1) && (c > 0) && chairs[r + 1][c - 1] == State::Occupied {
                    neigh_occ += 1;
                }
                // bottom
                if (r < rows - 1) && chairs[r + 1][c] == State::Occupied {
                    neigh_occ += 1;
                }
                // bottom-right
                if (r < rows - 1) && (c < cols - 1) && chairs[r + 1][c + 1] == State::Occupied {
                    neigh_occ += 1;
                }

                // depending on our state
                let v = match chairs[r][c] {
                    State::Occupied => {
                        if neigh_occ >= 4 {
                            State::Empty
                        } else {
                            occ += 1;
                            State::Occupied
                        }
                    }
                    State::Empty => {
                        if neigh_occ == 0 {
                            occ += 1;
                            State::Occupied
                        } else {
                            State::Empty
                        }
                    }
                    _ => State::Floor,
                };
                new_row.push(v);
            }
            new_chairs.push(new_row);
        }

        // update map
        chairs = new_chairs;
    }
    println!("{} occupied simple", occ);
}

fn check_direction(map: &Vec<Vec<State>>, r: usize, c: usize, inc_r: i32, inc_c: i32) -> usize {
    let mut r = r as i32;
    let mut c = c as i32;
    let rows = map.len() as i32;
    let cols = map[0].len() as i32;
    while true {
        // new position
        r += inc_r;
        c += inc_c;
        // valid position
        if (r >= 0) && (r < rows) && (c >= 0) && (c < cols) {
            let v = map[r as usize][c as usize];
            if v == State::Empty {
                return 0;
            } else if v == State::Occupied {
                return 1;
            }
        } else {
            // edge with nothing found
            return 0;
        }
    }
    return 0; // impossible to reach
}

fn solve_extra(map: &Vec<Vec<State>>) {
    // copy map
    let mut chairs = Vec::new();
    for row in map {
        let mut data = Vec::new();
        for v in row {
            data.push(*v);
        }
        chairs.push(data);
    }
    // simulate
    let mut occ_prev = 0; // always zero in the first one
    let mut occ = 1; // always zero in the first one
    while occ_prev != occ {
        // update and reset
        occ_prev = occ;
        occ = 0;

        // explore
        let mut new_chairs = Vec::new();
        let rows = chairs.len();
        let cols = chairs[0].len();
        for r in 0..rows {
            let mut new_row = Vec::new();
            for c in 0..cols {
                // check neighbors directions
                let mut neigh_occ = 0;
                // print!("{} {} => ", r, c);
                neigh_occ += check_direction(&chairs, r, c, -1, -1); // top-left
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, -1, 0); // top
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, -1, 1); // top-right
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, 0, -1); // left
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, 0, 1); // right
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, 1, -1); // bottom-left
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, 1, 0); // bottom
                // print!("{} ", neigh_occ);
                neigh_occ += check_direction(&chairs, r, c, 1, 1); // bottom-right
                // println!("{} ", neigh_occ);
                // println!("{} {} => {}", r, c, neigh_occ);

                // depending on our state
                let v = match chairs[r][c] {
                    State::Occupied => {
                        if neigh_occ >= 5 {
                            State::Empty
                        } else {
                            occ += 1;
                            State::Occupied
                        }
                    }
                    State::Empty => {
                        if neigh_occ == 0 {
                            occ += 1;
                            State::Occupied
                        } else {
                            State::Empty
                        }
                    }
                    _ => State::Floor,
                };
                new_row.push(v);
            }
            new_chairs.push(new_row);
        }

        // update map
        chairs = new_chairs;

        // debug
        // for r in 0..chairs.len() {
        //     for c in 0..chairs[0].len() {
        //         match chairs[r][c] {
        //             State::Occupied => print!("#"),
        //             State::Empty => print!("L"),
        //             _ => print!("."),
        //         }
        //     }
        //     println!("");
        // }
        // println!("");
        // println!("");
    }
    println!("{} occupied extra", occ);
}
