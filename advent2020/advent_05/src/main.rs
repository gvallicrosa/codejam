use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    // 0-6 are B/F => 0-127 row
    // F 0-63
    // FF 0-31 => F=0, B=1 in binary
    //
    // 7-9 are L/R => 0-7 col
    // L=0, R=1
    //
    // ID = row * 8 + col
    // maxID = 127 * 8 + 7 = 1023

    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut highest = 0;
    let mut found = [false; 1024];
    for line in contents.split("\n") {
        // for line in &["BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"] {
        if line.len() > 0 {
            let row = &line[0..7];
            let mut rownum = 0;
            for i in 0..7 {
                if row.chars().nth(6 - i).unwrap() == 'B' {
                    rownum += 2u32.pow(i as u32);
                }
            }
            let col = &line[7..10];
            let mut colnum = 0;
            for i in 0..3 {
                if col.chars().nth(2 - i).unwrap() == 'R' {
                    colnum += 2u32.pow(i as u32);
                }
            }
            let id = rownum * 8 + colnum;
            if id > highest {
                highest = id;
            }
            found[id as usize] = true;
            // println!("row {} col {} id {}", rownum, colnum, id);
        }
    }
    println!("{} highest", highest);
    for (i, v) in found.iter().enumerate() {
        if !v {
            println!("{} not found", i);
        }
    }
}
