use std::collections::HashMap;
use std::env;
use std::fs;

// #[derive(PartialEq, Copy, Clone)]
#[derive(PartialEq, Clone, Copy)]
pub enum Value {
    On,
    Off,
}

pub enum State {
    Title,
    Image,
}

#[derive(Clone, Copy)]
pub enum Compatibility {
    None,
    Top,
    Bottom,
    Left,
    Right,
}

pub struct Image {
    id: usize,
    top: Vec<Value>,
    bottom: Vec<Value>,
    left: Vec<Value>,
    right: Vec<Value>,
}

impl Image {
    fn new(id: usize, data: Vec<Vec<Value>>) -> Self {
        // borders
        let t = data[0].to_vec();
        let b = data[data.len() - 1].to_vec();
        let l: Vec<_> = data.iter().map(|row| row[0]).collect();
        let r: Vec<_> = data.iter().map(|row| row[row.len() - 1]).collect();
        // create
        Image {
            id: id,
            top: t,
            left: l,
            right: r,
            bottom: b,
        }
    }
}

fn compatible(li: &Vec<Value>, ri: &Vec<Value>) -> bool {
    // normal
    let mut valid = true;
    for (l, r) in li.iter().zip(ri.iter()) {
        if l != r {
            valid = false;
            break;
        }
    }
    if valid {
        return true;
    }
    // reverse one
    for (l, r) in li.iter().zip(ri.iter().rev()) {
        if l != r {
            return false;
        }
    }
    return true;
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut state = State::Title;
    let mut images = Vec::new();
    let mut id = 0;
    let mut data = Vec::new();
    for line in contents.split("\n") {
        if line.len() == 0 {
            // end of image
            images.push(Image::new(id, data.to_vec()));
            data.clear();
            state = State::Title;
        } else {
            match state {
                State::Title => {
                    id = line
                        .split("Tile ")
                        .nth(1)
                        .unwrap()
                        .split(":")
                        .nth(0)
                        .unwrap()
                        .parse::<usize>()
                        .unwrap();
                    state = State::Image;
                }
                State::Image => {
                    let row: Vec<_> = line
                        .chars()
                        .map(|c| match c {
                            '#' => Value::On,
                            '.' => Value::Off,
                            _ => panic!(),
                        })
                        .collect();
                    data.push(row);
                }
            }
        }
    }
    println!("{} images", images.len());

    // find compatibilities
    let mut compat: HashMap<usize, Vec<(usize, Compatibility)>> = HashMap::new();
    for i in 0..images.len() - 1 {
        for j in i + 1..images.len() {
            // check compatible
            for k in 0..4 {
                // candidates left
                let (left, cleft) = match k {
                    0 => (&images[i].top, Compatibility::Top),
                    1 => (&images[i].bottom, Compatibility::Bottom),
                    2 => (&images[i].left, Compatibility::Left),
                    3 => (&images[i].right, Compatibility::Right),
                    _ => panic!(),
                };
                // candidates right
                for l in 0..4 {
                    let (right, cright) = match l {
                        0 => (&images[j].top, Compatibility::Top),
                        1 => (&images[j].bottom, Compatibility::Bottom),
                        2 => (&images[j].left, Compatibility::Left),
                        3 => (&images[j].right, Compatibility::Right),
                        _ => panic!(),
                    };

                    if compatible(left, right) {
                        // write in both
                        compat
                            .entry(images[i].id)
                            .or_insert_with(Vec::new)
                            .push((images[j].id, cleft)); // with XX from side
                        compat
                            .entry(images[j].id)
                            .or_insert_with(Vec::new)
                            .push((images[i].id, cright));
                    }
                }
            }
        }
    }
    // check the ones with less compat
    let mut compat2 = Vec::new();
    for (k, vs) in compat {
        let mut t = 0;
        let mut b = 0;
        let mut l = 0;
        let mut r = 0;
        for (_, c) in vs.iter() {
            match c {
                Compatibility::Top => t += 1,
                Compatibility::Bottom => b += 1,
                Compatibility::Left => l += 1,
                Compatibility::Right => r += 1,
                _ => (),
            }
        }
        let sum = ((t > 0) as usize) + ((b > 0) as usize) + ((l > 0) as usize) + ((r > 0) as usize);
        if sum == 2 {
            compat2.push(k);
        }
        println!("{}: {} {} {} {} => {}", k, t, b, l, r, sum);
    }
    println!("\n corner candidates:");
    let mut result = 1;
    for v in compat2.iter() {
        println!("- {}", v);
        result *= v;
    }
    println!("\n{} total", result);
}
