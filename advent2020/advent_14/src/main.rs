use std::collections::HashMap;
use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut mask = Vec::new();
    let mut mem = HashMap::new();
    let mut mem_v2 = HashMap::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            if line.matches("mask").count() > 0 {
                // change mask skipping "mask = "
                mask = line.split(" = ").nth(1).unwrap().chars().collect();
            } else if line.matches("mem").count() > 0 {
                // write value
                let address = line
                    .split("[")
                    .nth(1)
                    .unwrap()
                    .split("]")
                    .nth(0)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();
                let value = line.split(" = ").nth(1).unwrap().parse::<usize>().unwrap();
                let bin: Vec<_> = format!("{:036b}", value).chars().collect();
                let bin_v2: Vec<_> = format!("{:036b}", address).chars().collect();
                // actually write
                let mut write = Vec::new();
                let mut write_v2 = Vec::new();
                for (i, m) in mask.iter().enumerate() {
                    match m {
                        'X' => {
                            write.push(bin[i]);
                            write_v2.push('X');
                        }
                        '1' => {
                            write.push('1');
                            write_v2.push('1');
                        }
                        '0' => {
                            write.push('0');
                            write_v2.push(bin_v2[i]);
                        }
                        _ => (),
                    }
                }
                // check if value already in memory
                if mem.contains_key(&address) {
                    *mem.get_mut(&address).unwrap() = write;
                } else {
                    mem.insert(address, write);
                }
                // v2 check and write to all possible addresses
                let mut addrs = Vec::new();
                addrs.push(0); // initial counter
                for (i, v) in write_v2.iter().rev().enumerate() {
                    let one = 2usize.pow(i as u32); // one value
                    let mut extend = Vec::new(); // extend values to track in branches
                    for addr in addrs.iter_mut() {
                        match v {
                            '1' => {
                                *addr += one;
                            }
                            'X' => {
                                // branch
                                extend.push(*addr); // branch with zero sum
                                *addr += one; // branch with one sum
                            }
                            _ => (),
                        }
                    }
                    // extend vals to track
                    if extend.len() > 0 {
                        addrs.extend(extend);
                    }
                }
                // v2 write value in all adresses
                for &addr in addrs.iter() {
                    if mem_v2.contains_key(&addr) {
                        *mem_v2.get_mut(&addr).unwrap() = value;
                    } else {
                        mem_v2.insert(addr, value);
                    }
                }
            }
        }
    }
    // compute sum
    let mut totalsum = 0;
    for (_address, values) in mem.iter() {
        let mut val = 0;
        for (i, v) in values.iter().rev().enumerate() {
            val += match v {
                '1' => 2usize.pow(i as u32),
                _ => 0,
            }
        }
        totalsum += val;
    }
    println!("{} total sum", totalsum);

    // compute sum v2
    let mut totalsum = 0;
    for (_address, value) in mem_v2.iter() {
        totalsum += value;
    }
    println!("{} total sum v2", totalsum);
}
