use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let alphabet: Vec<char> = "abcdefghijklmnopqrstuvwxyz".chars().collect();
    let mut found: Vec<bool> = alphabet.iter().map(|_v| false).collect();
    let mut count = 0;
    // extra
    let mut found_extra: Vec<usize> = alphabet.iter().map(|_v| 0).collect();
    let mut count_extra = 0;
    let mut people = 0;
    for line in contents.split("\n") {
        // passport separator
        if line.len() == 0 {
            // check old
            for v in found.iter() {
                if *v {
                    count += 1;
                }
            }
            for v in found_extra.iter() {
                if *v == people {
                    count_extra += 1;
                }
            }
            // new
            found = alphabet.iter().map(|_v| false).collect();
            found_extra = alphabet.iter().map(|_v| 0).collect();
            people = 0;
        } else {
            // add person
            people += 1;
            // check for substrings
            for c in line.chars() {
                for (n, v) in alphabet.iter().enumerate() {
                    if *v == c {
                        found[n] = true;
                        found_extra[n] += 1;
                        break;
                    }
                }
            }
        }
    }
    // initial
    println!("{} count", count);
    println!("{} count_extra", count_extra);
}
