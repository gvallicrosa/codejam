use std::collections::VecDeque;
use std::env;
use std::fs;

pub enum State {
    Player1,
    Player2,
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut deck1 = VecDeque::new();
    let mut deck2 = VecDeque::new();
    let mut state = State::Player1;
    for line in contents.split("\n") {
        if line.len() > 0 {
            if line.matches("Player").count() > 0 {
                // check headers
                if line.matches("Player 2:").count() > 0 {
                    state = State::Player2;
                }
            } else {
                // process cards
                let num = line.parse::<usize>().unwrap();
                match state {
                    State::Player1 => deck1.push_back(num),
                    State::Player2 => deck2.push_back(num),
                }
            }
        }
    }
    // save a copy
    let deck1c = deck1.clone();
    let deck2c = deck2.clone();

    // play normal
    let mut round = 0;
    while deck1.len() != 0 && deck2.len() != 0 {
        // take first card
        let c1 = deck1.pop_front().unwrap();
        let c2 = deck2.pop_front().unwrap();
        // wins the biggest
        if c1 > c2 {
            // 1 wins
            deck1.push_back(c1);
            deck1.push_back(c2);
        } else {
            // 2 wins
            deck2.push_back(c2);
            deck2.push_back(c1);
        }
        // count round
        round += 1;
    }
    println!("{} rounds", round);

    // score
    let mut score = 0;
    let mut index = 1;
    let mut deck = if deck1.len() != 0 { deck1 } else { deck2 };
    while deck.len() > 0 {
        score += index * deck.pop_back().unwrap();
        index += 1;
    }
    println!("{} score", score);

    // play recursive
    let (winner, mut deck) = recurse_combat(deck1c, deck2c);
    match winner {
        State::Player1 => println!("player 1 wins recursive"),
        State::Player2 => println!("player 2 wins recursive"),
    }

    // score recursive
    let mut score = 0;
    let mut index = 1;
    while deck.len() > 0 {
        score += index * deck.pop_back().unwrap();
        index += 1;
    }
    println!("{} score recursive", score);
}

fn recurse_combat(mut d1: VecDeque<usize>, mut d2: VecDeque<usize>) -> (State, VecDeque<usize>) {
    // memory
    let mut situations: Vec<(Vec<_>, Vec<_>)> = Vec::new();
    // play this game
    // println!("game:\n  {:?}\n  {:?}", d1, d2);
    while d1.len() != 0 && d2.len() != 0 {
        // check situation already happened
        let v1: Vec<_> = d1.iter().map(|v| *v).collect();
        let v2: Vec<_> = d2.iter().map(|v| *v).collect();
        for (s1, s2) in situations.iter() {
            // check sizes
            if (s1.len() != v1.len()) || (s2.len() != v2.len()) {
                continue;
            }
            // check v1 against s1
            let mut ok = true;
            for (s, v) in s1.iter().zip(v1.iter()) {
                if s != v {
                    ok = false;
                    break;
                }
            }
            // check v2 against s2
            for (s, v) in s2.iter().zip(v2.iter()) {
                if s != v {
                    ok = false;
                    break;
                }
            }
            // situation already happened
            if ok {
                // println!("RECURSIVE WIN");
                return (State::Player1, d1);
            }
        }
        // add this situation
        situations.push((v1, v2));

        // take first card
        let c1 = d1.pop_front().unwrap();
        let c2 = d2.pop_front().unwrap();
        // can we recurse?
        if c1 <= d1.len() && c2 <= d2.len() {
            let d1c: VecDeque<_> = d1.iter().take(c1).map(|v| *v).collect();
            let d2c: VecDeque<_> = d2.iter().take(c2).map(|v| *v).collect();
            match recurse_combat(d1c, d2c).0 {
                State::Player1 => {
                    // 1 wins
                    d1.push_back(c1);
                    d1.push_back(c2);
                }
                State::Player2 => {
                    // 2 wins
                    d2.push_back(c2);
                    d2.push_back(c1);
                }
            }
        } else {
            // wins the biggest
            if c1 > c2 {
                // 1 wins
                d1.push_back(c1);
                d1.push_back(c2);
            } else {
                // 2 wins
                d2.push_back(c2);
                d2.push_back(c1);
            }
        }
    }
    // check winner
    if d1.len() != 0 {
        return (State::Player1, d1);
    }
    return (State::Player2, d2);
}
