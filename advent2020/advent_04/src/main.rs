use std::env;
use std::fs;

struct Passport {
    byr: bool, // birth year
    iyr: bool, // issue year
    eyr: bool, // expiration year
    hgt: bool, // height
    hcl: bool, // hair color
    ecl: bool, // eye color
    pid: bool, // passport id
    cid: bool, // country id (not needed)
}

impl Passport {
    fn new() -> Self {
        Passport {
            byr: false,
            iyr: false,
            eyr: false,
            hgt: false,
            hcl: false,
            ecl: false,
            pid: false,
            cid: false,
        }
    }
    fn valid(&self) -> bool {
        self.byr && self.iyr && self.eyr && self.hgt && self.hcl && self.ecl && self.pid
    }
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut pass = Passport::new();
    let mut pass_extra = Passport::new();
    let mut valid = 0;
    let mut valid_extra = 0;
    for line in contents.split("\n") {
        // passport separator
        if line.len() == 0 {
            // check old
            if pass.valid() {
                valid += 1;
            }
            if pass_extra.valid() {
                valid_extra += 1;
            }
            // new passport
            pass = Passport::new();
            pass_extra = Passport::new();
        } else {
            // check for substrings
            if line.matches("byr:").count() > 0 {
                pass.byr = true;
                let val = line
                    .split("byr:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();
                if (1920 <= val) && (val <= 2002) {
                    pass_extra.byr = true;
                }
            }
            if line.matches("iyr:").count() > 0 {
                pass.iyr = true;
                let val = line
                    .split("iyr:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();
                if (2010 <= val) && (val <= 2020) {
                    pass_extra.iyr = true;
                }
            }
            if line.matches("eyr:").count() > 0 {
                pass.eyr = true;
                let val = line
                    .split("eyr:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();
                if (2020 <= val) && (val <= 2030) {
                    pass_extra.eyr = true;
                }
            }
            if line.matches("hgt:").count() > 0 {
                pass.hgt = true;
                let val = line
                    .split("hgt:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap();
                let size = val.len();
                if val[size - 2..size].matches("cm").count() > 0 {
                    let val = val.split("cm").nth(0).unwrap();
                    if val.chars().all(char::is_numeric) {
                        let val = val.parse::<usize>().unwrap();
                        if (150 <= val) && (val <= 193) {
                            pass_extra.hgt = true;
                        }
                    }
                } else if val[size - 2..size].matches("in").count() > 0 {
                    let val = val.split("in").nth(0).unwrap();
                    if val.chars().all(char::is_numeric) {
                        let val = val.parse::<usize>().unwrap();
                        if (59 <= val) && (val <= 76) {
                            pass_extra.hgt = true;
                        }
                    }
                }
            }
            if line.matches("hcl:").count() > 0 {
                pass.hcl = true;
                let val = line
                    .split("hcl:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap();
                if val.chars().nth(0).unwrap() == '#' {
                    if val.len() == 7 {
                        let chars_are_hex: Vec<bool> = val[1..]
                            .chars()
                            .map(|c| {
                                c.is_numeric()
                                    || c == 'a'
                                    || c == 'b'
                                    || c == 'c'
                                    || c == 'd'
                                    || c == 'e'
                                    || c == 'f'
                            })
                            .collect();
                        if !chars_are_hex.contains(&false) {
                            pass_extra.hcl = true;
                        }
                    }
                }
            }
            if line.matches("ecl:").count() > 0 {
                pass.ecl = true;
                let val = line
                    .split("ecl:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap();
                if (val == "amb")
                    || (val == "blu")
                    || (val == "brn")
                    || (val == "gry")
                    || (val == "grn")
                    || (val == "hzl")
                    || (val == "oth")
                {
                    pass_extra.ecl = true;
                }
            }
            if line.matches("pid:").count() > 0 {
                pass.pid = true;
                let val = line
                    .split("pid:")
                    .nth(1)
                    .unwrap()
                    .split(" ")
                    .nth(0)
                    .unwrap();
                if val.len() == 9 {
                    if val.chars().all(char::is_numeric) {
                        pass_extra.pid = true;
                    }
                }
            }
            if line.matches("cid:").count() > 0 {
                pass.cid = true;
                pass_extra.cid = true;
            }
        }
    }
    // initial
    println!("{} valid", valid);
    println!("{} valid extra", valid_extra);
}
