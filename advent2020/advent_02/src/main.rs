use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut valid = 0;
    let mut valid_extra = 0;
    for line in contents.split("\n") {
        if line.len() > 0 {
            let low = line.split("-").next().unwrap();
            let high = line.split(" ").next().unwrap().rsplit("-").next().unwrap();
            let letter = line.split(":").next().unwrap().rsplit(" ").next().unwrap();
            let password = line.rsplit(" ").next().unwrap();
            let low = low.parse::<usize>().unwrap();
            let high = high.parse::<usize>().unwrap();
            // letter occurences between limits
            let c = password.matches(letter).count();
            if (low <= c) && (c <= high) {
                valid += 1;
            }
            // one of the two positions contains the letter (index from 1)
            let pos0 = low-1;
            let pos1 = high-1;
            let letter = letter.as_bytes()[0] as char;
            let mut count = 0;
            if password.chars().nth(pos0).unwrap() == letter {
                count +=1;
            }
            if password.chars().nth(pos1).unwrap() == letter {
                count +=1;
            }
            if count == 1 {
                valid_extra += 1;
            }
        }
    }
    println!("{} valid passwords", valid);
    println!("{} valid extra passwords", valid_extra);
}
