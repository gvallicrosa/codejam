use std::collections::HashMap;
use std::env;
use std::fs;

#[derive(Clone, Debug)]
pub enum State {
    Black,
    White,
}

#[derive(PartialEq)]
pub enum Direction {
    West,
    East,
    SoEa,
    SoWe,
    NoEa,
    NoWe,
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub struct Position {
    x: i32,
    y: i32,
    z: i32,
}

impl Position {
    pub fn neighbour(&self, d: Direction) -> Self {
        match d {
            Direction::West => Position {
                x: self.x - 1,
                y: self.y + 1,
                z: self.z + 0,
            },
            Direction::East => Position {
                x: self.x + 1,
                y: self.y - 1,
                z: self.z + 0,
            },
            Direction::SoEa => Position {
                x: self.x + 0,
                y: self.y - 1,
                z: self.z + 1,
            },
            Direction::SoWe => Position {
                x: self.x - 1,
                y: self.y + 0,
                z: self.z + 1,
            },
            Direction::NoWe => Position {
                x: self.x + 0,
                y: self.y + 1,
                z: self.z - 1,
            },
            Direction::NoEa => Position {
                x: self.x + 1,
                y: self.y + 0,
                z: self.z - 1,
            },
        }
    }
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut tiles: HashMap<Position, State> = HashMap::new();
    let mut black = 0;
    for line in contents.split("\n") {
        if line.len() > 0 {
            let mut pos = Position { x: 0, y: 0, z: 0 };
            let mut chars = line.chars();
            // process line
            while true {
                match chars.next() {
                    // coordinates => https://www.redblobgames.com/grids/hexagons/
                    Some(c) => match c {
                        'w' => pos = pos.neighbour(Direction::West),
                        'e' => pos = pos.neighbour(Direction::East),
                        's' => {
                            // check next
                            match chars.next().unwrap() {
                                'e' => pos = pos.neighbour(Direction::SoEa),
                                'w' => pos = pos.neighbour(Direction::SoWe),
                                _ => panic!(),
                            }
                        }
                        'n' => {
                            // check next
                            match chars.next().unwrap() {
                                'w' => pos = pos.neighbour(Direction::NoWe),
                                'e' => pos = pos.neighbour(Direction::NoEa),
                                _ => panic!(),
                            }
                        }
                        // unreachable
                        _ => panic!(),
                    },
                    None => break,
                }
            }
            // change pointed tile
            // print!("({:>3}, {:>3}, {:>3}) to ", pos.x, pos.y, pos.z);
            let state = tiles.entry(pos).or_insert(State::White);
            match state {
                State::White => {
                    *state = State::Black;
                    black += 1;
                    // println!("black");
                }
                State::Black => {
                    *state = State::White;
                    black -= 1;
                    // println!("white");
                }
            }

            // add border around each tile
            tiles
                .entry(pos.neighbour(Direction::East))
                .or_insert(State::White);
            tiles
                .entry(pos.neighbour(Direction::West))
                .or_insert(State::White);
            tiles
                .entry(pos.neighbour(Direction::SoEa))
                .or_insert(State::White);
            tiles
                .entry(pos.neighbour(Direction::SoWe))
                .or_insert(State::White);
            tiles
                .entry(pos.neighbour(Direction::NoEa))
                .or_insert(State::White);
            tiles
                .entry(pos.neighbour(Direction::NoWe))
                .or_insert(State::White);
            // println!("{:?}", tiles.get(&pos_e));
        }
    }
    println!("{} black tiles", black);

    // rules that change it
    let mut day = 0;
    let mut newtiles: HashMap<Position, State> = HashMap::new();
    while day < 100 {
        // next
        day += 1;
        // go over all
        for (pos, s) in &tiles {
            // neighbours
            let pos_e = pos.neighbour(Direction::East);
            let pos_w = pos.neighbour(Direction::West);
            let pos_se = pos.neighbour(Direction::SoEa);
            let pos_sw = pos.neighbour(Direction::SoWe);
            let pos_ne = pos.neighbour(Direction::NoEa);
            let pos_nw = pos.neighbour(Direction::NoWe);
            // check black neighbours
            let mut black_neigh = 0;
            if let Some(State::Black) = tiles.get(&pos_e) {
                black_neigh += 1;
            }
            if let Some(State::Black) = tiles.get(&pos_w) {
                black_neigh += 1;
            }
            if let Some(State::Black) = tiles.get(&pos_se) {
                black_neigh += 1;
            }
            if let Some(State::Black) = tiles.get(&pos_nw) {
                black_neigh += 1;
            }
            if let Some(State::Black) = tiles.get(&pos_sw) {
                black_neigh += 1;
            }
            if let Some(State::Black) = tiles.get(&pos_ne) {
                black_neigh += 1;
            }
            // print!("{:?} neigh: {} => ", s, black_neigh);
            // what to do with current
            match s {
                State::Black => {
                    // black with 0 or >2 black => white
                    if (black_neigh == 0) || (black_neigh > 2) {
                        *newtiles.entry(*pos).or_insert(State::White) = State::White;
                        black -= 1;
                    // println!(" to white");
                    } else {
                        *newtiles.entry(*pos).or_insert(State::Black) = State::Black;
                        // println!(" keep black");
                    }
                }
                State::White => {
                    // white with 2 black => black
                    if black_neigh == 2 {
                        *newtiles.entry(*pos).or_insert(State::Black) = State::Black;
                        black += 1;
                    // println!(" to black");
                    } else {
                        *newtiles.entry(*pos).or_insert(State::White) = State::White;
                        // println!(" keep white");
                    }
                }
            }

            // insert tiles in new map
            newtiles.entry(pos_e).or_insert(State::White);
            newtiles.entry(pos_w).or_insert(State::White);
            newtiles.entry(pos_se).or_insert(State::White);
            newtiles.entry(pos_nw).or_insert(State::White);
            newtiles.entry(pos_sw).or_insert(State::White);
            newtiles.entry(pos_ne).or_insert(State::White);
        }

        // copy
        tiles = newtiles.clone();

        // debug
        // println!("day: {:>3} black tiles: {}", day, black);
    }
    println!("{} black tiles day {}", black, day);
}
