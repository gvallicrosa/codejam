use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut map = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            map.push(line.as_bytes());
        }
    }
    // initial
    println!("{} trees", check_movement(&map, 3, 1));
    // second
    println!("{} multiplied",
    check_movement(&map, 1, 1) *
    check_movement(&map, 3, 1) *
    check_movement(&map, 5, 1) *
    check_movement(&map, 7, 1) *
    check_movement(&map, 1, 2)
);
}

fn check_movement(map: &Vec<&[u8]>, inc_col: usize, inc_row: usize) -> usize {
    let mut trees = 0;
    let mut row = 0;
    let mut col = 0;
    let cols = map[0].len();
    while row < map.len() {
        // tree in position?
        let v = map[row][col] as char;
        if v == '#' {
            trees += 1;
        }
        // next position
        row += inc_row;
        col = (col + inc_col) % cols;
    }
    return trees;
}
