use std::env;
use std::fs;

#[derive(PartialEq, Clone, Copy)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn next(&self) -> Direction {
        return match self {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North,
        };
    }
    fn right(&self, value: i64) -> Direction {
        if value == 90 {
            return self.next();
        } else if value == 180 {
            return self.next().next();
        }
        // 270
        return self.next().next().next();
    }
    fn left(&self, value: i64) -> Direction {
        if value == 90 {
            return self.next().next().next();
        } else if value == 180 {
            return self.next().next();
        }
        // 270
        return self.next();
    }
}

struct Position {
    north: i64,
    east: i64,
    direction: Direction,
}

impl Position {
    fn new() -> Self {
        Position {
            north: 0,
            east: 0,
            direction: Direction::East,
        }
    }
    fn advance(&mut self, command: char, value: i64) {
        match command {
            'N' => self.north += value,
            'S' => self.north -= value,
            'E' => self.east += value,
            'W' => self.east -= value,
            'L' => self.direction = self.direction.left(value),
            'R' => self.direction = self.direction.right(value),
            'F' => match self.direction {
                Direction::North => self.north += value,
                Direction::South => self.north -= value,
                Direction::East => self.east += value,
                Direction::West => self.east -= value,
            },
            _ => (),
        }
    }
}

struct Waypoint {
    w_north: i64, // waypoint
    w_east: i64,
    s_north: i64, // ship
    s_east: i64,
}

impl Waypoint {
    fn new() -> Self {
        Waypoint {
            w_north: 1,
            w_east: 10,
            s_north: 0,
            s_east: 0,
        }
    }
    fn advance(&mut self, command: char, value: i64) {
        match command {
            // move waypoint (always is relative to ship)
            'N' => self.w_north += value,
            'S' => self.w_north -= value,
            'E' => self.w_east += value,
            'W' => self.w_east -= value,
            // rotate waypoint around ship
            'R' => {
                let n = self.w_north;
                let e = self.w_east;
                match value {
                    90 => {
                        self.w_north = -e;
                        self.w_east = n;
                    }
                    180 => {
                        self.w_north = -n;
                        self.w_east = -e;
                    }
                    270 => {
                        self.w_north = e;
                        self.w_east = -n;
                    }
                    _ => (),
                }
            }
            'L' => {
                let n = self.w_north;
                let e = self.w_east;
                match value {
                    90 => {
                        self.w_north = e;
                        self.w_east = -n;
                    }
                    180 => {
                        self.w_north = -n;
                        self.w_east = -e;
                    }
                    270 => {
                        self.w_north = -e;
                        self.w_east = n;
                    }
                    _ => (),
                }
            }
            // move to the waypoint n times
            'F' => {
                self.s_north += value * self.w_north;
                self.s_east += value * self.w_east;
            }
            _ => (),
        }
    }
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut position = Position::new();
    let mut waypoint = Waypoint::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            // first char is action
            let action = line.chars().nth(0).unwrap();
            let value = line[1..].parse::<i64>().unwrap();
            position.advance(action, value);
            waypoint.advance(action, value);
            // debug
            // if action == 'R' || action == 'L' {
            //     println!("{}{}", action, value);
            // }
            // println!(
            //     "{}{} => s: {},{}  w: {},{}", action, value,
            //     waypoint.s_north, waypoint.s_east, waypoint.w_north, waypoint.w_east
            // );
        }
    }
    println!(
        "{} manhattan distance",
        position.north.abs() + position.east.abs()
    );
    println!(
        "{} manhattan distance extra",
        waypoint.s_north.abs() + waypoint.s_east.abs()
    );
}
