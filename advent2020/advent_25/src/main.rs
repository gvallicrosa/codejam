use std::collections::HashMap;
use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut pub_card = 0;
    let mut pub_door = 0;
    for line in contents.split("\n") {
        if line.len() > 0 {
            if pub_card == 0 {
                pub_card = line.parse::<usize>().unwrap();
            } else {
                pub_door = line.parse::<usize>().unwrap();
            }
        }
    }

    // find number of loops
    let card_loops = find_loops(&pub_card);
    let door_loops = find_loops(&pub_door);
    println!("card loops: {}", card_loops);
    println!("door loops: {}", door_loops);
    println!("key: {}", transform(pub_card, door_loops));
    println!("key: {}", transform(pub_door, card_loops));

}

fn find_loops(pub_key: &usize) -> usize {
    let subject_number = 7;
    let mut value = 1;
    let mut loopsize = 0;
    while value != *pub_key {
        loopsize += 1;
        value *= subject_number;
        value %= 20201227;
    }
    return loopsize;
}

fn transform(subject_number: usize, loopsize: usize) -> usize {
    let mut value = 1;
    for _ in 0..loopsize {
        value *= subject_number;
        value %= 20201227;
    }
    return value;
}
