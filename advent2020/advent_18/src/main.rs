use std::env;
use std::fs;

// #[derive(PartialEq, Copy, Clone)]
#[derive(PartialEq)]
pub enum Token {
    Addition,
    Multiply,
    LeftPar,
    RightPar,
    Number(usize),
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut total = 0;
    let mut total_v2 = 0;
    for line in contents.split("\n") {
        if line.len() > 0 {
            // println!("{}", line);
            let (v, v2) = compute(line);
            // println!("{} => {}\n", line, v2);
            total += v;
            total_v2 += v2;
            // break;
        }
    }
    println!("{} total", total);
    println!("{} total_v2", total_v2);
}

fn compute(line: &str) -> (usize, usize) {
    // lexer
    let mut tokens = Vec::new();
    let mut buffer = String::new();
    for c in line.chars() {
        match c {
            '(' => {
                if buffer.len() > 0 {
                    let num = buffer.parse::<usize>().unwrap();
                    tokens.push(Token::Number(num));
                    buffer.clear();
                }
                tokens.push(Token::LeftPar);
            }
            ')' => {
                if buffer.len() > 0 {
                    let num = buffer.parse::<usize>().unwrap();
                    tokens.push(Token::Number(num));
                    buffer.clear();
                }
                tokens.push(Token::RightPar);
            }
            '+' => {
                if buffer.len() > 0 {
                    let num = buffer.parse::<usize>().unwrap();
                    tokens.push(Token::Number(num));
                    buffer.clear();
                }
                tokens.push(Token::Addition);
            }
            '*' => {
                if buffer.len() > 0 {
                    let num = buffer.parse::<usize>().unwrap();
                    tokens.push(Token::Number(num));
                    buffer.clear();
                }
                tokens.push(Token::Multiply);
            }
            '0'..='9' => {
                buffer.push(c);
            }
            _ => {
                if buffer.len() > 0 {
                    let num = buffer.parse::<usize>().unwrap();
                    tokens.push(Token::Number(num));
                    buffer.clear();
                }
            }
        }
    }
    // last value
    if buffer.len() > 0 {
        let num = buffer.parse::<usize>().unwrap();
        tokens.push(Token::Number(num));
        buffer.clear();
    }
    // ast
    return (parse(&tokens), parse_v2(&tokens));
}

fn parse(tokens: &Vec<Token>) -> usize {
    let (_, val) = parse_recursive(0, &tokens);
    return val;
}

fn parse_recursive(pos: usize, tokens: &Vec<Token>) -> (usize, usize) {
    // left operator (num, lpar)
    let (mut i, mut vleft) = match tokens[pos] {
        Token::Number(n) => (pos + 1, n),
        Token::LeftPar => parse_recursive(pos + 1, &tokens),
        _ => panic!(),
    };

    // end condition
    if i >= tokens.len() {
        return (i, vleft);
    }

    // get operator and right
    while true {
        // operator (add, mul, rightpar)
        let oper = match tokens[i] {
            Token::Addition => Token::Addition,
            Token::Multiply => Token::Multiply,
            Token::RightPar => {
                // println!("subexpr => {}", vleft);
                return (i + 1, vleft);
            }
            _ => panic!(),
        };
        i += 1;

        // get right value
        let posval = match tokens[i] {
            Token::Number(n) => (i + 1, n),
            Token::LeftPar => parse_recursive(i + 1, &tokens),
            _ => panic!(),
        };
        i = posval.0;
        let vright = posval.1;

        // operation
        match oper {
            Token::Addition => vleft += vright,
            Token::Multiply => vleft *= vright,
            _ => panic!(),
        }

        // end condition
        if i >= tokens.len() {
            return (i, vleft);
        }
        // println!("{}: vleft {}", i, vleft);
    }
    panic!();
    // return (0, 0);
}

fn parse_v2(tokens: &Vec<Token>) -> usize {
    // debug
    // let mut deep = 0;
    // for token in &tokens {
    //     match token {
    //         Token::LeftPar => deep += 1,
    //         Token::RightPar => deep -= 1,
    //         Token::Number(v) => println!("{:>1$}", v, 2 * deep),
    //         Token::Addition => println!("{:>1$}", "+", 2 * deep),
    //         Token::Multiply => println!("{:>1$}", "*", 2 * deep),
    //     }
    // }

    // debug line
    // for token in tokens {
    //     match token {
    //         Token::LeftPar => print!("("),
    //         Token::RightPar => print!(")"),
    //         Token::Number(v) => print!("{}", v),
    //         Token::Addition => print!("+"),
    //         Token::Multiply => print!("*"),
    //     }
    // }
    // println!("");

    // get result
    // println!("len: {}", tokens.len());
    let (_, val) = parse_recursive_v2(0, 0, &tokens);
    return val;
}

fn debug_tokens(depth: usize, tokens: &Vec<Token>) {
    // debug line
    print!("{:>1$}", "", 2 + 2 * depth);
    for token in tokens {
        match token {
            Token::LeftPar => print!("("),
            Token::RightPar => print!(")"),
            Token::Number(v) => print!("{}", v),
            Token::Addition => print!("+"),
            Token::Multiply => print!("*"),
        }
    }
    println!("");
}

fn parse_recursive_v2(pos: usize, depth: usize, tokens: &Vec<Token>) -> (usize, usize) {
    // get all tokens at this depth
    let mut tks = Vec::new();
    tks.push(Token::LeftPar);
    let mut i = pos;
    while i < tokens.len() {
        match tokens[i] {
            Token::LeftPar => {
                let (j, v) = parse_recursive_v2(i + 1, depth + 1, tokens);
                tks.push(Token::Number(v));
                i = j - 1;
            }
            Token::RightPar => break, // no more at this level
            Token::Number(n) => tks.push(Token::Number(n)),
            Token::Addition => tks.push(Token::Addition),
            Token::Multiply => {
                tks.push(Token::RightPar); // flat depth with parens to give + precedence
                tks.push(Token::Multiply);
                tks.push(Token::LeftPar);
            }
        }

        // next
        i += 1;
    }
    tks.push(Token::RightPar);
    // debug_tokens(depth, &tks);

    // compute as previous one
    let v = parse(&tks);
    // println!("{}: => {}", i+1, v);
    return (i+1, v);
}
