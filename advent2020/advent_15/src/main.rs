use std::collections::HashMap;

fn main() {
    // process
    let mut mem: HashMap<usize, usize> = HashMap::new();
    // let numbers = vec![0, 3, 6];  // input
    let numbers = vec![1, 3, 2]; // => 1
    let numbers = vec![2, 1, 3]; // => 10, 3544142
    let numbers = vec![1, 20, 8, 12, 0, 14]; // input
    for (i, n) in numbers.iter().enumerate().take(numbers.len() - 1) {
        println!("{:>2}: {}", i, n);
        mem.insert(*n, i);
    }
    // new numbers
    let mut prev = numbers[numbers.len() - 1];
    let mut turn = numbers.len() - 1;
    let limit = 2020;
    let limit = 30_000_000;
    let percent = limit / 100;
    while turn < limit {
        // next number
        let new = match mem.contains_key(&prev) {
            true => turn - mem.get(&prev).unwrap(),
            false => 0,
        };
        // insert prev in memory
        if mem.contains_key(&prev) {
            *mem.get_mut(&prev).unwrap() = turn;
        } else {
            mem.insert(prev, turn);
        }
        // next loop
        turn += 1;
        if turn == limit {
            println!("{:>2}: {} end", turn, prev);
        }
        if turn % percent == 0 {
            println!("{}%: {}", turn / percent, prev);
        }
        prev = new.clone();
    }
}
