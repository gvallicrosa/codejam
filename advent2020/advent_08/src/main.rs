use std::env;
use std::fs;

pub enum Command {
    Nop(i64),
    Acc(i64),
    Jmp(i64),
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut commands = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            let mut iter = line.split(" ");
            let command = iter.next().unwrap();
            let value = iter.next().unwrap().parse::<i64>().unwrap();
            if command.matches("nop").count() > 0 {
                commands.push(Command::Nop(value));
            } else if command.matches("jmp").count() > 0 {
                commands.push(Command::Jmp(value));
            } else if command.matches("acc").count() > 0 {
                commands.push(Command::Acc(value));
            }
        }
    }
    // execute
    let mut index = 0;
    let mut accum = 0;
    let mut visited: Vec<_> = commands.iter().map(|_v| false).collect();
    while !visited[index] {
        // visited
        visited[index] = true;
        // action
        match &commands[index] {
            Command::Nop(_v) => index += 1,
            Command::Jmp(v) => index = (index as i64 + v) as usize,
            Command::Acc(v) => {
                index += 1;
                accum += v;
            }
        }
    }
    // repair
    let mut index = 0;
    let mut accum = 0;
    let mut changed: Vec<_> = commands.iter().map(|_v| false).collect();
    let mut visited: Vec<_> = commands.iter().map(|_v| false).collect();
    while index < commands.len() {
        // visited
        visited[index] = true;

        // alternative when chainging this cell
        if !changed[index] {
            // changed
            changed[index] = true;
            // new variables
            let mut accum2 = accum;
            let mut index2 = index;
            let mut visited2 = visited.to_vec();
            let mut done = false;
            // explore alternate
            match &commands[index] {
                Command::Jmp(_v) => index2 += 1,
                Command::Nop(v) => index2 = (index2 as i64 + v) as usize,
                Command::Acc(_v) => done = true, // no alternative for this one
            }
            // normal execution the rest
            if !done {
                while (index2 < commands.len()) && (!visited2[index2]) {
                    visited2[index2] = true;
                    match &commands[index2] {
                        Command::Nop(_v) => index2 += 1,
                        Command::Jmp(v) => index2 = (index2 as i64 + v) as usize,
                        Command::Acc(v) => {
                            index2 += 1;
                            accum2 += v;
                        }
                    }
                }
            }
            // is a solution?
            if index2 >= commands.len() {
                println!("{} accum solved", accum2);
                break;
            }
        }

        // action
        match &commands[index] {
            Command::Nop(_v) => index += 1,
            Command::Jmp(v) => index = (index as i64 + v) as usize,
            Command::Acc(v) => {
                index += 1;
                accum += v;
            }
        }
    }
    // result
    println!("{} accum", accum);
}
