use std::collections::HashMap;
use std::env;
use std::fs;

struct Data {
    num: usize,
    name: String,
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut rules = HashMap::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            // take out last dot
            let line = &line[..line.len() - 1];
            // get color name
            let mut iter = line.split(" bags contain ");
            let name = iter.next().unwrap();
            let included = iter.next().unwrap();
            let mut data = Vec::new();
            if included.matches("no other bags").count() == 0 {
                // println!("{}", name);
                for inc in included.split(", ") {
                    let inc = inc.split(" bag").next().unwrap();
                    let num = inc[0..1].parse::<usize>().unwrap();
                    let ninc = inc[2..].split("bag").next().unwrap();
                    data.push(Data {
                        name: String::from(ninc),
                        num: num,
                    });
                    // println!("  - {}: {}", num, ninc);
                }
            } else {
                // no insertion = empty
                println!("{}: empty", name);
            }
            rules.insert(String::from(name), data);
        }
    }
    // count
    let size = rules.len();
    let mut count = 0;
    let mut last = 0;
    let mut contained = HashMap::new();
    let mut contained_count = HashMap::new();
    let solution = String::from("shiny gold");
    while contained.len() != size {
        println!("{:>3}/{}  => {:>3}", contained.len(), size, count);
        // loop over all rules
        for (key, values) in &rules {
            // check that this rule is not solved
            if !contained.contains_key(key) {
                // no contained bags
                if values.len() == 0 {
                    // not inside
                    contained.insert(key, false);
                    // contains zero bags
                    contained_count.insert(String::from(key), 0);
                    continue;
                }
                // for each contained bag in the rule
                let mut not = 0;
                let mut inside = 0;
                let mut allok = true;
                for v in values {
                    let name = &v.name;
                    let num = &v.num;
                    // check how many bags inside
                    if contained_count.contains_key(name) {
                        // itself amd the contained ones
                        inside += num * (1 + contained_count[name]);
                    } else {
                        allok = false;
                    }

                    // check if "shiny gold" contained directly or in children
                    if contained.contains_key(&name) {
                        if contained[&name] {
                            // one bag contains the bag
                            contained.insert(key, true);
                            count += 1;
                            break;
                        } else {
                            // not contained
                            not += 1;
                        }
                    } else if v.name.matches(&solution).count() > 0 {
                        // this bag contains it
                        contained.insert(key, true);
                        count += 1;
                        break;
                    }
                }
                // no bag contains it and has been explored
                if not == values.len() {
                    contained.insert(key, false);
                }
                // all contained bags counted
                if allok {
                    contained_count.insert(String::from(key), inside);
                }
            }
        }
        // end condition
        if (last == contained.len())  && contained_count.contains_key(&solution) {
            break;
        }
        last = contained.len();
    }

    // initial
    // println!("{}", contained_count.len());
    // for (k, v) in &contained_count {
        // println!("{}: {}", v, k);
    // }
    println!("{} count", count);
    println!("{} count_contained", contained_count[&solution]);
}
