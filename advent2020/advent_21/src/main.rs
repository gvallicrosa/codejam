use std::collections::HashMap;
use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut alergens2plates: HashMap<String, Vec<Vec<String>>> = HashMap::new();
    let mut plates = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            let plate: Vec<_> = line
                .split(" (contains ")
                .nth(0)
                .unwrap()
                .split(" ")
                .map(|s| s.to_string())
                .collect();
            let alers: Vec<_> = line
                .split(" (contains ")
                .nth(1)
                .unwrap()
                .split(")")
                .nth(0)
                .unwrap()
                .split(", ")
                .map(|s| s.to_string())
                .collect();
            // alergens with all plates that mention them
            for ale in alers.iter() {
                alergens2plates
                    .entry(ale.to_owned())
                    .or_insert_with(Vec::new)
                    .push(plate.to_vec());
            }
            // list of plates
            plates.push(plate);
        }
    }
    // check alergens pairings
    let n_alergens = alergens2plates.len();
    let mut bad_ingredients = Vec::new();
    let mut alergens_done = Vec::new();
    let mut alergen_ingredient = Vec::new();
    while bad_ingredients.len() < n_alergens {
        // check everything
        for (k, v) in alergens2plates.iter() {
            // ignore already solved alergens
            if alergens_done.iter().any(|&i| i == k) {
                continue;
            }
            // init search for one alergen
            let n = v.len();
            let mut counter = HashMap::new();
            for dish in v.iter() {
                for ingredient in dish.iter() {
                    // ignore bad ingredients
                    if bad_ingredients.iter().any(|&i| i == ingredient) {
                        continue;
                    }
                    *counter.entry(ingredient).or_insert(0) += 1;
                }
            }
            // check any has maximum
            let mut name = &counter.iter().nth(0).unwrap().0.to_owned(); // init value to first one
            let mut count = 0;
            for (i, c) in counter.iter() {
                if *c == n {
                    name = i;
                    count += 1;
                    // println!("{} has {}", i, k);
                }
            }
            // only one possibility
            if count == 1 {
                println!("{} --> {}", name, k);
                bad_ingredients.push(name.to_owned());
                alergens_done.push(k);
                alergen_ingredient.push((k, name.to_owned()));
            }
        }
        println!("");
    }

    // count ingredients that have no alergens
    let mut total = 0;
    for plate in plates.iter() {
        for ingredient in plate.iter() {
            // ignore bad ingredients
            if bad_ingredients.iter().any(|&i| i == ingredient) {
                continue;
            }
            total += 1;
        }
    }
    println!("{} total", total);

    // sort by alergen
    alergen_ingredient.sort();
    let mut canonical = String::new();
    for (a, i) in alergen_ingredient.iter() {
        println!("{} {}", a, i);
        canonical.push_str(i);
        canonical.push(',');
    }
    println!("canonical: {}", canonical);

}
