use std::iter::FromIterator;

fn main() {
    // get input
    // let input = "389125467"; // simple.txt
    let input = "712643589"; // input.txt

    // process
    let mut cups: Vec<_> = input.chars().map(|c| c.to_digit(10).unwrap()).collect();
    let maxval = cups.iter().max().unwrap().to_owned();
    let size = cups.len();

    part2(cups.to_vec(), maxval);

    // move
    let mut moves = 0;
    while moves < 100 {
        // increase
        moves += 1;

        // pick next 3 cups and remove them
        let current = cups.iter().nth(0).unwrap().to_owned();
        let picked = Vec::from_iter(cups.iter().skip(1).take(3).cloned());
        let unpicked = Vec::from_iter(cups.iter().skip(4).take(size - 4).cloned());

        // print!("({}) ", current);
        // print!("[");
        // for v in picked.iter() {
        //     print!("{} ", v);
        // }
        // print!("] ");
        // for v in unpicked.iter() {
        //     print!("{} ", v);
        // }
        // println!("");
        // println!("");

        // destination cup => cup with label equal to the current cup's label minus one (minus one ... wrap to max)
        let mut next = current;
        let mut done = false;
        while !done {
            // next
            if next == 0 {
                next = maxval;
            } else {
                next -= 1;
            }
            if unpicked.iter().any(|v| *v == next) {
                // place cups immediately clockwise of destination cup
                for (i, v) in unpicked.iter().enumerate() {
                    if *v == next {
                        let mut temp = Vec::new();
                        // from the unpicked until the current
                        for j in 0..i + 1 {
                            temp.push(unpicked[j])
                        }
                        // place the picked ones
                        for v in &picked {
                            temp.push(*v);
                        }
                        // then the next coming
                        for j in i + 1..unpicked.len() {
                            temp.push(*unpicked.iter().nth(j).unwrap())
                        }
                        // then the current one
                        temp.push(current);
                        // copy to main
                        cups = temp.to_vec();
                        // end
                        done = true;
                        break;
                    }
                }
            }
        }
        // selects a new current cup => immediately clockwise of the current cup
        // it is the first one
    }

    // clockwise order from cup 1
    print!("part1: ");
    for (i, v) in cups.iter().enumerate() {
        if *v == 1 {
            // next ones
            for j in i + 1..cups.len() {
                print!("{}", cups[j]);
            }
            // previous ones
            for j in 0..i {
                print!("{}", cups[j]);
            }
            // end
            println!("");
            break;
        }
    }
}

fn part2(mut cups_values: Vec<u32>, maxval: u32) {
    // add numbers to 1M
    let size = cups_values.len() as u32;
    for i in 0..1_000_000 - size {
        cups_values.push(maxval + 1 + i);
    }
    // add next
    let mut cups_position: Vec<usize> = (0..1_000_000).collect();
    let mut cups_next: Vec<usize> = Vec::new();
    for i in 0..cups_values.len() {
        // next pointer
        cups_next.push(i + 1);
        // position pointer
        let value = cups_values[i] as usize;
        cups_position[value - 1] = i;
    }
    cups_next[cups_values.len() - 1] = 0;

    // solve in the same way
    // move
    let mut moves = 0;
    let mut current_index = 0;
    // while moves < 100 {
    while moves < 10_000_000 {
        // increase
        moves += 1;
        // if moves % 1_000 == 0 {
        //     println!("{:>5}/10_000", moves / 1_000);
        // }
        // pick the following three
        // current value
        let current_value = cups_values[current_index];
        // pick next 3 cups and remove them
        let pick1_index = cups_next[current_index];
        let pick2_index = cups_next[pick1_index];
        let pick3_index = cups_next[pick2_index];
        // values
        let pick1_value = cups_values[pick1_index];
        let pick2_value = cups_values[pick2_index];
        let pick3_value = cups_values[pick3_index];
        // put current to point to the one after last picked
        cups_next[current_index] = cups_next[pick3_index];

        // destination cup => cup with label equal to the current cup's label minus one (minus one ... wrap to max)
        let mut destination_value = if current_value == 1 {
            cups_values.len() as u32
        } else {
            current_value - 1
        };
        while (destination_value == pick1_value)
            || (destination_value == pick2_value)
            || (destination_value == pick3_value)
        {
            if destination_value == 1 {
                destination_value = cups_values.len() as u32;
            } else {
                destination_value -= 1;
            }
        }
        // search for destination value
        let destination_index = cups_position[(destination_value as usize) - 1];
        // original next from destination
        let other_index = cups_next[destination_index];
        // place cups immediately clockwise of destination cup
        cups_next[destination_index] = pick1_index;
        cups_next[pick1_index] = pick2_index;
        cups_next[pick2_index] = pick3_index;
        cups_next[pick3_index] = other_index;
        // selects a new current cup => immediately clockwise of the current cup
        current_index = cups_next[current_index];

        // debug
        // let mut index = current_index;
        // let value = cups_values[index];
        // print!("{} ", value);
        // index = cups_next[index];
        // while cups_values[index] != value {
        //     print!("{} ", cups_values[index]);
        //     index = cups_next[index]
        // }
        // println!("");
    }

    // clockwise order from cup 1
    let mut index = 0;
    let mut done = false;
    while !done {
        // find 1
        if cups_values[index] == 1 {
            // get next two numbers

            let next_index = cups_next[index];
            let other_index = cups_next[next_index];
            let next_value = cups_values[next_index];
            let other_value = cups_values[other_index];
            println!(
                "part2: values {} {} => {}",
                next_value,
                other_value,
                (next_value as usize) * (other_value as usize)
            );
            done = true;
        }
        // next
        index = cups_next[index];
    }

    // clockwise order from cup 1
    // let mut index = 0;
    // let mut done = false;
    // while !done {
    //     // find 1
    //     if cups_values[index] == 1 {
    //         // run all until here again
    //         index = cups_next[index]; // move to next
    //         while cups_values[index] != 1 {
    //             print!("{}", cups_values[index]);
    //             index = cups_next[index];
    //         }
    //         println!("\ndone");
    //         done = true;
    //     }
    //     // next
    //     index = cups_next[index];
    // }
}
