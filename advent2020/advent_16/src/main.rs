use std::collections::HashMap;
use std::env;
use std::fs;

pub struct Range {
    min: usize,
    max: usize,
}

impl Range {
    fn is_inside(&self, v: usize) -> bool {
        if self.min <= v && v <= self.max {
            return true;
        }
        return false;
    }
}

pub struct Rule {
    name: String,
    rng1: Range,
    rng2: Range,
}

impl Rule {
    fn is_inside(&self, v: usize) -> bool {
        return self.rng1.is_inside(v) || self.rng2.is_inside(v);
    }
}

enum State {
    Rules,
    YourTicket,
    NearbyTickets,
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut state = State::Rules;
    let mut rules = Vec::new();
    let mut total = 0;
    let mut valid_tickets = Vec::new();
    for line in contents.split("\n") {
        if line.len() == 0 {
            // change in which part we are
            state = match state {
                State::Rules => State::YourTicket,
                State::YourTicket => State::NearbyTickets,
                State::NearbyTickets => State::NearbyTickets,
            }
        } else {
            // process according to part
            match state {
                State::Rules => {
                    let name = line.split(": ").nth(0).unwrap();
                    let ranges = line.split(": ").nth(1).unwrap();
                    let r1 = ranges.split(" or ").nth(0).unwrap();
                    let r2 = ranges.split(" or ").nth(1).unwrap();
                    let rng1 = Range {
                        min: r1.split("-").nth(0).unwrap().parse::<usize>().unwrap(),
                        max: r1.split("-").nth(1).unwrap().parse::<usize>().unwrap(),
                    };
                    let rng2 = Range {
                        min: r2.split("-").nth(0).unwrap().parse::<usize>().unwrap(),
                        max: r2.split("-").nth(1).unwrap().parse::<usize>().unwrap(),
                    };
                    rules.push(Rule {
                        name: name.to_owned(),
                        rng1: rng1,
                        rng2: rng2,
                    });
                }
                _ => {
                    // skip header
                    if line.matches("your ticket:").count() > 0 {
                        continue; // my ticket will be the first
                    }
                    if line.matches("nearby tickets:").count() > 0 {
                        continue;
                    }
                    let mut ticket = Vec::new();
                    let mut all_ok = true;
                    for s in line.split(",") {
                        // get value
                        let v = s.parse::<usize>().unwrap();
                        // check all rules
                        let mut inside_one = false;
                        for rule in &rules {
                            if rule.is_inside(v) {
                                inside_one = true;
                                break;
                            }
                        }
                        // add value if not inside any
                        if !inside_one {
                            total += v;
                            println!("{}", v);
                            all_ok = false;
                        }
                        // add to ticket
                        ticket.push(v);
                    }
                    // is a valid ticket
                    if all_ok {
                        valid_tickets.push(ticket);
                    }
                }
            }
        }
    }
    println!("{} total", total);
    println!("digraph graphname {{");
    println!("rankdir=\"LR\";");

    // check only the valid ones
    let myticket = valid_tickets[0].to_vec();
    let size = myticket.len();
    // for each field in the tickets
    let mut candidates : HashMap<String, Vec<usize>> = HashMap::new();
    for i in 0..size {
        // check a rule
        for rule in &rules {
            // for all tickets values in that field
            let mut all_inside = true;
            for ticket in &valid_tickets {
                let v = ticket[i];
                if !rule.is_inside(v) {
                    all_inside = false;
                    break;
                }
            }
            if all_inside {
                // println!("pos {:>2} can be rule {}", i, rule.name);
                println!("{} -> \"{}\";", i, rule.name);
                if candidates.contains_key(&rule.name) {
                    candidates.get_mut(&rule.name).unwrap().push(i);
                } else {
                    candidates.insert(rule.name.clone(), vec![i]);
                }
            }
        }
    }
    println!("}}");

    // assign
    let mut assigned = Vec::new();
    let mut result = 1;
    while assigned.len() < size {
        for rule in &rules {
            let data = candidates.get(&rule.name).unwrap();
            let mut count = 0;
            let mut value = 0;
            for v in data.iter() {
                // not assigned yet
                if !assigned.iter().any(|&d| d==*v) {
                    count += 1;
                    value = *v;
                }
            }
            // only found one
            if count == 1 {
                println!("{} is \"{}\"", value, rule.name);
                assigned.push(value);
                // count our values
                if rule.name.matches("departure ").count() > 0 {
                    result *= myticket[value];
                }
            }
        }
    }
    println!("{} total", result);
}
