use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let earliest = contents.split("\n").nth(0).unwrap().parse::<i64>().unwrap();
    let buses = contents.split("\n").nth(1).unwrap();
    let mut best_wait = earliest + 1_000_000;
    let mut best_line = -1;
    let mut vec_buses = Vec::new();
    for bus in buses.split(",") {
        if bus.chars().all(char::is_numeric) {
            let turn = bus.parse::<i64>().unwrap();
            if earliest % turn == 0 {
                println!("best is exactly {} wait {} => {}", turn, 0, 0);
                break;
            }
            let wait = (earliest / turn + 1) * turn - earliest;
            println!("bus {:>3} wait {}", turn, wait);
            if wait < best_wait {
                best_wait = wait;
                best_line = turn;
            }
            vec_buses.push(Some(turn));
        } else {
            vec_buses.push(None);
        }
    }
    println!(
        ">> best is bus {} wait {} => {}",
        best_line,
        best_wait,
        best_line * best_wait
    );

    // old_way(vec_buses);

    // time where bus 0 departs at t
    // bus 1 departs at t+1
    // bus 2 departs at t+2
    // ...
    let bus0 = vec_buses[0].unwrap();
    let mut factor = 1; // to multiply against bus0 to get time0
    let mut factor_increment = 1; // how much to increment the factor at each test
    println!("\n{:>3} bus => wait 0", bus0);
    for (i, bus) in vec_buses.iter().enumerate().skip(1) {
        match bus {
            Some(b) => {
                while true {
                    // time of first bus
                    let time0 = bus0 * factor;
                    // time of first bus + position of this divisible by this id
                    if (time0 + (i as i64)) % b == 0 {
                        // increment factor
                        factor_increment *= b;
                        // partial result
                        println!("{:>3} bus => wait {}", b, time0);
                        break;
                    }
                    // next loop factor
                    factor += factor_increment;
                }
            }
            None => (), // 'x' busses can depart at any time
        }
    }
}

fn old_way(buses: Vec<Option<i64>>) {
    // time where bus 0 departs at t
    // bus 1 departs at t+1
    // bus 2 departs at t+2
    // ...
    let mut time0 = 0;
    let mut factor = 0;
    let mut correct = false;
    let mut count = 0;
    while !correct {
        // init
        factor += 1;
        correct = true;
        if factor % 1_000_000_000 == 0 {
            println!("factor {}", factor);
        }
        // check
        time0 = factor * buses[0].unwrap();
        for (i, bus) in buses.iter().enumerate().skip(1) {
            match bus {
                Some(b) => {
                    if (time0 + (i as i64)) % b != 0 {
                        correct = false;
                        break;
                    }
                }
                None => (), // 'x' busses can depart at any time
            }
        }
        if correct && count < 10 {
            println!("wait {}", time0);
            correct = false;
            count += 1;
        }
    }
    println!("wait {}", time0);
}
