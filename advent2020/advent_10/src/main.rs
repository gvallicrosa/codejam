use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut numbers = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            let value = line.parse::<usize>().unwrap();
            numbers.push(value);
        }
    }

    // sort and then count jolt1 and jolt3
    numbers.sort();
    let mut curr_jolt = 0;
    let mut jolt1 = 0;
    let mut jolt2 = 0;
    let mut jolt3 = 0;
    let mut contiguous_j1 = Vec::new();
    let mut count_j1 = 0;
    for v in numbers {
        if v - curr_jolt == 1 {
            jolt1 += 1;
            count_j1 += 1;
        } else if v - curr_jolt == 2 {
            jolt2 += 1;
        } else if v - curr_jolt == 3 {
            jolt3 += 1;
            if count_j1 > 0 {
                contiguous_j1.push(count_j1 as u128);
                count_j1 = 0;
            }
        }
        curr_jolt = v;
    }
    jolt3 += 1; // last adapter

    // last count
    if count_j1 > 0 {
        contiguous_j1.push(count_j1 as u128);
    }

    println!("j1: {}  j2: {}  j3: {}", jolt1, jolt2, jolt3);
    println!("{} j1 * j3", jolt1 * jolt3);

    // ways to arrange them
    // j3 cannot be skipped
    // j1 can be skipped one at a time or combined (but cannot skip the first or last one in each group)
    let mut ways: u128 = 1;
    for (_i, v) in contiguous_j1.iter().enumerate() {
        println!("{} contiguous one diffs", v);
        match v {
            1 => ways *= 1, // XX
            2 => ways *= 2, // XXX, X_X
            3 => ways *= 4, // XXXX, X_XX, XX_X, X__X
            4 => ways *= 7, // XXXXX, X_XXX, XX_XX, XXX_X, X__XX, XX__X, X_X_X
            _ => (),
            // apparently is the tribonnacci sequence
        }
        // println!("{:>2}/{}: {} ways", i+1, contiguous_j1.len(), ways);
    }
    println!("{} ways", ways);
}
