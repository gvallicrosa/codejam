use std::collections::VecDeque;
use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut buffer = VecDeque::new();
    let mut numbers = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            let value = line.parse::<usize>().unwrap();
            // check is sum of 25 previous numbers
            if buffer.len() == 25 {
                // search in the previous entries for the sum
                let mut found = false;
                for i in 0..25 {
                    for j in i + 1..25 {
                        if buffer[i] + buffer[j] == value {
                            found = true;
                            break;
                        }
                    }
                    if found {
                        break;
                    }
                }
                // never found is the solution
                if !found {
                    println!("{} no sum buffer 25", value);
                    // find contiguous number that sum this number
                    for i in 0..numbers.len() {
                        let mut sum = 0;
                        let mut j = i;
                        while sum < value {
                            sum += numbers[j];
                            j += 1;
                        }
                        // exact sum
                        if sum == value {
                            // find min and max
                            let mut min = 1_000_000_000;
                            let mut max = 0;
                            for v in i..j {
                                println!("{}", numbers[v]);
                                if numbers[v] < min {
                                    min = numbers[v];
                                }
                                if numbers[v] > max {
                                    max = numbers[v];
                                }
                            }
                            println!("{} min + max", min + max);
                        }
                    }
                    break; // end
                }
                // delete oldest
                buffer.pop_front();
            }
            // add current value to buffer
            buffer.push_back(value);
            numbers.push(value);
        }
    }
}
