use std::env;
use std::fs;

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let vec: Vec<_> = contents.split("\n").map(|v| v.parse::<u32>()).collect();
    println!("{} numbers", vec.len());
    for i in 0..(vec.len() - 1) {
        // println!("{} {}", i, vec[i].as_ref().unwrap());

        for j in (i + 1)..(vec.len() - 1) {
            let a = vec[i].as_ref().unwrap();
            let b = vec[j].as_ref().unwrap();
            if a + b == 2020 {
                println!("2 nums answer {} * {} = {}", a, b, a * b);
            }
            for k in (j + 1)..(vec.len() - 1) {
                let c = vec[k].as_ref().unwrap();
                if a + b + c == 2020 {
                    println!("3 nums answer {} * {} * {} = {}", a, b, c, a * b * c);
                }
            }
        }
    }
}
