use std::env;
use std::fs;

#[derive(PartialEq, Copy, Clone)]
pub enum State {
    Active,
    Inactive,
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut space2d = Vec::new();
    for line in contents.split("\n") {
        if line.len() > 0 {
            println!("{}", line);
            let mut row = Vec::new();
            for c in line.chars() {
                match c {
                    '#' => row.push(State::Active),
                    _ => row.push(State::Inactive),
                }
            }
            space2d.push(row);
        }
    }
    println!("");

    // 3d space computation
    let mut space = Vec::new(); // zyx
    space.push(space2d.to_vec());
    for _i in 0..6 {
        // limits
        let zsize = space.len() as i64;
        let ysize = space[0].len() as i64;
        let xsize = space[0][0].len() as i64;
        print!("shape {}x{}x{} => ", xsize, ysize, zsize);
        // explore with border +1 to the actual ones
        let mut active = 0;
        let mut space3d = Vec::new(); // zyx
        for z in 0..zsize + 2 {
            let mut space2d = Vec::new();
            // allow -1
            for y in 0..ysize + 2 {
                // border, 1-size, border = size + 2
                let mut space1d = Vec::new();
                for x in 0..xsize + 2 {
                    // ===============
                    // check neighbors
                    // ===============
                    // println!("{},{},{}", x, y, z);
                    let mut active_neigh = 0;
                    for i in -1..2 {
                        let xi = x + i;
                        // ignore border
                        if xi <= 0 || xi > xsize {
                            continue; // new empty space
                        }
                        for j in -1..2 {
                            let yi = y + j;
                            // ignore border
                            if yi <= 0 || yi > ysize {
                                continue; // new empty space
                            }
                            for k in -1..2 {
                                let zi = z + k;
                                if zi <= 0 || zi > zsize {
                                    continue; // new empty space
                                }
                                // ignore itself
                                if i == 0 && j == 0 && k == 0 {
                                    continue;
                                }
                                // check status
                                // print!("  {},{},{}", xi,yi,zi);
                                let a = (xi - 1) as usize;
                                let b = (yi - 1) as usize;
                                let c = (zi - 1) as usize;
                                // println!(" => {},{},{}", a, b, c);
                                if space[c][b][a] == State::Active {
                                    active_neigh += 1;
                                }
                            }
                        }
                    }
                    // print!("{:>2} {:>2} {:>2} => {:>2} active ", x - 1, y - 1, z - 1, active_neigh);
                    // resolve what to do
                    let mut val = State::Inactive;
                    if (x > 0) && (x <= xsize) && (y > 0) && (y <= ysize) && (z > 0) && (z <= zsize)
                    {
                        val = space[(z - 1) as usize][(y - 1) as usize][(x - 1) as usize];
                    }
                    match val {
                        State::Active => {
                            if active_neigh == 2 || active_neigh == 3 {
                                space1d.push(State::Active);
                                active += 1;
                            // println!("#")
                            } else {
                                space1d.push(State::Inactive);
                                // println!(".")
                            }
                        }
                        State::Inactive => {
                            if active_neigh == 3 {
                                space1d.push(State::Active);
                                active += 1;
                            // println!("#")
                            } else {
                                space1d.push(State::Inactive);
                                // println!(".")
                            }
                        }
                    }
                }
                space2d.push(space1d);
                // println!("");
            }
            space3d.push(space2d);
            // println!("");
            // println!("");
        }
        println!("{} total", active);
        space = space3d; // copy as source
    }
    println!("");

    // 4d space computation
    let mut space3d = Vec::new(); // wzyx
    space3d.push(space2d);
    let mut space = Vec::new(); // wzyx
    space.push(space3d);
    for _i in 0..6 {
        // limits
        let wsize = space.len() as i64;
        let zsize = space[0].len() as i64;
        let ysize = space[0][0].len() as i64;
        let xsize = space[0][0][0].len() as i64;
        print!("shape {}x{}x{}x{} => ", xsize, ysize, zsize, wsize);
        // explore with border +1 to the actual ones
        let mut active = 0;
        let mut space4d = Vec::new(); // wzyx
        for w in 0..wsize + 2 {
            let mut space3d = Vec::new();
            for z in 0..zsize + 2 {
                let mut space2d = Vec::new();
                // allow -1
                for y in 0..ysize + 2 {
                    // border, 1-size, border = size + 2
                    let mut space1d = Vec::new();
                    for x in 0..xsize + 2 {
                        // ===============
                        // check neighbors
                        // ===============
                        // println!("{},{},{}", x, y, z);
                        let mut active_neigh = 0;
                        for i in -1..2 {
                            let xi = x + i;
                            // ignore border
                            if xi <= 0 || xi > xsize {
                                continue; // new empty space
                            }
                            for j in -1..2 {
                                let yi = y + j;
                                // ignore border
                                if yi <= 0 || yi > ysize {
                                    continue; // new empty space
                                }
                                for k in -1..2 {
                                    let zi = z + k;
                                    if zi <= 0 || zi > zsize {
                                        continue; // new empty space
                                    }
                                    for l in -1..2 {
                                        let wi = w + l;
                                        if wi <= 0 || wi > wsize {
                                            continue; // new empty space
                                        }
                                        // ignore itself
                                        if i == 0 && j == 0 && k == 0 && l == 0 {
                                            continue;
                                        }
                                        // check status
                                        // print!("  {},{},{}", xi,yi,zi);
                                        let a = (xi - 1) as usize;
                                        let b = (yi - 1) as usize;
                                        let c = (zi - 1) as usize;
                                        let d = (wi - 1) as usize;
                                        // println!(" => {},{},{}", a, b, c);
                                        if space[d][c][b][a] == State::Active {
                                            active_neigh += 1;
                                        }
                                    }
                                }
                            }
                        }
                        // print!("{:>2} {:>2} {:>2} => {:>2} active ", x - 1, y - 1, z - 1, active_neigh);
                        // resolve what to do
                        let mut val = State::Inactive;
                        if (x > 0)
                            && (x <= xsize)
                            && (y > 0)
                            && (y <= ysize)
                            && (z > 0)
                            && (z <= zsize)
                            && (w > 0)
                            && (w <= wsize)
                        {
                            val = space[(w - 1) as usize][(z - 1) as usize][(y - 1) as usize]
                                [(x - 1) as usize];
                        }
                        match val {
                            State::Active => {
                                if active_neigh == 2 || active_neigh == 3 {
                                    space1d.push(State::Active);
                                    active += 1;
                                // println!("#")
                                } else {
                                    space1d.push(State::Inactive);
                                    // println!(".")
                                }
                            }
                            State::Inactive => {
                                if active_neigh == 3 {
                                    space1d.push(State::Active);
                                    active += 1;
                                // println!("#")
                                } else {
                                    space1d.push(State::Inactive);
                                    // println!(".")
                                }
                            }
                        }
                    }
                    space2d.push(space1d);
                    // println!("");
                }
                space3d.push(space2d);
                // println!("");
                // println!("");
            }
            space4d.push(space3d);
        }
        println!("{} total", active);
        space = space4d; // copy as source
    }
}
