use std::collections::HashMap;
use std::env;
use std::fs;

// #[derive(PartialEq, Copy, Clone)]
pub enum ReadState {
    Rules,
    Messages,
}

pub enum Rule {
    Simple(char),
    List(Vec<usize>),
    Or(Vec<usize>, Vec<usize>),
}

fn main() {
    // get input
    let mut args = env::args();
    let _appname = args.next();
    let filename = args.next().expect("must give a filename");

    // process
    let contents = fs::read_to_string(filename).expect("something went wrong reading the file");
    let mut read_state = ReadState::Rules;
    let mut rules: HashMap<usize, Rule> = HashMap::new();
    let mut total = 0;
    for line in contents.split("\n") {
        if line.len() == 0 {
            read_state = ReadState::Messages;
            println!("read messages");
        } else {
            match read_state {
                ReadState::Rules => {
                    let n = line.split(": ").nth(0).unwrap().parse::<usize>().unwrap();
                    let rule = line.split(": ").nth(1).unwrap();
                    if rule.matches("\"").count() > 0 {
                        let c = rule.split("\"").nth(1).unwrap().chars().nth(0).unwrap();
                        rules.insert(n, Rule::Simple(c));
                    } else if rule.matches("|").count() > 0 {
                        let left = rule.split(" | ").nth(0).unwrap();
                        let right = rule.split(" | ").nth(1).unwrap();
                        let ls: Vec<_> = left
                            .split(" ")
                            .map(|v| v.parse::<usize>().unwrap())
                            .collect();
                        let rs: Vec<_> = right
                            .split(" ")
                            .map(|v| v.parse::<usize>().unwrap())
                            .collect();
                        rules.insert(n, Rule::Or(ls, rs));
                    } else {
                        let ns: Vec<_> = rule
                            .split(" ")
                            .map(|v| v.parse::<usize>().unwrap())
                            .collect();
                        rules.insert(n, Rule::List(ns));
                    }
                }
                ReadState::Messages => {
                    if check_message(line, &rules) {
                        println!("valid");
                        total += 1;
                    }
                }
            }
        }
    }
    println!("{} total", total);
}

fn check_message(line: &str, rules: &HashMap<usize, Rule>) -> bool {
    println!("\n{}", line);
    return check_rule(line, &rules, 0, 0);
}

fn expand_rule(rules: &HashMap<usize, Rule>, rule: usize) -> Vec<&str> {
    let mut valid = Vec::new();
    return valid;
}
fn expand_recursive(rules: &HashMap<usize, Rule>, rule: usize, pos: usize) -> Vec<String> {
    let rule = rules.get(&rule).unwrap();
    match rule {
        Rule::Simple(c) => {
            return vec![String::from(*c)];
        }
        Rule::List(rs) => {
            let mut val = Vec::new();
            for r in rs.iter() {
                val.append
            }
            return val;
        }
        Rule::Or(rs1, rs2) => {
            println!(" or {:?} {:?}", rs1, rs2);
            let mut ok1 = true;
            let mut ok2 = true;
            for (i, r) in rs1.iter().enumerate() {
                if !check_rule(line, &rules, *r, pos + i) {
                    ok1 = false;
                    break;
                }
            }
            for (i, r) in rs2.iter().enumerate() {
                if !check_rule(line, &rules, *r, pos + i) {
                    ok2 = false;
                    break;
                }
            }
            return ok1 || ok2;
        }
    }
}

fn check_rule(line: &str, rules: &HashMap<usize, Rule>, rule: usize, pos: usize) -> bool {
    // println!("check rule {}", rule);
    print!("{:>1$}", rule, 2 * pos);
    let rule = rules.get(&rule).unwrap();
    match rule {
        Rule::Simple(c) => {
            println!(" simple {}", c);
            if line.chars().nth(pos).unwrap() == *c {
                println!("{:>1$}  valid", "", 2 * pos);
                return true;
            }
            return false;
        }
        Rule::List(rs) => {
            println!(" list {:?}", rs);
            for (i, r) in rs.iter().enumerate() {
                if !check_rule(line, &rules, *r, pos + i) {
                    return false;
                }
            }
            return true;
        }
        Rule::Or(rs1, rs2) => {
            println!(" or {:?} {:?}", rs1, rs2);
            let mut ok1 = true;
            let mut ok2 = true;
            for (i, r) in rs1.iter().enumerate() {
                if !check_rule(line, &rules, *r, pos + i) {
                    ok1 = false;
                    break;
                }
            }
            for (i, r) in rs2.iter().enumerate() {
                if !check_rule(line, &rules, *r, pos + i) {
                    ok2 = false;
                    break;
                }
            }
            return ok1 || ok2;
        }
    }
}
