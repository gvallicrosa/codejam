package main

import (
	"fmt"
)

func Flip(S string, i int, K int) string {
	prev := S[:i]
	todo := S[i : i+K]
	post := S[i+K:]
	var new string
	for i := 0; i < K; i++ {
		if todo[i] == '+' {
			new += "-"
		} else {
			new += "+"
		}
	}
	return prev + new + post
}

func Solve(S string, K int) int {
	var flips int = 0
	L := len(S)
	for i := 0; i < L+1-K; i++ {
		if S[i] == '-' {
			S = Flip(S, i, K)
			flips++
		}
	}
	// recheck
	for i := 0; i < L; i++ {
		if S[i] == '-' {
			return -1
		}
	}
	return flips
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var S string // pancakes
		var K int    // width of flipper
		fmt.Scanln(&S, &K)
		// Solve
		r := Solve(S, K)
		// Show result
		if r >= 0 {
			fmt.Printf("Case #%d: %d\n", t+1, r)
		} else {
			fmt.Printf("Case #%d: IMPOSSIBLE\n", t+1)
		}
	}
}
