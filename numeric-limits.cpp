#include <cmath>
#include <cstdint>
#include <iostream>
#include <limits>

int main() {
  std::cout << "                   int: " << std::numeric_limits<int>::max()
            << " [" << ceil(log10(std::numeric_limits<int>::max())) << "]"
            << std::endl;
  std::cout << "              long int: "
            << std::numeric_limits<long int>::max() << " ["
            << ceil(log10(std::numeric_limits<long int>::max())) << "]"
            << std::endl;
  std::cout << "         long long int: "
            << std::numeric_limits<long long int>::max() << " ["
            << ceil(log10(std::numeric_limits<long long int>::max())) << "]"
            << std::endl;
  std::cout << std::endl;
  std::cout << "          unsigned int: "
            << std::numeric_limits<unsigned int>::max() << " ["
            << ceil(log10(std::numeric_limits<unsigned int>::max())) << "]"
            << std::endl;
  std::cout << "     unsigned long int: "
            << std::numeric_limits<unsigned long int>::max() << " ["
            << ceil(log10(std::numeric_limits<unsigned long int>::max())) << "]"
            << std::endl;
  std::cout << "unsigned long long int: "
            << std::numeric_limits<unsigned long long int>::max() << " ["
            << ceil(log10(std::numeric_limits<unsigned long long int>::max()))
            << "]" << std::endl;
  std::cout << "                size_t: "
            << std::numeric_limits<size_t>::max() << " ["
            << ceil(log10(std::numeric_limits<size_t>::max()))
            << "]" << std::endl;
  std::cout << std::endl;
  std::cout << "uint8_t: " << UINT8_MAX << " [" << ceil(log10(UINT8_MAX)) << "]"
            << std::endl;
  std::cout << "uint16_t: " << UINT16_MAX << " [" << ceil(log10(UINT16_MAX))
            << "]" << std::endl;
  std::cout << "uint32_t: " << UINT32_MAX << " [" << ceil(log10(UINT32_MAX))
            << "]" << std::endl;
  std::cout << "uint64_t: " << UINT64_MAX << " [" << ceil(log10(UINT64_MAX))
            << "]" << std::endl;
}
