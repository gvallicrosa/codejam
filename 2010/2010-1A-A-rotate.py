#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Solve Round 1A 2010.

https://code.google.com/codejam/contest/544101/dashboard#s=p0

Problem

In the exciting game of Join-K, red and blue pieces are dropped into an N-by-N
table. The table stands up vertically so that pieces drop down to the
bottom-most empty slots in their column. For example, consider the following
two configurations:

Legal position - Illegal position
    .......         .......
    .......         .......
    .......         .......
    ....R..         .......
    ...RB..  Bad -> ..BR...
    ..BRB..         ...R...
    .RBBR..         .RBBR..

In these pictures, each '.' represents an empty slot, each 'R' represents a
slot filled with a red piece, and each 'B' represents a slot filled with a blue
piece. The left configuration is legal, but the right one is not. This is
because one of the pieces in the third column (marked with the arrow) has not
fallen down to the empty slot below it.

A player wins if they can place at least K pieces of their color in a row,
either horizontally, vertically, or diagonally. The four possible orientations
are shown below:

Four in a row
    R   RRRR    R   R
    R          R     R
    R         R       R
    R        R         R

In the "Legal Position" diagram at the beginning of the problem statement,
both players had lined up two pieces in a row, but not three.

As it turns out, you are right now playing a very exciting game of Join-K, and
you have a tricky plan to ensure victory! When your opponent is not looking,
you are going to rotate the board 90 degrees clockwise onto its side. Gravity
will then cause the pieces to fall down into a new position as shown below:

 Start       Rotate     Gravity
.......     .......     .......
.......     R......     .......
.......     BB.....     .......
...R...     BRRR...     R......
...RB..     RBB....     BB.....
..BRB..     .......     BRR....
.RBBR..     .......     RBBR...

Unfortunately, you only have time to rotate once before your opponent will
notice.

All that remains is picking the right time to make your move. Given a board
position, you should determine which player (or players!) will have K pieces in
a row after you rotate the board clockwise and gravity takes effect in the new
direction.

Notes

You can rotate the board only once.
* Assume that gravity only takes effect after the board has been rotated
  completely.
* Only check for winners after gravity has finished taking effect.

Input

The first line of the input gives the number of test cases, T. T test cases
follow, each beginning with a line containing the integers N and K. The next N
lines will each be exactly N characters long, showing the initial position of
the board, using the same format as the diagrams above.

The initial position in each test case will be a legal position that can occur
during a game of Join-K. In particular, neither player will have already formed
K pieces in a row.

Output

For each test case, output one line containing "Case #x: y", where x is the
case number (starting from 1), and y is one of "Red", "Blue", "Neither", or
"Both". Here, y indicates which player or players will have K pieces in a row
after you rotate the board.

Input:
    T - number of test cases
    N K - number of lines, number equal to win
Output:
    Case #x: y
    x - case number
    y - Red, Blue, Neither, Both

Sample:
=======
Input:
4
7 3
.......
.......
.......
...R...
...BB..
..BRB..
.RRBR..
6 4
......
......
.R...R
.R..BB
.R.RBR
RB.BBB
4 4
R...
BR..
BR..
BR..
3 3
B..
RB.
RB.

Output:
Case #1: Neither
Case #2: Both
Case #3: Red
Case #4: Blue
"""

import numpy as np
import sys

equivalent = {'.': 0, 'R': 1, 'B': 2}

fh = open(sys.argv[1], 'r')
line = fh.readline()
T = int(line)
for t in range(T):
    line = fh.readline().split()
    N, K = int(line[0]), int(line[1])
    rows = list()
    for n in range(N):
        line = fh.readline().split()[0]
        vals = [equivalent[v] for v in line]
        rows.append(vals)

    original = np.array(rows, dtype=np.uint8)
    # print '============'
    # print K, 'equal'
    # print 'original'
    # print original
    rotated = np.rot90(original, k=3)  # 3 times counter clockwise
    # print 'rotated'
    # print rotated
    for c in range(rotated.shape[1]):  # columns
        r = rotated.shape[0] - 1
        while r > 0:  # rows
            # print r, c
            if rotated[r, c] == 0:
                allzero = True
                # move all that are up
                for i in range(r, 0, -1):
                    val = rotated[i - 1, c]
                    rotated[i, c] = val
                    if not val == 0:
                        allzero = False
                rotated[0, c] = 0
                # print rotated, allzero
                if allzero:
                    r -= 1
            else:
                r -= 1
    # print 'gravity'
    # print rotated
    win = [False, False]  # red, blue
    # Horizontal
    for r in range(rotated.shape[0]):
        for j in (1, 2):
            row = rotated[r, :]
            num = np.sum(row == j)
            if num >= K:
                # Check that are contiguous
                cont = 0
                for c in range(rotated.shape[1]):
                    val = rotated[r, c]
                    if val == j:
                        cont += 1
                    else:
                        cont = 0
                    if cont == K:
                        win[j - 1] = True
    # print 'horizontal', win
    # Vertical
    for c in range(rotated.shape[1]):
        for j in (1, 2):
            col = rotated[:, c]
            num = np.sum(col == j)
            if num >= K:
                # Check that are contiguous
                cont = 0
                for r in range(rotated.shape[0]):
                    val = rotated[r, c]
                    if val == j:
                        cont += 1
                    else:
                        cont = 0
                    if cont == K:
                        win[j - 1] = True
    # print 'vertical', win
    # Diagonal to top right
    vertical = True
    done = False
    r = 0
    c = 0
    while not done:
        # print rotated.shape, r, c

        line = list()
        i = r
        j = c
        while i >= 0 and j < rotated.shape[1]:
            line.append(rotated[i, j])
            i -= 1
            j += 1
        # print line
        line = np.array(line)
        for j in (1, 2):
            num = np.sum(line == j)
            if num >= K:
                # Check that are contiguous
                cont = 0
                for v in range(line.shape[0]):
                    val = line[v]
                    if val == j:
                        cont += 1
                    else:
                        cont = 0
                    if cont == K:
                        win[j - 1] = True

        # Next loop
        if vertical:
            if r < rotated.shape[0] - 1:
                r += 1
            else:
                vertical = False
        if not vertical:
            if c < rotated.shape[1] - 1:
                c += 1
            else:
                done = True
    # print 'diagonal top right', win

    # Diagonal to top left
    vertical = True
    done = False
    r = rotated.shape[0] - 1
    c = 0
    while not done:
        # print rotated.shape, r, c

        line = list()
        i = r
        j = c
        while i < rotated.shape[0] and j < rotated.shape[1]:
            line.append(rotated[i, j])
            i += 1
            j += 1
        # print line
        line = np.array(line)
        for j in (1, 2):
            num = np.sum(line == j)
            if num >= K:
                # Check that are contiguous
                cont = 0
                for v in range(line.shape[0]):
                    val = line[v]
                    if val == j:
                        cont += 1
                    else:
                        cont = 0
                    if cont == K:
                        win[j - 1] = True

        # Next loop
        if vertical:
            if r > 0:
                r -= 1
            else:
                vertical = False
        if not vertical:
            if c < rotated.shape[1] - 1:
                c += 1
            else:
                done = True
    # print 'diagonal top left', win

    res = 'Neither'
    if win[0] and win[1]:
        res = 'Both'
    elif win[0]:
        res = 'Red'
    elif win[1]:
        res = 'Blue'
    print('Case #{:d}: {:s}'.format(t + 1, res))
