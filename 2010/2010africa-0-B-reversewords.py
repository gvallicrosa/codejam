#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Round Qualification B 2010 Africa.

https://code.google.com/codejam/contest/351101/dashboard#s=p1

Problem

Given a list of space separated words, reverse the order of the words. Each
line of text contains L letters and W words. A line will only consist of
letters and space characters. There will be exactly one space character between
each pair of consecutive words.

Input

The first line of input gives the number of cases, N.
N test cases follow. For each test case there will a line of letters and space
characters indicating a list of space separated words. Spaces will not appear
at the start or end of a line.

Output

For each test case, output one line containing "Case #x: " followed by the list
 of words in reverse order.

Limits

Small dataset

N = 5
1 ≤ L ≤ 25

Large dataset

N = 100
1 ≤ L ≤ 1000

Sample

Input

3
this is a test
foobar
all your base

Output

Case #1: test a is this
Case #2: foobar
Case #3: base your all
"""

import sys

fh = open(sys.argv[1], 'r')
N = int(fh.readline())  # number of test cases
for n in range(N):
    words = fh.readline().split()
    rever = words[::-1]
    res = ' '.join(rever)
    print('Case #{:d}: {:s}'.format(n + 1, res))
