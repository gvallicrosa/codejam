#! /#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""
Solve Round 1A B 2010.

https://code.google.com/codejam/contest/544101/dashboard#s=p1


"""


T = int(input())  # number of test cases
for t in range(T):
    D, I, M, N = [int(x) for x in input().split()]  # delete, insert, maxdist
    A = [int(x) for x in input().split()]  # group #people

    res = 0

    print("Case #{:d}: {}".format(t + 1, res))
