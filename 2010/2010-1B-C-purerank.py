#! /#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""
Solve Round 1B C 2010.

https://code.google.com/codejam/contest/635101/dashboard#s=p2

Problem

"""


def fibbo(n):
    if n == 0:
        return 1
    elif n == 1:
        return 1
    else:
        return fibbo(n - 1) + fibbo(n - 2)

T = int(input())  # number of test cases
for t in range(T):
    n = int(input())  # maximum number in the (2, 3, ..., n) set
    # The sequence reassembles the fibbonacci
    # n = 2 -> 1
    # n = 3 -> 2
    # n = 4 -> 3
    # n = 5 -> 5
    # n = 6 -> 8
    ans = fibbo(n - 1) % 100003
    print('Case #{:d}: {:d}'.format(t + 1, ans))
