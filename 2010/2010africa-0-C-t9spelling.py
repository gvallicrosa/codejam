#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Round Qualification C 2010 Africa.

https://code.google.com/codejam/contest/351101/dashboard#s=p2

Problem

The Latin alphabet contains 26 characters and telephones only have ten digits
on the keypad. We would like to make it easier to write a message to your
friend using a sequence of keypresses to indicate the desired characters. The
letters are mapped onto the digits as shown below. To insert the character B
for instance, the program would press 22. In order to insert two characters in
sequence from the same key, the user must pause before pressing the key a
second time. The space character ' ' should be printed to indicate a pause. For
example, 2 2 indicates AA whereas 22 indicates B.

Input

The first line of input gives the number of cases, N. N test cases follow. Each
case is a line of text formatted as

desired_message
Each message will consist of only lowercase characters a-z and space characters
' '. Pressing zero emits a space.

Output

For each test case, output one line containing "Case #x: " followed by the
message translated into the sequence of keypresses.

Limits

1 ≤ N ≤ 100.

Small dataset

1 ≤ length of message in characters ≤ 15.

Large dataset

1 ≤ length of message in characters ≤ 1000.

Sample

Input

4
hi
yes
foo  bar
hello world

Output

Case #1: 44 444
Case #2: 999337777
Case #3: 333666 6660 022 2777
Case #4: 4433555 555666096667775553
"""

import sys

dictionary = {'a': '2', 'b': '22', 'c': '222',
              'd': '3', 'e': '33', 'f': '333',
              'g': '4', 'h': '44', 'i': '444',
              'j': '5', 'k': '55', 'l': '555',
              'm': '6', 'n': '66', 'o': '666',
              'p': '7', 'q': '77', 'r': '777', 's': '7777',
              't': '8', 'u': '88', 'v': '888',
              'w': '9', 'x': '99', 'y': '999', 'z': '9999',
              ' ': '0'}

fh = open(sys.argv[1], 'r')
N = int(fh.readline())  # number of test cases
for n in range(N):
    sentence = fh.readline()[:-1]
    old = 'A'
    res = ''
    for c in sentence:
        val = dictionary[c]
        # print c, old
        if val[0] == old:
            res += ' '
        res += val
        old = val[0]
    res = ''.join(res)
    print('Case #{:d}: {:s}'.format(n + 1, res))
