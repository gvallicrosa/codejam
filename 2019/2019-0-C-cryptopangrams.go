package main

import (
	"bytes"
	"fmt"
	"math/big"
	"sort"
)

func intScanln(n int) ([]int, error) {
	x := make([]int, n)
	y := make([]interface{}, len(x))
	for i := range x {
		y[i] = &x[i]
	}
	n, err := fmt.Scanln(y...)
	x = x[:n]
	return x, err
}

// PrimeFactors gets all prime factors of a given number n
func PrimeFactors(n int) (pfs []int) {
	// Get the number of 2s that divide n
	for n%2 == 0 {
		pfs = append(pfs, 2)
		n = n / 2
	}

	// n must be odd at this point. so we can skip one element
	// (note i = i + 2)
	for i := 3; i*i <= n; i = i + 2 {
		// while i divides n, append i and divide n
		for n%i == 0 {
			pfs = append(pfs, i)
			n = n / i
			if len(pfs) == 2 {
				return
			}
		}
	}

	// This condition is to handle the case when n is a prime number
	// greater than 2
	if n > 2 {
		pfs = append(pfs, n)
	}

	return
}

// cannot handle 10^100
func solveTest1Only(N int, L int, Ls []int) string {
	primes := make([]int, L+1)  // one more prime than products
	temp := PrimeFactors(Ls[0]) // primes of first number
	// check if repeated numbers
	var firstDifferent int
	for firstDifferent = 1; firstDifferent < L; firstDifferent++ {
		if Ls[0] != Ls[firstDifferent] {
			break
		}
	}
	// know which prime is the first or second number
	if Ls[firstDifferent]%temp[0] == 0 {
		if firstDifferent%2 == 1 {
			primes[0] = temp[1]
			primes[1] = temp[0]
		} else {
			primes[0] = temp[0]
			primes[1] = temp[1]
		}
	} else {
		if firstDifferent%2 == 1 {
			primes[0] = temp[0]
			primes[1] = temp[1]
		} else {
			primes[0] = temp[1]
			primes[1] = temp[0]
		}
	}
	// compute rest of primes
	for i := 1; i < L; i++ {
		// fmt.Println(primes[:i+1], Ls[i])
		primes[i+1] = Ls[i] / primes[i]
	}
	// unique values
	mapUnique := make(map[int]bool)
	for _, v := range primes {
		mapUnique[v] = true
	}
	sorted := make([]int, 26)
	// sort primes to know which letter corresponds
	i := 0
	for key := range mapUnique {
		sorted[i] = key
		i++
	}
	sort.Ints(sorted)
	// fins by order the sentence
	var buffer bytes.Buffer // fast string concatenation
	for i := 0; i < L+1; i++ {
		num := primes[i]
		var j int
		for j = 0; j < L+1; j++ {
			if sorted[j] == num {
				if j < 26 {
					buffer.WriteString(string(rune(int('A') + j)))
				}
				break
			}
		}
	}
	return buffer.String()
}

// BigIntSlice stores *big.Ints
type BigIntSlice []*big.Int

func (s BigIntSlice) Len() int           { return len(s) }
func (s BigIntSlice) Less(i, j int) bool { return s[i].Cmp(s[j]) < 0 }
func (s BigIntSlice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

// SearchBigInts searches for x in a sorted slice of *big.Int and returns the index
// as specified by sort.Search. The slice must be sorted in ascending order.
func SearchBigInts(a BigIntSlice, x *big.Int) int {
	return sort.Search(len(a), func(i int) bool { return a[i].Cmp(x) >= 0 })
}

func bigIntScanln(n int) (BigIntSlice, error) {
	x := make([]string, n)
	y := make([]interface{}, len(x))
	for i := range x {
		y[i] = &x[i]
	}
	n, err := fmt.Scanln(y...)
	x = x[:n]
	// convert to big.Int
	s := make(BigIntSlice, n)
	for i, v := range x {
		s[i] = big.NewInt(0)
		s[i].SetString(v, 10)
	}
	return s, err
}

func solve(N big.Int, L int, Ls BigIntSlice) string {
	// declare a zero for comparison
	var zero = big.NewInt(0)
	// declare seprated primes that we have to find
	primes := make(BigIntSlice, L+1) // one more prime than products
	// try to find gcd of a single pair
	var i int
	for i = 0; i < L-1; i++ {
		z := big.NewInt(0)
		z.GCD(nil, nil, Ls[i], Ls[i+1])
		if (z.Cmp(zero) != 0) && (z.Cmp(Ls[i]) != 0) && (z.Cmp(Ls[i+1]) != 0) {
			primes[i+1] = z // hte known prime is i + 1
			break
		}
	}
	// fill all the other numbers by just dividing
	for j := i + 2; j < L+1; j++ {
		primes[j] = big.NewInt(0)
		primes[j].Div(Ls[j-1], primes[j-1])
	}
	for j := i; j >= 0; j-- {
		primes[j] = big.NewInt(0)
		primes[j].Div(Ls[j], primes[j+1])
	}
	// unique values
	mapUnique := make(map[string]bool)
	for _, v := range primes {
		mapUnique[v.String()] = true
	}
	// sort primes to know which letter corresponds
	sorted := make(BigIntSlice, 26)
	i = 0
	for key := range mapUnique {
		sorted[i] = big.NewInt(0)
		sorted[i].SetString(key, 10)
		i++
	}
	sort.Sort(sorted)
	// finds by order the sentence
	var buffer bytes.Buffer // fast string concatenation
	for i := 0; i < L+1; i++ {
		num := primes[i]
		index := SearchBigInts(sorted, num)
		buffer.WriteString(string(rune(int('A') + index)))
	}
	return buffer.String()
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var N big.Int // max number prime, len of numbers
		var L int
		fmt.Scanln(&N, &L)
		Ls, _ := bigIntScanln(L) // prime1 * prime2, prime2 * prime3, ...
		// Solve
		answer := solve(N, L, Ls)
		// Show result
		fmt.Printf("Case #%d: %s\n", t+1, answer)
	}
}
