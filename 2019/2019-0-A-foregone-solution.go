package main

import (
	"bytes"
	"fmt"
)

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var N string // number
		fmt.Scanln(&N)
		// Solve
		var Abuffer bytes.Buffer // fast string concatenation
		var Bbuffer bytes.Buffer
		for _, c := range N {
			if c == '4' {
				Abuffer.WriteString("2")
				Bbuffer.WriteString("2")
			} else {
				Abuffer.WriteString(string(c))
				Bbuffer.WriteString("0")
			}
		}
		// Show result
		fmt.Printf("Case #%d: %s %s\n", t+1, Abuffer.String(), Bbuffer.String())
	}
}
