package main

import (
	"bytes"
	"fmt"
)

func opposite(C rune) string {
	if C == 'S' {
		return "E"
	}
	return "S"
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var N int // number
		fmt.Scanln(&N)
		var Ps string // movements [E, S]
		fmt.Scanln(&Ps)
		// Solve
		var buffer bytes.Buffer // fast string concatenation
		for _, c := range Ps {
			buffer.WriteString(opposite(c))
		}
		// Show result
		fmt.Printf("Case #%d: %s\n", t+1, buffer.String())
	}
}
