/**
Solve Qualification Round D 2017.

https://code.google.com/codejam/contest/3264486/dashboard#s=p3

Problem


*/

#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

void printGrid(const vector<vector<char>> &grid) {
  for (int i = 0; i < grid.size(); i++) {
    for (int j = 0; j < grid[i].size(); j++) {
      cout << grid[i][j];
    }
    cout << endl;
  }
  cout << endl;
}

vector<vector<char>> solveTowers(const vector<vector<char>> &grid) {
  // towers (only one in each column or row)
  vector<vector<char>> towers(grid.size(), vector<char>(grid.size(), '.'));
  // rows
  vector<int> rows;
  for (int i = 0; i < grid.size(); i++) {
    bool tower = false;
    for (int j = 0; j < grid.size(); j++) {
      if (grid[i][j] == 'x') {
        tower = true;
        towers[i][j] = 'x';
      } else if (grid[i][j] == 'o') {
        tower = true;
        towers[i][j] = 'x';
      }
    }
    if (!tower) {
      rows.push_back(i);
    }
  }
  // columns
  vector<int> cols;
  for (int i = 0; i < grid.size(); i++) {
    bool tower = false;
    for (int j = 0; j < grid.size(); j++) {
      if (grid[j][i] == 'x') {
        tower = true;
      } else if (grid[j][i] == 'o') {
        tower = true;
      }
    }
    if (!tower) {
      cols.push_back(i);
    }
  }
  assert(rows.size() == cols.size());
  // add them
  for (int i = 0; i < rows.size(); i++) {
    towers[rows[i]][cols[i]] = 'x';
  }
  // return
  return towers;
}

vector<vector<char>> solveBishops(const vector<vector<char>> &grid) {
  // towers (only one in each column or row)
  vector<vector<char>> bishops(grid.size(), vector<char>(grid.size(), '.'));
  for (int i = 0; i < grid.size(); i++) {
    for (int j = 0; j < grid.size(); j++) {
      if (grid[i][j] == '+') {
        bishops[i][j] = '+';
      } else if (grid[i][j] == 'o') {
        bishops[i][j] = '+';
      }
    }
  }
  printGrid(bishops);
  // return
  return bishops;
}

int main() {
  // Test cases
  int T;
  cin >> T;
  for (int t = 1; t <= T; t++) {
    // Grid size and provided models
    unsigned int N, M;
    cin >> N >> M;
    // Init the grid
    vector<vector<char>> grid(N, vector<char>(N, '.'));
    // Read the already positioned
    for (unsigned int m = 0; m < M; m++) {
      char c;
      unsigned int R, C;
      cin >> c >> R >> C;
      grid[R - 1][C - 1] = c;
    }
    printGrid(grid);
    // Solve problem
    // ===========================================================
    // + are bishops
    // x are towers/rooks
    // o are queens
    vector<vector<char>> towers = solveTowers(grid);
    vector<vector<char>> bishops = solveBishops(grid);
    // Combine solutions
    int changes = 0;
    int style = 0;
    stringstream output;
    vector<vector<char>> result(N, vector<char>(N, '.'));
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        // Final grid
        if ((towers[i][j] != '.') && (bishops[i][j] != '.')) {
          result[i][j] = 'o';
        } else if (towers[i][j] != '.') {
          result[i][j] = 'x';
        } else if (bishops[i][j] != '.') {
          result[i][j] = '+';
        }
        // Check differences
        if (result[i][j] != grid[i][j]) {
          changes++;
          output << result[i][j] << " " << i + 1 << " " << j + 1 << endl;
        }

        // Count style points
        if (result[i][j] == 'o') {
          style++;
          style++;
        } else if (result[i][j] == 'x') {
          style++;
        } else if (result[i][j] == '+') {
          style++;
        }
      }
    }

    cout << "Case #" << t << ": " << style << " " << changes << endl;
    cout << output.str();

  } // end for T
  return 0;
}
