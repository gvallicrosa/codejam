#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>
using namespace std;

double compute(const vector<double> &P, const int &K) {
  double prob = 0.0;
  int N = P.size();
  // combinations
  vector<bool> v(N, false);
  fill(v.begin(), v.begin() + K, true);
  do {
    double temp = 1.0;
    for (int i = 0; i < N; ++i) {
      // cout << v[i] << " " << P[i] << endl;
      if (v[i]) {
        temp *= P[i]; // success
      } else {
        temp *= (1.0 - P[i]); // fail
      }
      // cout << temp << endl;
    }
    prob += temp;
  } while (std::prev_permutation(v.begin(), v.end()));
  return prob;
}

int main() {
  // Test cases
  int T;
  cin >> T;
  for (int t = 1; t <= T; t++) {
    // total, minimum
    int N, K;
    cin >> N >> K;
    double U;
    cin >> U;
    vector<double> P(N);
    double psum = 0.0;
    for (int i = 0; i < N; i++) {
      double p;
      cin >> p;
      P[i] = p;
      psum += p;
    }
    sort(P.begin(), P.end());

    double prob = 0.0;
    if (N == 1) {
      // increase at maximum
      prob = P[0] + U;
    } else if (U == static_cast<double>(N) - psum) {
      // check if arrive to 1.0
      prob = 1.0;
    } else if (N == K) {
      // small 1
      // increase the smaller ones
      bool finished = false;
      int idsec = 1;
      int num = 0;
      double tospend = 0.0;
      while (!finished) {
        if ((P[idsec] - P[0]) > 0) {
          num = idsec;
          tospend = P[idsec] - P[0];
          if (tospend * num <= U) {
            U -= tospend * num;
          } else {
            tospend = U / static_cast<double>(num);
            U = 0.0;
            finished = true;
          }
          // spend the learning
          for (int i = 0; i < idsec; i++) {
            P[i] += tospend;
          }
        }
        // increase the small index
        idsec++;
        if (idsec == N) {
          finished = true;
        }
      }
      // Spend remaining U overall
      if (U > 0.0) {
        tospend = U / static_cast<double>(N);
        for (int i = 0; i < N; i++) {
          P[i] += tospend;
        }
      }
      // Final value
      prob = 1.0;
      for (int i = 0; i < N; i++) {
        prob *= P[i];
      }
    } else {
      // small 2
      // K < N
      sort(P.begin(), P.end(), greater<double>());
      // is it possible ? meaning no too many zeros ?
      bool possible = true;
      for (int i = 0; i < K; i++) {
        if (P[i] == 0.0) {
          possible = false;
        }
      }
      if (possible) {
        // increase the biggest ones
        for (int i = 0; i < N; i++) {
          double tospend = 1.0 - P[i];
          if (tospend <= U) {
            P[i] = 1.0;
            U -= tospend;
          } else {
            P[i] += U;
            U = 0;
            break;
          }
        }
      } else {
        // must increase some zeros to something
        cout << "missing condition" << endl;
      }

      // final computation
      // check smaller path
      bool reverse = true;
      if ((N - K) > K) {
        reverse = false;
        K = N - K;
      }
      // compute
      prob = 0.0;
      for (int i = 0; i < K; i++) {
        double temp = compute(P, i);
        prob += temp;
        // cout << "  " << temp << endl;
      }
      // cout << prob << endl;

      if (reverse) {
        prob = 1.0 - prob;
      }
    }
    cout << "Case #" << t << ": " << fixed << setprecision(8) << prob << endl;
  } // for t
  return 0;
}
