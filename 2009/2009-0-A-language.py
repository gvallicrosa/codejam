#! /#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Round Qualification A 2009.

https://code.google.com/codejam/contest/90101/dashboard#s=p0

Problem

After years of study, scientists at Google Labs have discovered an alien
language transmitted from a faraway planet. The alien language is very unique
in that every word consists of exactly L lowercase letters. Also, there are
exactly D words in this language.

Once the dictionary of all the words in the alien language was built, the next
breakthrough was to discover that the aliens have been transmitting messages to
Earth for the past decade. Unfortunately, these signals are weakened due to the
distance between our two planets and some of the words may be misinterpreted.
In order to help them decipher these messages, the scientists have asked you to
devise an algorithm that will determine the number of possible interpretations
for a given pattern.

A pattern consists of exactly L tokens. Each token is either a single lowercase
letter (the scientists are very sure that this is the letter) or a group of
unique lowercase letters surrounded by parenthesis ( and ). For example:
(ab)d(dc) means the first letter is either a or b, the second letter is
definitely d and the last letter is either d or c. Therefore, the pattern
(ab)d(dc) can stand for either one of these 4 possibilities: add, adc, bdd,
bdc.

Input

The first line of input contains 3 integers, L, D and N separated by a space.
D lines follow, each containing one word of length L. These are the words that
are known to exist in the alien language. N test cases then follow, each on its
own line and each consisting of a pattern as described above. You may assume
that all known words provided are unique.

Output

For each test case, output

Case #X: K
where X is the test case number, starting from 1, and K indicates how many
words in the alien language match the pattern.

Limits

Small dataset

1 ≤ L ≤ 10
1 ≤ D ≤ 25
1 ≤ N ≤ 10
Large dataset

1 ≤ L ≤ 15
1 ≤ D ≤ 5000
1 ≤ N ≤ 500

Sample

Input

3 5 4
abc
bca
dac
dbc
cba
(ab)(bc)(ca)
abc
(abc)(abc)(abc)
(zyx)bc

Output

Case #1: 2
Case #2: 1
Case #3: 3
Case #4: 0
"""

import sys

if __name__ == '__main__':
    # Open file and read header
    fh = open(sys.argv[1], 'r')
    line = fh.readline()
    line = line.split()
    L = int(line[0])  # different lowercase letters
    D = int(line[1])  # words in dictionary
    N = int(line[2])  # possible inputs

    # Read the words in dictionary
    words = list()
    for i in range(D):
        words.append(fh.readline().split()[0])

    # Read possible inputs
    for i in range(N):
        # Patterns init
        patterns = list()
        for j in range(L):
            patterns.append(list())

        # Patterns input
        text = fh.readline().split()[0]
        inpar = False
        index = 0
        for c in text:
            if c == '(':
                inpar = True
            elif c == ')':
                inpar = False
                index += 1
            else:
                patterns[index].append(c)
                if not inpar:
                    index += 1

        # Check each dictionary word with the patterns to see if it fits
        good = 0
        for word in words:
            ok = True
            if not len(word) == len(patterns):
                continue
            for j in range(len(word)):
                if word[j] not in patterns[j]:
                    ok = False
                    break
            if ok:
                good += 1

        # Return result
        print('Case #{:d}: {:d}'.format(i + 1, good))
