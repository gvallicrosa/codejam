package main

import (
	"fmt"
	"sort"
)

func troubleSort(L []int) {
	done := false
	for !done {
		done = true
		for i := 0; i < len(L)-2; i++ {
			if L[i] > L[i+2] {
				done = false
				// reverse the sublist from L[i] to L[i+2], inclusive
				L[i], L[i+2] = L[i+2], L[i]
			}
		}
	}
	// return L
}

func solve(L []int) int {
	// Bubble sort
	b := make([]int, len(L))
	copy(b, L)
	sort.Ints(b)
	// TroubleSort
	troubleSort(L)
	// print
	// fmt.Println(b)
	// fmt.Println(L)
	// check
	for i := range L {
		if L[i] != b[i] {
			return i
		}
	}
	return -1
}

func intScanln(n int) ([]int, error) {
	x := make([]int, n)
	y := make([]interface{}, len(x))
	for i := range x {
		y[i] = &x[i]
	}
	n, err := fmt.Scanln(y...)
	x = x[:n]
	return x, err
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var A int // minimum prepared area
		fmt.Scanln(&A)
		L, _ := intScanln(N) // list of ints
		// Solve
		r := solve(L)
		if r < 0 {
			fmt.Printf("Case #%d: OK\n", t+1)
		} else {
			fmt.Printf("Case #%d: %d\n", t+1, r)
		}

	}
}
