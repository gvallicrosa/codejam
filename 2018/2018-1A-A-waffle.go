package main

import (
	"fmt"
)

type Waffle []string
type Waffles []Waffle

func (W Waffle) cutRows(indexs []int) Waffles {
	// delete last index since is = R
	// indexs = indexs[0 : len(indexs)-1]
	// cuts after the index
	var lastIndex = 0
	var Ws = make(Waffles, len(indexs))
	for i, idx := range indexs {
		var rows = 1 + idx - lastIndex
		Ws[i] = make(Waffle, rows)
		for r := 0; r < rows; r++ {
			Ws[i][r] = W[lastIndex+r]
		}
		lastIndex = idx + 1
	}
	return Ws
}

func solve(R, C, H, V int, W Waffle) int {
	// Count all chocolate
	var choc = 0
	var rowChoc = make([]int, R)
	// var colChoc = make([]int, C)
	for i := 0; i < R; i++ {
		for j := 0; j < C; j++ {
			if W[i][j] == '@' {
				choc++
				rowChoc[i]++
				// colChoc[j]++
			}
		}
	}
	// Check no chocolate
	if H < R && V < C && choc == 0 {
		// fmt.Println("no chocolate")
		return 1
	}
	// Check total cuts
	var totalParts = (H + 1) * (V + 1)
	if choc%totalParts != 0 {
		// fmt.Println("bad total cuts")
		return -1
	}
	// Check row cuts
	if choc%(H+1) != 0 {
		// fmt.Println("bad row cuts")
		return -1
	}
	// Check col cuts
	if choc%(V+1) != 0 {
		// fmt.Println("bad col cuts")
		return -1
	}
	// Cut rows
	var rowParts = choc / (H + 1)
	var count = 0
	var rowIndexs = make([]int, 0)
	for i := 0; i < R; i++ {
		count += rowChoc[i]
		if count == rowParts { // accumulated enough
			rowIndexs = append(rowIndexs, i)
			count = 0
		}
	}
	// fmt.Println(rowIndexs)
	if count != 0 {
		// fmt.Println("impossible to cut rows")
		return -1 // all correctly cut
	}
	var Ws = W.cutRows(rowIndexs)
	// fmt.Println(W)
	// fmt.Println(Ws)
	// Check cut columns
	var cuts = 0
	var colParts = choc / totalParts
	// fmt.Println("colparts", colParts)
	var counters = make([]int, len(Ws))
	// For each column
	for c := 0; c < C; c++ {
		// fmt.Println("col", c)
		// Count chocolate in each waffle
		for i, w := range Ws {
			// Check all rows
			for r := 0; r < len(w); r++ {
				if w[r][c] == '@' {
					counters[i]++
				}
			}
		}
		// Check if all add up
		var done = true
		for _, c := range counters {
			if c > colParts {
				// impossible to cut
				return -1
			}
			if c != colParts {
				done = false
				break
			}
		}
		// simulate cut
		if done {
			cuts++
			// fmt.Println("enoguh")
			for i := range counters {
				counters[i] = 0
			}
		}
		// fmt.Println(counters)
	}
	// assert cut
	if cuts != V+1 {
		return -1
	}
	for _, c := range counters {
		if c != 0 {
			return -1
		}
	}
	// If arrived here, then is possible
	return 0
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var R, C, H, V int // rows, cols, horiz cuts, vert cuts
		fmt.Scanln(&R, &C, &H, &V)
		var W = make(Waffle, R) // waffle description
		var temp string
		for i := 0; i < R; i++ {
			fmt.Scanln(&temp)
			W[i] = temp
		}
		// Solve
		r := solve(R, C, H, V, W)
		if r < 0 {
			fmt.Printf("Case #%d: IMPOSSIBLE\n", t+1)
		} else {
			fmt.Printf("Case #%d: POSSIBLE\n", t+1)
		}
	}
}
