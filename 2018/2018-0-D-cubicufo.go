package main

import (
	"fmt"
	"math"
	"sort"
)

type matrix3d [3][3]float64
type vector3d [3]float64
type point struct {
	X, Y float64
}
type line [2]point
type points []point

func (p points) Len() int      { return len(p) }
func (p points) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p points) Less(i, j int) bool {
	if p[i].X == p[j].X {
		return p[i].Y < p[i].Y
	}
	return p[i].X < p[j].X
}

// ConvexHull returns the set of points that define the convex hull of p in CCW order starting from the left most.
func (p points) convexHull() points {
	// From https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
	// with only minor deviations.
	sort.Sort(p)
	var h points

	// Lower hull
	for _, pt := range p {
		for len(h) >= 2 && !ccw(h[len(h)-2], h[len(h)-1], pt) {
			h = h[:len(h)-1]
		}
		h = append(h, pt)
	}

	// Upper hull
	for i, t := len(p)-2, len(h)+1; i >= 0; i-- {
		pt := p[i]
		for len(h) >= t && !ccw(h[len(h)-2], h[len(h)-1], pt) {
			h = h[:len(h)-1]
		}
		h = append(h, pt)
	}

	return h[:len(h)-1]
}

// ccw returns true if the three points make a counter-clockwise turn
func ccw(a, b, c point) bool {
	return ((b.X - a.X) * (c.Y - a.Y)) > ((b.Y - a.Y) * (c.X - a.X))
}

func (m matrix3d) dot(v vector3d) vector3d {
	var r vector3d
	for i := 0; i < 3; i++ {
		// r[i] = 0.0
		for j := 0; j < 3; j++ {
			r[i] += m[i][j] * v[j]
		}
	}
	return r
}

func euler2rotationmatrix(r, p, y float64) matrix3d {
	sa := math.Sin(r)
	ca := math.Cos(r)
	sb := math.Sin(p)
	cb := math.Cos(p)
	sc := math.Sin(y)
	cc := math.Cos(y)
	// R = RotZ(c)RotY(b)RotX(a)
	// [cbcc  sasbcc-casc  casbcc+sasc]
	// [cbsc  sasbsc+cacc  casbsc-sacc]
	// [-sb         sacb         cacb]
	var M matrix3d
	M[0][0] = cb * cc
	M[0][1] = sa*sb*cc - ca*sc
	M[0][2] = ca*sb*cc + sa*sc
	M[1][0] = cb * sc
	M[1][1] = sa*sb*sc + ca*cc
	M[1][2] = ca*sb*sc - sa*cc
	M[2][0] = -sb
	M[2][1] = sa * cb
	M[2][2] = ca * cb
	return M
}

func shadowArea(vertices [8]vector3d, r, p, y float64) float64 {
	// do the rotation
	rot := euler2rotationmatrix(r, p, y)
	var rvert [8]vector3d
	for i := 0; i < 8; i++ {
		rvert[i] = rot.dot(vertices[i])
	}
	// do a convex hull of x and z values
	pts := make(points, 8)
	for i := 0; i < 8; i++ {
		p := point{rvert[i][0], rvert[i][2]}
		pts[i] = p
	}
	hull := pts.convexHull()
	// compute area of convex hull
	lines := make([]line, len(hull))
	for i := range hull {
		l := line{hull[i], hull[(i+1)%len(hull)]}
		lines[i] = l
	}
	s := 0.0
	for _, l := range lines {
		s += l[0].X*l[1].Y - l[1].X*l[0].Y
	}
	return 0.5 * math.Abs(s)
}

func solve(A float64) [3]vector3d {
	// fmt.Println(">> A", A)
	// fmt.Println("sqrt2 ", math.Sqrt(2.0))
	// faces
	var faces [3]vector3d
	faces[0] = [3]float64{0.5, 0.0, 0.0}
	faces[1] = [3]float64{0.0, 0.5, 0.0}
	faces[2] = [3]float64{0.0, 0.0, 0.5}
	// vertices
	var vertices [8]vector3d
	vertices[0] = [3]float64{+0.5, +0.5, +0.5}
	vertices[1] = [3]float64{-0.5, +0.5, +0.5}
	vertices[2] = [3]float64{+0.5, -0.5, +0.5}
	vertices[3] = [3]float64{+0.5, +0.5, -0.5}
	vertices[4] = [3]float64{-0.5, -0.5, +0.5}
	vertices[5] = [3]float64{+0.5, -0.5, -0.5}
	vertices[6] = [3]float64{-0.5, +0.5, -0.5}
	vertices[7] = [3]float64{-0.5, -0.5, -0.5}
	// solution
	r45 := 0.25 * math.Pi
	var rot matrix3d
	if math.Abs(A-1.0) < 1e-6 {
		// Minimum
		return faces
	} else if math.Abs(A-math.Sqrt(2.0)) < 1e-6 {
		// Max of first rotation
		rot = euler2rotationmatrix(r45, 0.0, 0.0)
	} else if math.Abs(A-1.732050) < 1e-6 {
		// Maximum value
		y := 35.26439 * math.Pi / 180.0
		rot = euler2rotationmatrix(r45, 0.0, y)
	} else {
		// Any other rotation
		if A < math.Sqrt(2.0) {
			// Bisection method
			a := 0.0 // 0 deg
			fa := 1.0 - A
			b := r45 // 45 deg
			// fb := math.Sqrt(2.0) - A
			m := 0.5 * (a + b)
			fm := shadowArea(vertices, m, 0.0, 0.0) - A
			for math.Abs(fm) > 1e-9 {
				if fa*fm < 0.0 {
					b = m
					// fb = fm
				} else {
					a = m
					fa = fm
				}
				// new
				m = 0.5 * (a + b)
				fm = shadowArea(vertices, m, 0.0, 0.0) - A
			}
			// found
			rot = euler2rotationmatrix(m, 0.0, 0.0)
		} else {
			// Bisection method
			a := 0.0 // 0 deg
			fa := math.Sqrt(2.0) - A
			b := 35.26439 * math.Pi / 180.0 // maximum precomputed
			// fb := shadowArea(vertices, r45, 0.0, b)
			// fmt.Println(a, fa, b, fb)
			m := 0.5 * (a + b)
			fm := shadowArea(vertices, r45, 0.0, m) - A
			for math.Abs(fm) > 1e-9 {
				if fa*fm < 0.0 {
					b = m
					// fb = fm
				} else {
					a = m
					fa = fm
				}
				// new
				m = 0.5 * (a + b)
				// fmt.Println(a, b, "=>", m)
				// fmt.Println("f()", fa, fb, fm)
				// time.Sleep(500 * time.Millisecond)
				fm = shadowArea(vertices, r45, 0.0, m) - A
			}
			// found
			// fmt.Println("solution:", fm)
			rot = euler2rotationmatrix(r45, 0.0, m)
		}
	}
	// final rotation
	var r [3]vector3d
	for i := 0; i < 3; i++ {
		r[i] = rot.dot(faces[i])
	}
	return r
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var A float64 // area to cover
		fmt.Scanln(&A)
		// Solve
		// fmt.Println(A)
		r := solve(A)
		fmt.Printf("Case #%d:\n", t+1)
		for i := 0; i < 3; i++ {
			fmt.Printf("%.16f %.16f %.16f\n", r[i][0], r[i][1], r[i][2])
		}
	}
}
