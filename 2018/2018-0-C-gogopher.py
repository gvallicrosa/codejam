#!/usr/bin/env python3
# import numpy as np
import sys

# offsets to center the rectangle
dx, dy = 500, 500


class Grid(object):
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.shape = [rows, cols]
        self.data = [[0, ] * cols for r in range(rows)]
        assert len(self.data) == rows
        assert len(self.data[0]) == cols

    def sum(self, mini, maxi, minj, maxj):
        s = 0
        for i in range(mini, maxi):
            for j in range(minj, maxj):
                s += self.data[i][j]
        return s

    def set_value(self, i, j, v):
        self.data[i][j] = v

    def __str__(self):
        lines = list()
        for i in range(self.rows):
            chars = [str(v) for v in self.data[i]]
            line = " ".join(chars)
            lines.append(line)
        return '\n'.join(lines)


def increase(i, j, g):
    lastrow = (i == g.shape[0] - 2)
    lastcol = (j == g.shape[1] - 2)
    if (not lastrow) and (not lastcol):
        # move down
        return i + 1, j
    elif lastrow and (not lastcol):
        # move right and reset row
        return 1, min(j + 2, g.shape[1] - 2)
    elif (not lastrow) and lastcol:
        # move down
        return i + 1, j
    else:
        # impossible
        pass


def check(i, j, g):
    lastrow = (i == g.shape[0] - 2)
    lastcol = (j == g.shape[1] - 2)
    # sum up to
    if (not lastrow) and (not lastcol):
        # check top left 1x2 [i-1][j-1, j]
        return g.sum(i - 1, i, j - 1, j + 1) == 2
    elif lastrow and (not lastcol):
        # check bottom left 3x2 [i-1,i,i+1][j-1, j]
        return g.sum(i - 1, i + 2, j - 1, j + 1) == 6
    elif (not lastrow) and lastcol:
        # check top right 1x3 [i-1][j-1, j, j+1]
        return g.sum(i - 1, i, j - 1, j + 2) == 3
    else:
        # check bottom right 3x3 [i-1,i,i+1][j-1, j, j+1]
        return g.sum(i - 1, i + 2, j - 1, j + 2) == 9


def solve(A):
    if A == 20:
        # make a 4x5 square
        g = Grid(4, 5)
    elif A == 200:
        # make a 10x20 square
        g = Grid(10, 20)
    # initial position
    print(g.shape, file=sys.stderr)
    count = 0
    i, j = 1, 1
    while count < 1000:
        ok = check(i, j, g)
        if ok:
            # advance
            i, j = increase(i, j, g)
            # print(i, j, file=sys.stderr)
        else:
            # send cell
            count += 1
            print(i + dx, j + dy)
            # read answer
            ans = [int(v) for v in input().split()]
            if ans[0] == 0 and ans[1] == 0:
                break  # done
            # change value
            a, b = ans[0] - dx, ans[1] - dy
            g.set_value(a, b, 1)
    print("done", count, file=sys.stderr)
    print(g, file=sys.stderr)


if __name__ == '__main__':
    # Test cases
    T = int(input())
    for t in range(T):
        # Read problem
        A = int(input())
        # Solve
        solve(A)
