package main

import (
	"fmt"
	"sort"
)

func min64(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

func max64(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

func main() {
	// Pre-allocate
	var M, S, P [1000]int64
	var CAPS [1000]int
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var R, B, C int64 // robots, bits, cashiers
		fmt.Scanln(&R, &B, &C)
		for i := 0; i < int(C); i++ {
			fmt.Scanln(&M[i], &S[i], &P[i])
		}
		// Solve
		var minTime int64 = 2                   // 1 bit + 1 extra
		var maxTime int64 = 2000000000000000000 // 2e18
		for minTime < maxTime {
			var time = (maxTime + minTime) / 2 // take middle
			// current capacities
			for i := 0; i < int(C); i++ {
				CAPS[i] = int(max64(0, min64(M[i], (time-P[i])/S[i])))
			}
			// actual capacity
			sort.Sort(sort.Reverse(sort.IntSlice(CAPS[0:C])))
			var c int64
			for i := 0; i < int(R); i++ {
				c += int64(CAPS[i])
			}
			// check
			if c >= B {
				maxTime = time
			} else {
				minTime = time + 1
			}
		}
		fmt.Printf("Case #%d: %d\n", t+1, minTime)
	}
}
