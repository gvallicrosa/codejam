package main

import (
	"fmt"
	"strings"
)

func solve(D int, P string) int {
	// Check impossible by getting smallest possible value
	minval := 0
	for _, c := range P {
		if c == 'S' {
			minval++
		}
	}

	// fmt.Println(D, P)
	// fmt.Println("min", minval)
	if minval > D {
		return -1 // impossible
	}
	// Check current
	ok := check(D, P)
	if ok {
		return 0
	}
	// Do some movements
	// Biggest change is to swap the last CS
	swaps := 0
	for !ok {
		// need to look for the last CS
		idx := strings.LastIndex(P, "CS")
		// swap values
		prev := P[:idx]
		post := P[idx+2:]
		P = prev + "SC" + post
		swaps++
		// check
		ok = check(D, P)
	}
	return swaps
}

func check(D int, P string) bool {
	// Check current
	b := 1
	d := 0
	for _, c := range P {
		if c == 'S' {
			d += b
		}
		if c == 'C' {
			b *= 2
		}
	}
	if d > D {
		return false
	}
	return true
}

func main() {
	// Test cases
	var T int // number of test cases
	fmt.Scanln(&T)
	for t := 0; t < T; t++ {
		// Read problem
		var D int    // maximum damage
		var P string // program
		fmt.Scanln(&D, &P)
		// Solve
		r := solve(D, P)
		if r < 0 {
			fmt.Printf("Case #%d: IMPOSSIBLE\n", t+1)
		} else {
			fmt.Printf("Case #%d: %d\n", t+1, r)
		}

	}
}
