#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Round Qualification C 2015.

https://code.google.com/codejam/contest/6224486/dashboard#s=p2

Problem

The Dutch computer scientist Edsger Dijkstra made many important contributions
to the field, including the shortest path finding algorithm that bears his
name. This problem is not about that algorithm.

You were marked down one point on an algorithms exam for misspelling
"Dijkstra" -- between D and stra, you wrote some number of characters, each of
which was either i, j, or k. You are prepared to argue to get your point back
using quaternions, an actual number system (extended from complex numbers) with
the following multiplicative structure:

[image]

To multiply one quaternion by another, look at the row for the first quaternion
and the column for the second quaternion. For example, to multiply i by j,
look in the row for i and the column for j to find that the answer is k. To
multiply j by i, look in the row for j and the column for i to find that the
answer is -k.

As you can see from the above examples, the quaternions are not
commutative -- that is, there are some a and b for which a * b != b * a.
However they are associative -- for any a, b, and c, it's true
that a * (b * c) = (a * b) * c.

Negative signs before quaternions work as they normally do -- for any
quaternions a and b, it's true that -a * -b = a * b,
and -a * b = a * -b = -(a * b).

You want to argue that your misspelling was equivalent to the correct spelling
ijk by showing that you can split your string of is, js, and ks in two places,
forming three substrings, such that the leftmost substring reduces (under
quaternion multiplication) to i, the middle substring reduces to j, and the
right substring reduces to k. (For example, jij would be interpreted
as j * i * j; j * i is -k, and -k * j is i, so jij reduces to i.) If this is
possible, you will get your point back. Can you find a way to do it?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each consists of one line with two space-separated integers L and X,
followed by another line with L characters, all of which are i, j, or k. Note
that the string never contains negative signs, 1s, or any other characters. The
string that you are to evaluate is the given string of L characters repeated X
times. For instance, for L = 4, X = 3, and the given string kiij, your input
string would be kiijkiijkiij.

Output

For each test case, output one line containing "Case #x: y", where x is the
test case number (starting from 1) and y is either YES or NO, depending on
whether the string can be broken into three parts that reduce to i, j, and k,
in that order, as described above.

Limits

1 ≤ T ≤ 100.
1 ≤ L ≤ 10000.

Small dataset

1 ≤ X ≤ 10000.
1 ≤ L * X ≤ 10000.

Large dataset

1 ≤ X ≤ 1012.
1 ≤ L * X ≤ 1016.

Sample

Input

5
2 1
ik
3 1
ijk
3 1
kji
2 6
ji
1 10000
i

Output

Case #1: NO
Case #2: YES
Case #3: NO
Case #4: YES
Case #5: NO

In Case #1, the string is too short to be split into three substrings.

In Case #2, just split the string into i, j, and k.

In Case #3, the only way to split the string into three parts is k, j, i, and
this does not satisfy the conditions.

In Case #4, the string is jijijijijiji. It can be split into jij (which reduces
to i), iji (which reduces to j), and jijiji (which reduces to k).

In Case #5, no matter how you choose your substrings, none of them can ever
reduce to a j or a k.
"""

import sys
import numpy as np

# Single


def read_word(f):
    return f.readline().strip()


def read_int(f, b=10):
    return int(read_word(f), b)


def read_float(f):
    return float(read_word(f))

# Multiple


def read_words(f):
    return read_word(f).split(' ')


def read_ints(f, b=10):
    return [int(x, b) for x in read_words(f)]


def read_floats(f):
    return [float(x) for x in read_words(f)]

# The table
table = {('1', '1'): '1', ('1', 'i'): 'i', ('1', 'j'): 'j', ('1', 'k'): 'k',
         ('i', '1'): 'i', ('i', 'i'): '-1', ('i', 'j'): 'k', ('i', 'k'): '-j',
         ('j', '1'): 'j', ('j', 'i'): '-k', ('j', 'j'): '-1', ('j', 'k'): 'i',
         ('k', '1'): 'k', ('k', 'i'): 'j', ('k', 'j'): '-i', ('k', 'k'): '-1'}


def find_letter(string, value='i', start=0, end=0):
    '''
    Find if letter is possible.
    '''
    pos = list()
    cur = None
    for i in range(start, end):

        # Current val
        if cur is None:
            cur = string[i]
        else:
            val = table[(cur[-1], string[i])]
            cur = cur[:-1] + val

            # Double minus sign "--i"
            if len(cur) == 3:
                cur = cur[-1]

        # Check
        if cur == value:
            pos.append(i)
    return pos


def checkj(string, start, ends):

    # Loop
    j = start
    cur = None
    ends = ends.tolist()

    if not len(ends) > 0:
        return False

    while j <= ends[-1]:
        if cur is None:
            cur = string[j]
        else:
            val = table[(cur[-1], string[j])]
            cur = cur[:-1] + val

        # Extra minus signs
        if len(cur) % 2 == 1:
            cur = cur[-1]

        if j in ends and cur == 'j':
            return True

        # Next
        j += 1

    return False


def find_letter_back(string, value='z'):
    '''
    Find if letter is possible.
    '''
    pos = list()
    cur = None
    i = len(string) - 1
    while i > 0:

        # Current val
        if cur is None:
            cur = string[i]
        else:
            val = table[(string[i], cur[-1])]
            cur = cur[:-1] + val

            # Double minus sign "--i"
            if len(cur) == 3:
                cur = cur[-1]

        # Check
        if cur == value:
            pos.append(i)

        # Next
        i -= 1

    return pos

# Main
if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("No file provided")
        exit(0)

    fname = sys.argv[1]
    fh = file(fname, 'r')
    fo = file(fname + '.out', 'w')

    # Number of problems
    N = read_int(fh)
    print N
    for n in range(N):

        # Get values
        L, X = read_ints(fh)
        string = read_word(fh)
        print L, X

        # Complete problem (no need for all)
        string = string * min(X, 10)

        # Init
        possible = False
        ipos = find_letter(string, value='i', start=0, end=len(string))
        kpos = find_letter_back(string, value='k')
        # print ipos
        # print kpos
        # Sort
        kpos = kpos[::-1]
        # Array
        iarr = np.array(ipos) + 1
        karr = np.array(kpos) - 1
        for i in ipos:
            kend = karr[karr >= i]
            if checkj(string, start=i, ends=kend):
                possible = True
                break

        print possible

        # Output
        ans = {True: 'YES', False: 'NO'}[possible]
        data = "Case #%d: %s\n" % (n + 1, ans)
        fo.write(data)
