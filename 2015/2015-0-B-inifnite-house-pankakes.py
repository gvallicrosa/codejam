#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Solve Round Qualification B 2015.

https://code.google.com/codejam/contest/6224486/dashboard#s=p1

Problem

At the Infinite House of Pancakes, there are only finitely many pancakes, but
there are infinitely many diners who would be willing to eat them! When the
restaurant opens for breakfast, among the infinitely many diners, exactly D
have non-empty plates; the ith of these has Pi pancakes on his or her plate.
Everyone else has an empty plate.

Normally, every minute, every diner with a non-empty plate will eat one pancake
from his or her plate. However, some minutes may be special. In a special
minute, the head server asks for the diners' attention, chooses a diner with a
non-empty plate, and carefully lifts some number of pancakes off of that
diner's plate and moves those pancakes onto one other diner's (empty or non-
empty) plate. No diners eat during a special minute, because it would be rude.

You are the head server on duty this morning, and it is your job to decide
which minutes, if any, will be special, and which pancakes will move where.
That is, every minute, you can decide to either do nothing and let the diners
eat, or declare a special minute and interrupt the diners to make a single
movement of one or more pancakes, as described above.

Breakfast ends when there are no more pancakes left to eat. How quickly can you
make that happen?

Input

The first line of the input gives the number of test cases, T. T test cases
follow. Each consists of one line with D, the number of diners with non-empty
plates, followed by another line with D space-separated integers representing
the numbers of pancakes on those diners' plates.

Output

For each test case, output one line containing "Case #x: y", where x is the
test case number (starting from 1) and y is the smallest number of minutes
needed to finish the breakfast.

Limits

1 ≤ T ≤ 100.

Small dataset

1 ≤ D ≤ 6.
1 ≤ Pi ≤ 9.

Large dataset

1 ≤ D ≤ 1000.
1 ≤ Pi ≤ 1000.

Sample

Input

3
1
3
4
1 2 1 2
1
4

Output

Case #1: 3
Case #2: 2
Case #3: 3

In Case #1, one diner starts with 3 pancakes and everyone else's plate is
empty. One optimal strategy is:

Minute 1: Do nothing. The diner will eat one pancake.

Minute 2 (special): Interrupt and move one pancake from that diner's stack onto
another diner's empty plate. (Remember that there are always infinitely many
diners with empty plates available, no matter how many diners start off with
pancakes.) No pancakes are eaten during an interruption.

Minute 3: Do nothing. Each of those two diners will eat one of the last two
remaining pancakes.

In Case #2, it is optimal to let the diners eat for 2 minutes, with no
interruptions, during which time they will finish all the pancakes.

In Case #3, one diner starts with 4 pancakes and everyone else's plate is
empty. It is optimal to use the first minute as a special minute to move two
pancakes from the diner's plate to another diner's empty plate, and then do
nothing and let the diners eat for the second and third minutes.
"""

import sys

# Single


def read_word(f):
    return f.readline().strip()


def read_int(f, b=10):
    return int(read_word(f), b)


def read_float(f):
    return float(read_word(f))

# Multiple


def read_words(f):
    return read_word(f).split(' ')


def read_ints(f, b=10):
    return [int(x, b) for x in read_words(f)]


def read_floats(f):
    return [float(x) for x in read_words(f)]

# Main
if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("No file provided")
        exit(0)

    fname = sys.argv[1]
    fh = file(fname, 'r')
    fo = file(fname + '.out', 'w')

    # Number of problems
    N = read_int(fh)
    for n in range(N):

        # Get values
        D = read_int(fh)
        vals = read_ints(fh)
        maxtime = max(vals)

        vals.sort()
        print vals

        # Solve
        passtime = 0
        finished = False
        while not finished:

            # Time normal
            maxval = vals[-1]

            # Time special
            nummax = vals.count(maxval)
            idmax = vals.index(maxval)
            secondmax = max(vals[:idmax]) if idmax > 0 else 0

            # How many divisions?
            timediv = maxval + 1
            i = 2
            extra = 0
            if maxval % i > 0:
                extra = 1
            val = 1 * (i - 1) * nummax + max(maxval / i + extra, secondmax)
            while val <= timediv:
                # Save
                timediv = val
                totake = maxval / i

                # Next loop
                i += 1
                extra = 0
                if maxval % i > 0:
                    extra = 1
                val = 1 * (i - 1) * nummax + max(maxval / i + extra, secondmax)

            if timediv <= maxval:
                # Special
                passtime += 1
                vals[-1] -= totake
                vals.append(totake)
                vals.sort()
                print 'special', i - 1, totake, vals

            else:
                # Normal
                passtime += 1
                finished = True
                for i in range(len(vals)):
                    if vals[i] > 0:
                        vals[i] -= 1

                    if not vals[i] == 0:
                        finished = False
                print 'normal', vals

        print passtime
        assert(maxtime >= passtime)
        raw_input('enter')
        # Output
        data = "Case #%d: %d\n" % (n + 1, passtime)
        fo.write(data)
