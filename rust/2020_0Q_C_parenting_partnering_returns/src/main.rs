use std::io;

struct Activity {
    start: u32,
    stop: u32,
}

impl Activity {
    pub fn overlaps(&self, other: &Activity) -> bool {
        // println!("{} {} : {} {}", self.start, self.stop, other.start, other.stop);
        // starts in the middle of the other
        if (self.start > other.start) && (self.start < other.stop) {
            return true;
        }
        // stops in the middle of the other
        if (self.stop > other.start) && (self.stop < other.stop) {
            return true;
        }
        // other starts in the middle
        if (other.start > self.start) && (other.start < self.stop) {
            return true;
        }
        // other stops in the middle
        if (other.stop > self.start) && (other.stop < self.stop) {
            return true;
        }
        // are on the same time start/stop
        if (self.start == other.start) || (self.stop == other.stop) {
            return true;
        }
        return false;
    }
}

struct Graph {
    vertexs : i32,
    adj : Vec<Vec<i32>>,
}

struct CycleResult {
    pub cycle: bool,
    pub care: Vec<char>,
}

impl Graph {
    pub fn new(num_vertexs: i32) -> Graph {
        Graph{
            vertexs: num_vertexs,
            adj: vec![Vec::<i32>::new(); num_vertexs as usize],
        }
    }

    pub fn add_edge(&mut self, i: i32, j: i32) {
        self.adj[i as usize].push(j);
        self.adj[j as usize].push(i);
    }

    fn is_cyclic_util(&self, v: i32, mut visited: &mut Vec<bool>, mut care: &mut Vec<char>, parent: i32) -> bool {
        // visited
        visited[v as usize] = true;

        // who takes care
        if care[v as usize] == '?' {
            // has parent
            if parent == -1 {
                care[v as usize] = 'C';  // start with C, why not?
            } else {
                if care[parent as usize] == 'C' {
                    care[v as usize] = 'J';
                } else {
                    care[v as usize] = 'C';
                }
            }

        }

        // recurse adjacent
        for i in self.adj[v as usize].iter() {
            if !visited[*i as usize] {
                if self.is_cyclic_util(*i, &mut visited, &mut care, v) {
                    return true;
                }
            }

            // adjacent visited and not parent of current vertex
            else if *i != parent {
                return true;
            }
        }
        false
    }

    pub fn is_cyclic(&self) -> CycleResult {
        // nothing visited yet
        let mut visited = vec![false; self.vertexs as usize];
        let mut care = vec!['?'; self.vertexs as usize];

        // DFS all vertexs
        for u in 0..self.vertexs {
            if !visited[u as usize] {
                if self.is_cyclic_util(u, &mut visited, &mut care, -1) {
                    return CycleResult{
                        cycle: true,
                        care: care,
                    };
                }
            }
        }
        CycleResult{
            cycle: false,
            care: care,
        }
    }
}



fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();
    // println!("{}", problems);

    // for each problem
    for t in 0..problems {
        // read N
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let num = input.trim().parse::<usize>().unwrap();
        // println!("\n\n{}", num);

        // read Activities
        let mut activities = Vec::with_capacity(num);
        for _i in 0..num {
            input.clear();
            io::stdin().read_line(&mut input).unwrap();
            // println!("{}", input.trim());
            let temp: Vec<_> = input.trim().split_whitespace().map(|v| v.parse::<u32>().unwrap()).collect();
            let start = temp[0];
            let stop = temp[1];
            activities.push(Activity { start, stop });
        }

        // check overlaps
        // create graph edges
        let mut graph = Graph::new(num as i32);
        for i in 0..num-1 {
            // println!("");
            for j in i+1..num {
                // println!("{} {}", i, j);
                // println!("{} ", activities[i].overlaps(&activities[j]));
                if activities[i].overlaps(&activities[j]) {
                    // graph[i].push(j);
                    // graph[j].push(i);
                    graph.add_edge(i as i32, j as i32);
                }
            }
            // println!("");
        }
        // println!("cyclic: {}", graph.is_cyclic());
        let solution = graph.is_cyclic();
        if solution.cycle {
            println!("Case #{}: IMPOSSIBLE", t + 1);
            continue;
        }
        // who takes care
        let res : String = solution.care.iter().collect();
        println!("Case #{}: {}", t + 1, res);
    }
}
