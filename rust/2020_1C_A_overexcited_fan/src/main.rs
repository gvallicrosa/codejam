use std::io;

struct Position {
    pub east: i32,
    pub north: i32,
}

impl Position {
    pub fn new(e: i32, n: i32) -> Position {
        Position { east: e, north: n }
    }
    pub fn movement(&mut self, direction: char) {
        match direction {
            'N' => self.north += 1,
            'S' => self.north -= 1,
            'E' => self.east += 1,
            'W' => self.east -= 1,
            _ => panic!("impossible direction"),
        };
    }
    pub fn get_time(&self) -> i32 {
        self.east.abs() + self.north.abs()
    }
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read (X, Y)
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input.trim().split_whitespace().collect();
        let x = temp[0].parse::<i32>().unwrap();
        let y = temp[1].parse::<i32>().unwrap();
        let mut rue = Position::new(x, y);
        let rute = temp[2];
        // println!("({}, {})", rue.east, rue.north);
        // solve
        let mut lowest = -1; // lowest time to catch it
        let mut rue_time = 0; // time of the rue

        // initial position check
        let me = rue.get_time(); // time it takes me to reach here
                                 // rue is here like me of after
        if me <= rue_time {
            // is this faster?
            if lowest == -1 || rue_time < lowest {
                lowest = rue_time;
            }
        }
        // move rue and check
        for c in rute.chars() {
            rue.movement(c); // advance rue
            rue_time += 1; // time when rue is here
            let me = rue.get_time(); // time it takes me to reach here

            // println!(
            //     "({}, {}) rue: {}, me: {}, lowest: {}",
            //     rue.east, rue.north, rue_time, me, lowest
            // );

            // rue is here like me of after
            if me <= rue_time {
                // is this faster?
                if lowest == -1 || rue_time < lowest {
                    lowest = rue_time;
                }
            }
        }

        // solution
        if lowest == -1 {
            println!("Case #{}: IMPOSSIBLE", t + 1);
        } else {
            println!("Case #{}: {}", t + 1, lowest);
        }
    }
}
