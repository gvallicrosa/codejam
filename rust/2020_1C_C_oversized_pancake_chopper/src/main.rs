use std::io;
use std::collections::HashMap;

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read N, D
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input
            .trim()
            .split_whitespace()
            .map(|v| v.parse::<usize>().unwrap())
            .collect();
        let num = temp[0];
        let dinners = temp[1];

        // read slices
        input.clear();
        let slices: Vec<_> = input
            .trim()
            .split_whitespace()
            .map(|v| v.parse::<usize>().unwrap())
            .collect();

        // solve
        // count slices of each size
        let mut by_size: HashMap<usize, usize> = HashMap::new();
        for s in slices {
            let counter = by_size.entry(s).or_insert(0);
            *counter += 1;
        }
        // get unique sizes sorted in vector
        let mut size_indexs : Vec<_> = Vec::new();
        for (k, v) in &by_size {
            size_indexs.push(k);
        }
        size_indexs.sort()

        // answer
            println!("Case #{}: {}", t + 1, res);

    }
}
