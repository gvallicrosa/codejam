use std::io;
use std::cmp;

fn check_starts(parts: &Vec<Vec<String>>) -> Option<String> {
    let mut pattern = String::new();
    for p in parts {
        let block = p.iter().next().unwrap();
        if block.len() != 0 {
            if pattern.len() == 0 {
                // check empty
                pattern = block.clone();
            } else {
                // check with previous value
                let n = cmp::min(pattern.len(), block.len());
                if block[0..n].eq(&pattern[0..n]) {
                    if block.len() > pattern.len() {
                        pattern = block.clone();
                    }
                } else {
                    return None;
                }
            }
        }
        // println!("sta: {}", pattern);
    }
    Some(pattern)
}

fn check_ends(parts: &Vec<Vec<String>>) -> Option<String> {
    let mut pattern = String::new();
    for p in parts {
        let block: String = p.iter().last().unwrap().chars().rev().collect();
        if block.len() != 0 {
            if pattern.len() == 0 {
                // check empty
                pattern = block.clone();
            } else {
                // check with previous value
                let n = cmp::min(pattern.len(), block.len());
                if block[0..n].eq(&pattern[0..n]) {
                    if block.len() > pattern.len() {
                        pattern = block.clone();
                    }
                } else {
                    return None;
                }
            }
        }
        // println!("end: {}", pattern);
    }
    // undo reverse pattern
    Some(pattern.chars().rev().collect())
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T and B
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<usize>().unwrap();
    input.clear();

    // for each problem
    for t in 0..problems {
        // number of patterns
        io::stdin().read_line(&mut input).unwrap();
        let num = input.trim().parse::<usize>().unwrap();
        input.clear();

        // read the patterns
        let mut parts: Vec<Vec<String>> = Vec::with_capacity(num);
        for _i in 0..num {
            io::stdin().read_line(&mut input).unwrap();
            parts.push(input.trim().split("*").map(|v| String::from(v)).collect()); // take ownership
            input.clear();
        }

        // for p in &parts {
        //     println!("{:?}", p);
        // }

        // check starts and ends
        let pstart = match check_starts(&parts) {
            Some(v) => v,
            None => {
                println!("Case #{}: *", t + 1);
                continue;
            },
        };
        let pend = match check_ends(&parts) {
            Some(v) => v,
            None => {
                println!("Case #{}: *", t + 1);
                continue;
            },
        };

        // compose
        let mut result = String::new();
        result.push_str(&pstart);
        for ps in parts {
            for p in ps[1..ps.len()-1].iter() {
                result.push_str(p);
            }
        }
        result.push_str(&pend);

        // result
        println!("Case #{}: {}", t + 1, result);
    }
}
