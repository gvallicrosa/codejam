use std::io;


fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read N
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let num = input.trim().parse::<usize>().unwrap();

        // read matrix M
        let mut mat = vec![0; num * num];
        for i in 0..num {
            // read
            input.clear();
            io::stdin().read_line(&mut input).unwrap();
            let temp: Vec<_> = input.trim().split_whitespace().map(|v| v.parse::<u32>().unwrap()).collect();
            for j in 0..num {
                mat[i*num + j] = temp[j];
            }
        }

        // for i in 0..(N) {
        //     for j in 0..(N) {
        //         print!("{} ", M[i * N + j]);
        //     }
        //     println!("");
        // }

        let mut trace = 0;
        let mut rows = 0;
        let mut cols = 0;
        for i in 0..num {
            // trace
            trace += mat[i*num + i];
            // repeated rows and cols
            let mut rep_row = false;
            let mut rep_col = false;
            for j in 0..num-1 {
                let rx = mat[i*num + j]; // row
                let cx = mat[j*num + i]; // col
                // the following positions
                for k in j+1..num {
                    // rows
                    let ry = mat[i*num + k];
                    if rx == ry {
                        rep_row = true;
                    }
                    // cols
                    let cy = mat[k*num + i];
                    if cx == cy {
                        rep_col = true;
                    }
                }
                if rep_row && rep_col {
                    break;
                }
            }
            if rep_row {
                rows += 1;
            }
            if rep_col {
                cols += 1;
            }
        }

        // solution
        println!("Case #{}: {} {} {}", t + 1, trace, rows, cols);
    }
}
