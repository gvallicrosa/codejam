use std::io;
use std::iter::FromIterator;

fn show(skills: &Vec<Vec<usize>>) {
    let rows = skills.len();
    let cols = skills[0].len();
    for r in 0..rows {
        for c in 0..cols {
            if skills[r][c] == 0 {
                print!("· ");
            } else {
                print!("{} ", skills[r][c]);
            }
        }
        println!("");
    }
    println!("");
}

fn solution1() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<usize>().unwrap();
    input.clear();

    // for i in (0..10).into_iter().rev() {
    //     println!("{}", i);
    // }

    // for each problem
    for t in 0..problems {
        // rows and columns
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        let rows = temp[0];
        let cols = temp[1];
        input.clear();

        // skills
        let mut skills = vec![Vec::new(); rows];
        for i in 0..rows {
            io::stdin().read_line(&mut input).unwrap();
            skills[i] = input
                .trim()
                .split_whitespace()
                .map(|x| x.parse::<usize>().unwrap())
                .collect();
            input.clear();
        }
        let mut alone = vec![vec![false; cols]; rows];

        // show(&skills);

        // solve
        let mut total_interest = 0;
        loop {
            // check who deleted
            let mut to_delete = Vec::new();
            for r in 0..rows {
                for c in 0..cols {
                    // add current interest
                    total_interest += skills[r][c];

                    // check alone
                    if alone[r][c] {
                        continue; // never to delete or already deleted
                    }

                    // find neighbors
                    let mut neig_vals = 0;
                    let mut neig_count = 0;
                    // left
                    for i in (0..r).into_iter().rev() {
                        if skills[i][c] > 0 {
                            neig_vals += skills[i][c];
                            neig_count += 1;
                            break;
                        }
                    }
                    // right
                    for i in r + 1..rows {
                        if skills[i][c] > 0 {
                            neig_vals += skills[i][c];
                            neig_count += 1;
                            break;
                        }
                    }
                    // top
                    for i in (0..c).into_iter().rev() {
                        if skills[r][i] > 0 {
                            neig_vals += skills[r][i];
                            neig_count += 1;
                            break;
                        }
                    }
                    // bottom
                    for i in c + 1..cols {
                        if skills[r][i] > 0 {
                            neig_vals += skills[r][i];
                            neig_count += 1;
                            break;
                        }
                    }

                    // mark as alone?
                    if neig_count == 0 {
                        alone[r][c] = true;
                        continue; // for sure survives
                    }

                    // should delete?
                    if skills[r][c] * neig_count < neig_vals {
                        // if skills[r][c] < neig_vals / neig_count {
                        to_delete.push((r, c));
                        alone[r][c] = true; // same as deleted
                    }
                }
            }

            // end condition
            if to_delete.len() == 0 {
                break;
            }

            // delete
            // println!("{:?}", to_delete);
            for (r, c) in to_delete {
                skills[r][c] = 0;
            }

            // show(&skills);
        }

        // result
        println!("Case #{}: {}", t + 1, total_interest);
    }
}

struct Cell {
    pub value: usize,
    pub left: Option<usize>,
    pub right: Option<usize>,
    pub top: Option<usize>,
    pub down: Option<usize>,
}

impl Cell {
    pub fn new(v: usize) -> Cell {
        Cell {
            value: v,
            left: None,
            right: None,
            top: None,
            down: None,
        }
    }
}

struct DanceFloor {
    rows: usize,
    cols: usize,
    pub cells: Vec<Cell>,
}

impl DanceFloor {
    pub fn new(rs: usize, cs: usize) -> DanceFloor {
        DanceFloor {
            rows: rs,
            cols: cs,
            cells: Vec::new(),
        }
    }

    pub fn append(&mut self, vals: &mut Vec<Cell>) {
        self.cells.append(vals);
    }

    pub fn link(&mut self) {
        for i in 0..self.rows {
            for j in 0..self.cols {
                // our
                let index = i * self.cols + j;
                // top
                if i > 0 {
                    self.cells[index].top = Some((i - 1) * self.cols + j);
                }
                // bottom
                if i < self.rows - 1 {
                    self.cells[index].down = Some((i + 1) * self.cols + j);
                }
                // left
                if j > 0 {
                    self.cells[index].left = Some(i * self.cols + j - 1);
                }
                // right
                if j < self.cols - 1 {
                    self.cells[index].right = Some(i * self.cols + j + 1);
                }
            }
        }
    }

    pub fn get(&self, r: usize, c: usize) -> &Cell {
        &self.cells[r * self.cols + c]
    }

    pub fn delete(&mut self, r: usize, c: usize) {
        let index = r * self.cols + c;
        self.cells[index].value = 0; // delete skill
                                     // left => right
        if self.cells[index].left.is_some() {
            // something at our left
            let other = self.cells[index].left.unwrap();
            self.cells[other].right = self.cells[index].right; // assign to our right
        }
        // right => left
        if self.cells[index].right.is_some() {
            // something at our right
            let other = self.cells[index].right.unwrap();
            self.cells[other].left = self.cells[index].left; // assign to our left
        }
        if self.cells[index].top.is_some() {
            // something at our top
            let other = self.cells[index].top.unwrap();
            self.cells[other].down = self.cells[index].down; // assign to our down
        }
        if self.cells[index].down.is_some() {
            // something at our down
            let other = self.cells[index].down.unwrap();
            self.cells[other].top = self.cells[index].top; // assign to our top
        }
    }
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<usize>().unwrap();
    input.clear();

    // for each problem
    for t in 0..problems {
        // rows and columns
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        let rows = temp[0];
        let cols = temp[1];
        input.clear();

        // skills
        let mut dance = DanceFloor::new(rows, cols);
        for _i in 0..rows {
            io::stdin().read_line(&mut input).unwrap();
            let mut vs: Vec<_> = input
                .trim()
                .split_whitespace()
                .map(|x| Cell::new(x.parse::<usize>().unwrap()))
                .collect();
            dance.append(&mut vs);
            input.clear();
        }
        dance.link();

        // solve
        let mut total_interest = 0;
        loop {
            // check who deleted
            let mut to_delete = Vec::new();
            for r in 0..rows {
                for c in 0..cols {
                    // check alone
                    let cell = dance.get(r, c);
                    if cell.value == 0 {
                        continue;
                    }

                    // add current interest
                    total_interest += cell.value;

                    // find neighbors
                    let mut neig_vals = 0;
                    let mut neig_count = 0;

                    // left
                    if let Some(i) = cell.left {
                        neig_vals += dance.cells[i].value;
                        neig_count += 1;
                    }
                    // right
                    if let Some(i) = cell.right {
                        neig_vals += dance.cells[i].value;
                        neig_count += 1;
                    }
                    // top
                    if let Some(i) = cell.top {
                        neig_vals += dance.cells[i].value;
                        neig_count += 1;
                    }
                    // down
                    if let Some(i) = cell.down {
                        neig_vals += dance.cells[i].value;
                        neig_count += 1;
                    }

                    // should delete?
                    if cell.value * neig_count < neig_vals {
                        to_delete.push((r, c));
                    }
                }
            }

            // end condition
            if to_delete.len() == 0 {
                break;
            }

            // delete
            // println!("{:?}", to_delete);
            for (r, c) in to_delete {
                dance.delete(r, c);
            }

            // show(&skills);
        }

        // result
        println!("Case #{}: {}", t + 1, total_interest);
    }
}
