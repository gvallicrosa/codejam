# from collections import defaultdict

# read T
T = int(input())
for t in range(T):
    # read U
    input()

    # read 10^4 pairs of (Qi, Ri)
    # lmap = defaultdict(int)
    lmap = dict()
    for i in range(10000):
        _, letters = input().split()
        # only Ri
        for c in letters:
            if c in lmap:
                lmap[c] += 1
            else:
                lmap[c] = 1

    # final sort and guess
    sortlist = list()
    for key in lmap:
        sortlist.append((lmap[key], key))
    sortlist.sort()
    sortlist.reverse()
    # put the last one as first (0)
    rlist = [sortlist[-1], ] + sortlist[:-1]

    # print(sortlist)
    # print(rlist)

    # solutions
    res = ''.join((v for _, v in rlist))
    print(f"Case #{t+1:d}: {res:s}")
