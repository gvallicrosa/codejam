use std::io;
use std::collections::HashMap;
// use std::cmp::Ordering;

// #[derive(Eq)]
#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct Letter {
    pub c: char,
    pub count: i64,
}

// impl Ord for Letter {
//     fn cmp(&self, other: &Self) -> Ordering {
//         self.count.cmp(&other.count)
//     }
// }

// impl PartialOrd for Letter {
//     fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
//         Some(self.cmp(other))
//     }
// }

// impl PartialEq for Letter {
//     fn eq(&self, other: &Self) -> bool {
//         self.count == other.count
//     }
// }

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read U
        input.clear();
        io::stdin().read_line(&mut input).unwrap();

        // read 10^4 pairs of (Qi, Ri)
        let mut map: HashMap<_, i64> = HashMap::new();
        let mut front: HashMap<_, i64> = HashMap::new();
        for _ in 0..10_000 {
            input.clear();
            io::stdin().read_line(&mut input).unwrap();
            let temp: Vec<_> = input.trim().split_whitespace().collect();

            // only Ri
            for (i, c) in temp[1].chars().enumerate() {
                // check which letter is never in front?
                if i == 0 {
                    let counter = front.entry(c).or_insert(0);
                    *counter += 1;
                }
                // all letters
                let counter = map.entry(c).or_insert(0);
                *counter += 1;
            }
        }

        // final sort and guess
        let mut slist = Vec::new();
        for (c, count) in &front {
            // println!("{}: {}", c, count);
            slist.push(Letter {
                c: *c,
                count: *count,
            });
        }
        // slist.sort();
        slist.sort_by(|a, b| b.count.cmp(&a.count));

        // get letters in front
        let lfront: String = front.iter().map(|(k, _)| k).collect();
        let alllet: String = map.iter().map(|(k, _)| k).collect();
        let mut n = 'A';
        for c in alllet.chars() {
            if !lfront.contains(c) {
                n = c;
            }
        }
        // println!("front: {} {}", front, front.len());
        // println!("not-front: {}", n);

        // put the last one as first (0)
        let mut res = String::new();
        if lfront.len() == 9 {
            res.push(n);
            for letter in slist {
                if letter.c != n {
                    res.push(letter.c);
                }
            }
        } else {
            res.push(slist[9].c);
            for i in 0..9 {
                res.push(slist[i].c);
            }
        }

        // for l in slist {
        //     println!("{}: {}", l.c, l.count);
        // }

        // result
        println!("Case #{}: {}", t + 1, res);
    }
}
