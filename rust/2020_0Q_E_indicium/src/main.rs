use std::io;

fn get_row_with_value(matrix : &Vec<Vec<usize>>, row: usize, value: usize) -> Option<usize> {
    for j in row+1..matrix.len() {
        if matrix[j][row] == value {
            return Some(j);
        }
    }
    None
}


fn switch_rows(matrix : &mut Vec<Vec<usize>>, i: usize, j:usize) {
    let ri = matrix[i].clone();
    let rj = matrix[j].clone();
    matrix[i] = rj;
    matrix[j] = ri;
}


fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read N, K
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input.trim().split_whitespace().map(|v| v.parse::<usize>().unwrap()).collect();
        let num = temp[0];
        let trace = temp[1];

        // solve
        let mut matrix = vec![Vec::<usize>::with_capacity(num); num];
        for i in 0..num  {
            for j in 0..num {
                matrix[i].push(1 + (i+j) % num);
            }
        }

        // wich numbers we need for the sum?
        let v0 = trace / num;
        let v1 = v0 + 1;
        let a = num * v1 - trace;
        // println!("{} * {} + {} * {} = {} ({})", a, v0, num-a, v1, trace, a*v0+(num-a)*v1);

        for i in 0..num  {
            for j in 0..num {
                print!("{} ", matrix[i][j]);
            }
            println!("");
        }

        // mix matrix so values go to the trace
        let mut possible = true;
        let mut count_v0 = 0;
        let mut count_v1 = 0;
        for i in 0..num {
            let v = matrix[i][i];
            if (v == v0) && (count_v0 < a) {
                count_v0 += 1;
            } else if (v == v1) && (count_v1 < num - a) {
                count_v1 += 1;
            } else {
                // switch for v0
                if count_v0 < a {
                    match get_row_with_value(&matrix, i, v0) {
                        Some(c) => {
                            switch_rows(&mut matrix, i, c);
                            count_v0 += 1;
                            continue;
                        },
                        None => ()
                    }
                }
                // switch for v1
                if count_v1 < num - a {
                    match get_row_with_value(&matrix, i, v1) {
                        Some(c) => {
                            switch_rows(&mut matrix, i, c);
                            count_v1 += 1;
                            continue;
                        },
                        None => ()
                    }
                }
                // no options => impossible
                possible = false;
                break;
            }
        }

        println!("");
        if possible {
            println!("POSSIBLE");
        } else {
            println!("IMPOSSIBLE");
        }
        println!("{} * {} + {} * {} = {} ({})", a, v0, num-a, v1, trace, a*v0+(num-a)*v1);
        println!("{} : {}", a, num-a);
        // for i in 0..num  {
        //     for j in 0..num {
        //         print!("{} ", matrix[i][j]);
        //     }
        //     println!("");
        // }


        // let res : String = solution.care.iter().collect();
        // println!("Case #{}: {}", t + 1, res);
    }
}
