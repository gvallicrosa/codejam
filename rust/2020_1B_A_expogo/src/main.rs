use std::io;

fn log2(x: i32) -> i32 {
    32 - x.leading_zeros() as i32 // ceil bound
}

struct Position {
    pub x: i32,
    pub y: i32,
}

impl Position {
    pub fn new(a: i32, b: i32) -> Position {
        Position { x: a, y: b }
    }
    pub fn is_even(&self) -> bool {
        (self.x.abs() + self.y.abs()) % 2 == 0
    }
    pub fn check_future_ok(&self, other: &Position) -> bool {
        let mut p = Position {
            x: self.x,
            y: self.y,
        };
        p.origin(&other);
        p.scale();
        !p.is_even()
    }
    pub fn origin(&mut self, other: &Position) {
        self.x -= other.x;
        self.y -= other.y;
    }
    pub fn scale(&mut self) {
        self.x /= 2;
        self.y /= 2;
    }
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();
    input.clear();

    // movements
    let west = Position::new(-1, 0);
    let east = Position::new(1, 0);
    let south = Position::new(0, -1);
    let north = Position::new(0, 1);

    // for each problem
    for t in 0..problems {
        // read (X, Y)
        io::stdin().read_line(&mut input).unwrap();
        let temp: Vec<_> = input
            .trim()
            .split_whitespace()
            .map(|v| v.parse::<i32>().unwrap())
            .collect();
        let goal = Position::new(temp[0], temp[1]);
        input.clear();

        // solve
        // println!("goal {} {}", goal.x, goal.y);
        if goal.is_even() {
            println!("Case #{}: IMPOSSIBLE", t + 1);
        } else {
            // solve
            let mut xy = Position::new(goal.x.abs(), goal.y.abs());
            let mut solution = String::new();
            let total = xy.x + xy.y;
            let steps = log2(total);
            // println!("{} {:b} {}", total, total, steps);
            for _i in 0..steps {
                // println!("init xy {} {}", xy.x, xy.y);
                // end condition
                if (xy.x == 1) && (xy.y == 0) {
                    // println!("  x end cond");
                    solution.push('E');
                    break;
                } else if (xy.x == 0) && (xy.y == 1) {
                    // println!("  y end cond");
                    solution.push('N');
                    break;
                }
                // other
                if xy.x % 2 == 1 {
                    // println!("  x odd");
                    if xy.check_future_ok(&west) {
                        xy.origin(&west);
                        solution.push('W');
                    } else {
                        assert!(xy.check_future_ok(&east));
                        xy.origin(&east);
                        solution.push('E');
                    }
                } else if xy.y % 2 == 1 {
                    // println!("  y odd");
                    if xy.check_future_ok(&south) {
                        xy.origin(&south);
                        solution.push('S');
                    } else {
                        assert!(xy.check_future_ok(&north));
                        xy.origin(&north);
                        solution.push('N');
                    }
                } else {
                    panic!("impossible");
                }
                // println!("  {} {} {}", xy.x, xy.y, solution);
                xy.scale();
                // println!("  > {} {}", xy.x, xy.y);
            }
            // println!("{} {}", goal.x < 0, goal.y < 0);
            let res: String = solution
                .chars()
                .map(|c| change(c, goal.x < 0, goal.y < 0))
                .collect();
            println!("Case #{}: {}", t + 1, res);
        }
    }
}

fn change(c: char, invNS: bool, invEW: bool) -> char {
    if invNS {
        if c == 'N' {
            return 'S';
        } else if c == 'S' {
            return 'N';
        }
    }
    if invEW {
        if c == 'E' {
            return 'W';
        } else if c == 'W' {
            return 'E';
        }
    }
    c
}
