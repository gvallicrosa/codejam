use std::io;

struct Database {
    size: usize,
    pub values: Vec<i32>,
    pub fixed: Vec<bool>,
}

impl Database {
    pub fn new(num: usize) -> Database {
        Database {
            size: num,
            values: vec![-1; num],
            fixed: vec![false; num],
        }
    }

    // pub fn flip(&mut self) {
    //     for i in 0..self.size {
    //         self.values[i] *= -1;
    //     }
    // }

    // pub fn reverse(&mut self) {
    //     for i in 0..self.size / 2 {
    //         self.values[i].swap(self.values[self.size - 1 - i]);
    //     }
    // }

    // pub fn all_fixed(&self) -> bool {
    //     for i in 0..self.size {
    //         if !self.fixed[i] {
    //             return false;
    //         }
    //     }
    //     true
    // }

    pub fn get_result(&self) -> String {
        self.values
            .iter()
            .map(|v| if v == &0 { '0' } else { '1' })
            .collect()
    }

    pub fn print_raw(&self) {
        for i in 0..self.size {
            eprint!("{} ", self.values[i]);
        }
        eprintln!("");
    }

    // pub fn get_same(&self) -> Option<usize> {
    //     for i in 0..self.size / 2 {
    //         if self.values[i] == self.values[self.size - 1 - i] {
    //             return Some(i);
    //         }
    //     }
    //     None
    // }
    // pub fn get_diff(&self) -> Option<usize> {
    //     for i in 0..self.size / 2 {
    //         if self.values[i] != self.values[self.size - 1 - i] {
    //             return Some(i);
    //         }
    //     }
    //     None
    // }

    fn fix_aux(&mut self, index: usize, value: i32, vfinal: i32, vother: i32) {
        if !self.fixed[index] {
            self.fixed[index] = true;
            if self.values[index] == value {
                self.values[index] = vfinal;
            } else {
                self.values[index] = vother;
            }
        }
    }

    pub fn fix(&mut self, index: usize, value: i32) {
        if !self.fixed[index] {
            // current
            let current = self.values[index];
            // other value
            let other = if value == 0 { 1 } else { 0 };
            // parse group
            for i in 0..5 {
                // first
                let j = index + i;
                self.fix_aux(j, current, value, other);
                // last
                let k = self.size - 1 - j;
                self.fix_aux(k, current, value, other);
            }
        }
    }
}

fn ask(i: usize) -> i32 {
    println!("{}", i + 1);
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.trim().parse::<i32>().unwrap()
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T and B
    io::stdin().read_line(&mut input).unwrap();
    let temp: Vec<_> = input
        .trim()
        .split_whitespace()
        .map(|v| v.parse::<usize>().unwrap())
        .collect();
    let problems = temp[0];
    let num = temp[1];

    // for each problem
    for _t in 0..problems {
        // cases
        if num == 10 {
            // directly with text values
            let mut values = String::new();
            for i in 0..10 {
                println!("{}", i + 1);
                input.clear();
                io::stdin().read_line(&mut input).unwrap();
                values.push(input.trim().chars().next().unwrap());
            }
            println!("{}", values);
        } else {
            // database
            let mut db = Database::new(num);
            // get all values from all queries and save -q for 0 and +q for 1
            for index in 0..num / 10 {
                let label = (index + 2) as i32;
                // eprintln!("{} {}", num, index);
                // full query before changes
                for i in 0..5 {
                    // symmetric indexs (start, end)
                    let j = index * 5 + i;
                    let k = num - 1 - j;

                    // query start
                    let v = ask(j);
                    if v == 0 {
                        db.values[j] = -label;
                    } else {
                        db.values[j] = label;
                    }

                    // query end
                    let v = ask(k);
                    if v == 0 {
                        db.values[k] = -label;
                    } else {
                        db.values[k] = label;
                    }
                }
                // db.print_raw();
            }

            // get first diff and first same
            // let same_opt = db.get_same();
            // let diff_opt = db.get_diff();

            // causality
            // if (None == same_opt) || (None == diff_opt) {
            // all different
            // println!("ALL DIFFERENT");
            // query 10, one of each subgroup of (5 + 5) * (2 or 10)
            for group in 0..num / 10 {
                // max 10
                let index = group * 5;
                db.fix(index, ask(index)); // fix values
                db.print_raw();
            }
            println!("{}", db.get_result());
            // } else {

            // }

            // } else if None == diff_opt {
            // println!("ALL THE SAME");
            // all the same
            // } else {
            // we have both
            // }
            // propagate in their respective groups

            // start to get and guess next values only from one side since the other can be guessed by relationship
        }

        // answer
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        if input.trim() == "N" {
            break;
        }
    }
}
