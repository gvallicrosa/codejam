use std::io;
use std::cmp;
use std::collections::HashMap;

struct Pascal {
    pub row: usize,
    pub values: Vec<usize>,
}

impl Pascal {
    pub fn new() -> Pascal {
        Pascal {
            row: 1,
            values: vec![1 as usize],
        }
    }

    pub fn next(&mut self) -> () {
        self.row += 1;
        // println!("self.row {}", self.row);
        let mut v = vec![1];
        for i in 0..cmp::min(self.values.len() - 1, 7) {
            // println!("self.values {} {}", self.values[i], self.values[i + 1]);
            v.push(self.values[i] + self.values[i + 1]);
        }
        v.push(1);
        self.values = v;
    }
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<usize>().unwrap();
    input.clear();

    let mut p = Pascal::new();
    let mut triangle = Vec::new();
    triangle.push(p.values.clone());
    // for _ in 0..10 {
    //     println!("{:2} {:?}", p.row, p.values);
    //     p.next();
    // }

    // for each problem
    for t in 0..problems {
        // number of patterns
        // io::stdin().read_line(&mut input).unwrap();
        // let num = input.trim().parse::<usize>().unwrap();
        // input.clear();

        for num in 1..1_000_000_000 {

        // solve
        let mut accum = 0;
        let mut result = Vec::new();
        accum += 1;
        result.push((1, 1, accum)); // 1
        let mut row = 2;
        if num < 2000 {
            // work on the second col where nums are 1, 2, 3, 4, 5...
            // let n = row - 1; => v = n
            while accum < num {
                let v = row - 1;
                if accum + v <= num {
                    accum += v;
                    result.push((row, 2, accum));
                    row += 1;
                } else {
                    // leave not to overpass requested number
                    row -= 1;
                    break;
                }
            }
        } else {
            // look at fifth column where nums are 1, 5, 15, 35, 70, 126...
            // let n = row - 4; => v = (n * (n + 1) * (n + 2) * (n + 3)) / 24
            // look at fourth column where nums are 1, 4, 10, 20, 35, 56, 84...
            // let n = row - 3; => v = (n * (n + 1) * (n + 2)) / 6
            // look at third column where nums are 1, 3, 6, 10, 15, 21...
            // let n = row - 2; => v = n * (n + 1) / 2
            accum += 1;
            result.push((2, 2, accum)); // 1
            accum += 1;
            result.push((3, 3, accum)); // 1
            accum += 1;
            result.push((4, 4, accum)); // 1
            accum += 1;
            result.push((5, 5, accum)); // 1
            row = 6;
            while accum < num {
                // compute values
                let n = row - 1;
                let v2 = n;
                let n = row - 2;
                let v3 = n * (n + 1) / 2;
                let n = row - 3;
                let v4 = (n * (n + 1) * (n + 2)) / 6;
                let n = row - 4;
                let v5 = (n * (n + 1) * (n + 2) * (n + 3)) / 24;
                // println!("{} {} {} {}", v2, v3, v4, accum);
                if accum + v5 + v4 + v3 + v2 < num {
                    accum += v5;
                    result.push((row, 5, accum));
                    row += 1;
                } else {
                    // move to third colum
                    row -= 1;
                    break;
                }
            }
            while accum < num {
                // compute values
                let n = row - 1;
                let v2 = n;
                let n = row - 2;
                let v3 = n * (n + 1) / 2;
                let n = row - 3;
                let v4 = (n * (n + 1) * (n + 2)) / 6;
                // println!("{} {} {} {}", v2, v3, v4, accum);
                if accum + v4 + v3 + v2 < num {
                    accum += v4;
                    result.push((row, 4, accum));
                    row += 1;
                } else {
                    // move to third colum
                    row -= 1;
                    break;
                }
            }
            while accum < num {
                // compute values
                let n = row - 1;
                let v2 = n;
                let n = row - 2;
                let v3 = n * (n + 1) / 2;
                if accum + v3 + v2 < num {
                    accum += v3;
                    result.push((row, 3, accum));
                    row += 1;
                } else {
                    // move to second column
                    row -= 1;
                    break;
                }
            }
            // continue with second column
            while accum < num {
                let n = row - 1;
                let v2 = n;
                if accum + v2 <= num {
                    accum += v2;
                    result.push((row, 2, accum));
                    row += 1;
                } else {
                    // move to first column
                    row -= 1;
                    break;
                }
            }
        }
        // keep adding 1s until num
        while accum < num {
            accum += 1;
            result.push((row, 1, accum));
            row += 1;
        }

        // checker
        println!(">> {}", num);
        let mut val = 0;
        let mut uniques = HashMap::new();
        assert!(result.len() <= 500 );
        for i in 0..result.len() {
            // add value
            let (r, c, _d) = result[i];
            while r > p.row {
                p.next();
                triangle.push(p.values.clone());
            }
            val += triangle[r-1][c-1]; // indexing from zero

            // println!("pos {} {} val {}", r, c, val);

            // other
            if i == 0 {
                // first value
                assert_eq!(r, 1);
                assert_eq!(c, 1);
            } else {
                // movements
                let (pr, pc, _) = result[i - 1];
                let mut ok = false;
                if (pr + 1 == r) && (pc + 1 == c) {
                    // diag right
                    ok = true;
                } else if (pr + 1 == r) && (pc == c) {
                    // down a column
                    ok = true;
                } else if (pr == r) && (pc == c + 1) {
                    // left a row
                    ok = true;
                }
                assert!(ok);
            }

            // unique positions?
            match uniques.get(&(r, c)) {
                Some(_) => panic!("position ({} {}) not unique", r , c),
                None => (),
            }
            uniques.insert((r, c), 1);


        }
        assert_eq!(val, num);

        // result
        println!("Case #{}:", t + 1);


        // println!("Case #{}: {}", t + 1, result.len());
        // let mut maxr = 0;
        // let mut maxc = 0;
        // for &(r, c, d) in &result {
        //     if r > maxr {
        //         maxr = r;
        //     }
        //     if c > maxc {
        //         maxc = c;
        //     }
        // }
        // maxr -= 1; // index from 0
        // maxc -= 1;
        // println!("{} {}", maxr, maxc);

        // let mut m = vec![vec![0; maxc]; maxr];
        // for &(r, c, d) in &result {
            // m[r-1][c-1] += 1; // index from zero
        // }

        // for r in 0..maxr {
        //     for c in 0..maxc {
        //         if m[r][c] > 0 {
        //             print!("{}", m[r][c]);
        //         } else {
        //             print!(" ");
        //         }
        //     }
        //     println!("");
        // }

        // for (r, c, d) in result {
        //     // println!("{} {} {}", r, c, d);
        //     println!("{} {}", r, c);
        // }
        }
    }
}
