use std::io;


fn main() {
    // string to read to
    let mut input = String::new();

    // read T
    io::stdin().read_line(&mut input).unwrap();
    let problems = input.trim().parse::<u32>().unwrap();

    // for each problem
    for t in 0..problems {
        // read string
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        let line = String::from(input.trim());

        // solve
        let mut depth = 0;
        let mut result = String::new();
        for c in line.chars() {
            let v = c.to_digit(10).unwrap();
            if v < depth {
                // too deep
                for _i in 0..depth-v {
                    result.push(')');
                }
                depth = v;
            } else if v > depth {
                // too shallow
                for _i in 0..v-depth {
                    result.push('(');
                }
                depth = v;
            }
            result.push(c);
        }
        // close all levels
        for _i in 0..depth {
            result.push(')');
        }

        // solution
        println!("Case #{}: {}", t + 1, result);
    }
}
