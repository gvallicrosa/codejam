use std::io;

fn ask(x: i32, y: i32) -> String {
    // ask
    println!("{} {}", x, y);
    // read
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    String::from(input.trim())
}

fn main() {
    // string to read to
    let mut input = String::new();

    // read T, A, B
    io::stdin().read_line(&mut input).unwrap();
    let temp: Vec<_> = input
        .trim()
        .split_whitespace()
        .map(|v| v.parse::<i32>().unwrap())
        .collect();
    let problems = temp[0];
    let minr = temp[1];
    let _maxr = temp[2];
    input.clear();

    let min_pos: i32 = -1_000_000_000;
    let max_pos = min_pos.abs();
    let width = 2 * max_pos;

    // println!("0: {:?}", neighbourhood(0, 0, 0));
    // println!("1: {:?}", neighbourhood(0, 0, 1));
    // println!("2: {:?}", neighbourhood(0, 0, 2));

    // for each problem
    for _t in 0..problems {
        // solve
        let mut wrong = false;
        let mut done = false;
        let mut queries = 0;
        let mut hits = Vec::new();

        // binary search from left
        let mut low = min_pos;
        let mut high = min_pos + (width - 2 * minr) + 1;
        // check initial conditions
        let res = ask(low, 0);
        queries += 1;
        if res == "HIT" {
            hits.push((low, 0));
        } else {
            // binary search
            while low < high {
                let mid = (low + high) / 2;
                // ask
                let res = ask(mid, 0);
                queries += 1;
                if res == "CENTER" {
                    done = true;
                    break;
                } else if res == "HIT" {
                    high = mid;
                } else if res == "MISS" {
                    low = mid;
                } else {
                    wrong = true;
                    break;
                }
                // end condition
                if queries == 300 {
                    wrong = true;
                    break;
                }
                if low + 1 == high {
                    hits.push((high, 0));
                    break;
                }
            }
        }

        eprintln!("q {}", queries);

        // exit condition
        if done {
            continue;
        } else if wrong {
            break;
        }

        // binary search from bottom
        let mut low = min_pos;
        let mut high = min_pos + (width - 2 * minr) + 1;
        // check initial conditions
        let res = ask(0, low);
        queries += 1;
        if res == "HIT" {
            hits.push((0, low));
        } else {
            // binary search
            while low < high {
                let mid = (low + high) / 2;
                // ask
                let res = ask(0, mid);
                queries += 1;
                if res == "CENTER" {
                    done = true;
                    break;
                } else if res == "HIT" {
                    high = mid;
                } else if res == "MISS" {
                    low = mid;
                } else {
                    wrong = true;
                    break;
                }
                // end condition
                if queries == 300 {
                    wrong = true;
                    break;
                }
                if low + 1 == high {
                    hits.push((0, high));
                    break;
                }
            }
        }

        eprintln!("q {}", queries);

        // exit condition
        if done {
            continue;
        } else if wrong {
            break;
        }

        // binary search from diagonal
        let mut low = min_pos;
        let mut high = min_pos + (width - 2 * minr) + 1;
        // binary search
        while low < high {
            let mid = (low + high) / 2;
            // ask
            let res = ask(mid, mid);
            queries += 1;
            if res == "CENTER" {
                done = true;
                break;
            } else if res == "HIT" {
                high = mid;
            } else if res == "MISS" {
                low = mid;
            } else {
                wrong = true;
                break;
            }
            // end condition
            if queries == 300 {
                wrong = true;
                break;
            }
            if low + 1 == high {
                hits.push((high, high));
                break;
            }
        }

        eprintln!("q {}", queries);
        eprintln!("hits {:?}", hits);

        // exit condition
        if done {
            continue;
        } else if wrong {
            break;
        }

        // center of the circle
        let (x1, y1) = hits[0];
        let (x2, y2) = hits[1];
        let (x3, y3) = hits[2];
        let x1 = x1 as f32;
        let x2 = x2 as f32;
        let x3 = x3 as f32;
        let y1 = y1 as f32;
        let y2 = y2 as f32;
        let y3 = y3 as f32;
        // mid point
        let xmid1 = (x1 + x2) / 2.0;
        let xmid2 = (x1 + x3) / 2.0;
        let ymid1 = (y1 + y2) / 2.0;
        let ymid2 = (y1 + y3) / 2.0;
        // slope
        let s1 = (x1 - x2) / (y2 - y1);
        let s2 = (x1 - x3) / (y3 - y1);
        // perpendiculars
        let p1 = ymid1 - s1 * xmid1;
        let p2 = ymid2 - s2 * xmid2;
        // center
        let x = (p1 - p2) / (s2 - s1);
        let y = p1 + s1 * x;
        // int
        let x = x as i32;
        let y = y as i32;

        eprintln!("center {} {}", x, y);

        for d in 0..10 {
            // neghbourhood at distance
            for (vx, vy) in neighbourhood(x, y, d) {
                let res = ask(vx, vy);
                queries += 1;
                if res == "CENTER" {
                    done = true;
                    break;
                } else if res == "HIT" {
                } else if res == "MISS" {
                } else {
                    wrong = true;
                    break;
                }
                // end condition
                if queries == 300 {
                    wrong = true;
                    break;
                }
            }
            // exit
            if done || wrong {
                break;
            }
        }

        // nothing else
        if wrong {
            break;
        }
    }
}

fn neighbourhood(x: i32, y: i32, d: i32) -> Vec<(i32, i32)> {
    let mut v = Vec::new();
    if d == 0 {
        v.push((x, y));
    } else {
        let minx = x - d;
        let maxx = x + d;
        let miny = y - d;
        let maxy = y + d;
        for vx in minx..maxx+1 {
            // full rows
            v.push((vx, miny));
            v.push((vx, maxy));
        }
        for vy in miny + 1..maxy {
            // rows without ends
            v.push((minx, vy));
            v.push((maxx, vy));
        }
    }
    return v;
}
