#include "./graphs.h"

int main() {
  // As a complete graph
  std::vector<std::vector<bool>> graph(14, std::vector<bool>(14, false));
  graph[0][1] = true;
  graph[0][2] = true;
  graph[0][3] = true;
  graph[0][4] = true;
  graph[0][5] = true;
  graph[0][6] = true;
  graph[1][8] = true;
  graph[1][9] = true;
  graph[3][7] = true;
  graph[3][10] = true;
  graph[4][9] = true;
  graph[5][9] = true;
  graph[5][10] = true;
  graph[6][12] = true;
  graph[7][13] = true;
  graph[8][13] = true;
  graph[9][13] = true;
  graph[10][13] = true;
  graph[11][13] = true;
  graph[12][13] = true;
  // As collection of edges
  std::vector<std::pair<int, int>> pairs(8);
  pairs[0] = {1, 8};
  pairs[1] = {1, 9};
  pairs[2] = {3, 7};
  pairs[3] = {3, 10};
  pairs[4] = {4, 9};
  pairs[5] = {5, 9};
  pairs[6] = {5, 10};
  pairs[7] = {6, 12};
  // As a map form left to right
  std::map<int, std::vector<int>>
      lr; //  = {{1, {8, 9}}, {3, {7, 10}}, {4, {9}}, {5, {9, 10}}, {6, {12}}};
  lr[1].push_back(8);
  lr[1].push_back(9);
  lr[3].push_back(7);
  lr[3].push_back(10);
  lr[4].push_back(9);
  lr[5].push_back(9);
  lr[5].push_back(10);
  lr[6].push_back(12);
  // lr[1] = {8, 9};
  // lr[3] = {7, 10};
  // lr[4] = {9};
  // lr[5] = {9, 10};
  // lr[6] = {12};
  // Solve
  FordFulkersonSolver solver(graph);
  std::cout << solver.solve(0, 13) << std::endl;
  solver = FordFulkersonSolver(pairs);
  std::cout << solver.solve() << std::endl;
  // Solve
  hopcroftkarp::init(6, 6);
  hopcroftkarp::addEdge(1-1, 8-7);
  hopcroftkarp::addEdge(1-1, 9-7);
  hopcroftkarp::addEdge(3-1, 7-7);
  hopcroftkarp::addEdge(3-1, 10-7);
  hopcroftkarp::addEdge(4-1, 9-7);
  hopcroftkarp::addEdge(5-1, 9-7);
  hopcroftkarp::addEdge(5-1, 10-7);
  hopcroftkarp::addEdge(6-1, 12-7);
  std::cout << hopcroftkarp::maxMatching() << std::endl;
  return 0;
}
