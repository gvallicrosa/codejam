#include <algorithm>
#include <cassert>
#include <deque>
#include <iostream>
#include <map>
#include <set>
#include <utility>
#include <vector>

class GraphSolver {
protected:
  std::vector<std::vector<bool>> graph_;
  int rows_;
  int source_ = -1;
  int sink_ = -1;

public:
  GraphSolver(const std::vector<std::vector<bool>> &graph) {
    graph_ = graph;
    rows_ = graph_.size();
    for (int i = 0; i < rows_; i++) {
      assert(rows_ == graph_[i].size());
    }
  }
  GraphSolver(const std::vector<std::pair<int, int>> &pairs) {
    // Count left and right elements
    std::set<int> left;
    std::set<int> right;
    for (const auto pair : pairs) {
      left.insert(pair.first);
      right.insert(pair.second);
    }
    // Assert no elements in both sides
    for (auto l : left) {
      for (auto r : right) {
        if (l == r) {
          throw("Repeated elements.");
        }
      }
    }
    // Correspondences
    std::map<int, int> corresp;
    int index = 0;
    for (auto l : left) {
      corresp[l] = index;
      index++;
    }
    for (auto r : right) {
      corresp[r] = index;
      index++;
    }
    source_ = index;
    sink_ = index + 1;
    // Construct the graph
    int num = left.size() + right.size() + 2;
    graph_ = std::vector<std::vector<bool>>(num, std::vector<bool>(num, false));
    for (const auto pair : pairs) {
      graph_[corresp[pair.first]][corresp[pair.second]] = true;
    }
    // Assign source and sink
    for (auto l : left) {
      graph_[source_][corresp[l]] = true;
    }
    for (auto r : right) {
      graph_[corresp[r]][sink_] = true;
    }
  }
};

class FordFulkersonSolver : private GraphSolver {
public:
  FordFulkersonSolver(const std::vector<std::vector<bool>> &graph)
      : GraphSolver(graph) {}
  FordFulkersonSolver(const std::vector<std::pair<int, int>> &pairs)
      : GraphSolver(pairs) {}
  // Returns true if there is a path from source 's' to sink 't' in
  // residual graph. Also fills parent[] to store the path
  bool BFS(const int &source, const int &target, std::vector<int> &parent) {
    // Mark all the vertices as not visited
    std::vector<bool> visited(rows_, false);

    // Create a queue for BFS
    std::deque<int> queue;

    // Mark the source node as visited and enqueue it
    queue.push_back(source);
    visited[source] = true;

    // Standard BFS Loop
    while (queue.size() > 0) {
      // Dequeue a vertex from queue and print it
      int u = queue.front();
      queue.pop_front();

      // Get all adjacent vertices's of the dequeued vertex u
      // If a adjacent has not been visited, then mark it
      // visited and enqueue it
      for (int i = 0; i < graph_[u].size(); i++) {
        if ((!visited[i]) && (graph_[u][i])) {
          queue.push_back(i);
          visited[i] = true;
          parent[i] = u;
        }
      }
    }
    // If we reached sink in BFS starting from source, then return true
    return visited[target];
  }

  // Returns the maximum flow from s to t in the given graph
  int solve(int source = -1, int sink = -1) {
    // Take precomputed source and sink
    if ((source == -1) && (source_ != -1)) {
      source = source_;
    }
    if ((sink == -1) && (sink_ != -1)) {
      sink = sink_;
    }
    if ((source == -1) || (sink == -1)) {
      throw("Invalid source or sink.");
    }

    // This array is filled by BFS and to store path
    std::vector<int> parent(rows_, -1);

    // There is no flow initially
    int max_flow = 0;

    // Augment the flow while there is path from source to sink
    while (BFS(source, sink, parent)) {
      // Find minimum residual capacity of the edges along the
      // path filled by BFS. Or we can say find the maximum flow
      // through the path found.
      float path_flow = 1e100;
      int s = sink;
      while (s != source) {
        path_flow = 1.0; // min(path_flow, self.graph[parent[s]][s])
        s = parent[s];
      }
      // Add path flow to overall flow
      max_flow += path_flow;

      // update residual capacities of the edges and reverse edges along the
      // path
      int v = sink;
      while (v != source) {
        int u = parent[v];
        graph_[u][v] = false; // -= path_flow
        graph_[v][u] = true;  // += path_flow
        v = parent[v];
      }
    }
    return max_flow;
  }
};


namespace hopcroftkarp {
// class HopcroftKarpSolver
const int MAXN1 = 50000;
const int MAXN2 = 50000;
const int MAXM = 150000;

int n1, n2, edges, last[MAXN1], prev[MAXM], head[MAXM];
int matching[MAXN2], dist[MAXN1], Q[MAXN1];
bool used[MAXN1], vis[MAXN1];

void init(int _n1, int _n2) {
  n1 = _n1;
  n2 = _n2;
  edges = 0;
  std::fill(last, last + n1, -1);
}

void addEdge(int u, int v) {
  head[edges] = v;
  prev[edges] = last[u];
  last[u] = edges++;
}

void bfs() {
  std::fill(dist, dist + n1, -1);
  int sizeQ = 0;
  for (int u = 0; u < n1; ++u) {
    if (!used[u]) {
      Q[sizeQ++] = u;
      dist[u] = 0;
    }
  }
  for (int i = 0; i < sizeQ; i++) {
    int u1 = Q[i];
    for (int e = last[u1]; e >= 0; e = prev[e]) {
      int u2 = matching[head[e]];
      if (u2 >= 0 && dist[u2] < 0) {
        dist[u2] = dist[u1] + 1;
        Q[sizeQ++] = u2;
      }
    }
  }
}

bool dfs(int u1) {
  vis[u1] = true;
  for (int e = last[u1]; e >= 0; e = prev[e]) {
    int v = head[e];
    int u2 = matching[v];
    if (u2 < 0 || (!vis[u2] && dist[u2] == dist[u1] + 1 && dfs(u2))) {
      matching[v] = u1;
      used[u1] = true;
      return true;
    }
  }
  return false;
}

int maxMatching() {
  std::fill(used, used + n1, false);
  std::fill(matching, matching + n2, -1);
  for (int res = 0;;) {
    bfs();
    std::fill(vis, vis + n1, false);
    int f = 0;
    for (int u = 0; u < n1; ++u)
      if (!used[u] && dfs(u))
        ++f;
    if (!f)
      return res;
    res += f;
  }
}
}  // namespace hopcroftkarp
